import logging

from package_parser import PackageParser

class OnePassSignaturePacket(PackageParser):
    """class implements openpgp One-Pass Signature Packet (Tag 4)"""

    def __init__(self, length):
        super(OnePassSignaturePacket, self).__init__()

        self.length = length
        self.package = {}
        self.tmp_list = []
        self.count = 0
        self.state = 0
        self.amount_of_mpi_bytes = 0

    def process(self, byte):
        """parses a public key packet as defined in RFC 4880 5.5.2 used in Tag 4
        (see 5.5.1.1 and 5.5.1.2)

        Args:
            byte: the next input byte for the parser
        """

        payload_byte = int.from_bytes(byte, byteorder='big')
        oldstate = self.state

        if self.state == 0:
            self.package['version'] = payload_byte
            if self.package['version'] != 3:
                logging.getLogger(__name__).error('only version 3 is supported')
                exit()
            self.state = 1
        elif self.state == 1:
            # for the available signature types see
            # [RFC4880 sec 5.2.1](https://tools.ietf.org/html/rfc4880#section-5.2.1)
            self.package['signature type'] = payload_byte
            self.state = 2
        elif self.state == 2:
            self.package['hash algorithm'] = payload_byte
            self.state = 3
        elif self.state == 3:
            self.package['public-key algorithm'] = payload_byte
            self.state = 4
        elif self.state == 4:
            self.tmp_list.append(byte)
            if self.count >= 8 - 1:
                self.package['Key ID'] = self.tmp_list
                self.state = 5
        elif self.state == 5:
            self.package['nested'] = payload_byte
            self.state = 6
        else:
            logging.getLogger(__name__).error('reached unspecified state')
            exit()

        # if state didn't change increment otherwise set to zero
        self.count += 1
        if self.state != oldstate:
            self.count = 0
            self.tmp_list = []

    def __str__(self):
        return ('One-Pass Signature Packet:\n'
                '               Version: %d\n'
                '               Sig Type: %d\n'
                '               Hash algorithm: %d\n'
                '               Public-Key algorithm: %d\n'
                '               Key ID: %s\n'
                '               nested: %d\n'
                % (self.package['version'],
                   self.package['signature type'],
                   self.package['hash algorithm'],
                   self.package['public-key algorithm'],
                   self.package['Key ID'],
                   self.package['nested'],
                  ))
