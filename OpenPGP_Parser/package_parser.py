""" Defines the Interface for specific payload parser in openpgp packages"""
from abc import ABCMeta
from abc import abstractmethod

class PackageParser(metaclass=ABCMeta):
    """ Interface to be used by openpgp payload parsers """

    @abstractmethod
    def process(self, byte):
        """ this function is called for each byte. It should consume the byte
        and parse the package it was designed for

        Args:
                byte: the next byte to be consumed
        """
        pass
