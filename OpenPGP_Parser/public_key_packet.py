import logging
from datetime import datetime
import hashlib
from Crypto.PublicKey import RSA

from package_parser import PackageParser

class PublicKeyPacket(PackageParser):
    """class implements openpgp Public_key packets (Tag 6 and 14)"""


    def __init__(self, length):
        super(PublicKeyPacket, self).__init__()

        self.length = length
        self.package = {}
        self.tmp_list = []
        self.count = 0
        self.state = 0
        self.amount_of_mpi_bytes = 0
        self.fingerprint = hashlib.sha1()
        self.fingerprint.update(b'\x99')
        self.fingerprint.update(length.to_bytes(2, byteorder='big'))

    def process(self, byte):
        """parses a public key packet as defined in RFC 4880 5.5.2 used in Tag 6
        and Tag 14 (see 5.5.1.1 and 5.5.1.2)

        Args:
            byte: the next input byte for the parser
        """
        self.fingerprint.update(byte)
        payload_byte = int.from_bytes(byte, byteorder='big')
        oldstate = self.state

        if self.state == 0:
            self.package['version'] = payload_byte
            if self.package['version'] != 4:
                # TODO (Tommy Lehmann): implement version 3
                logging.getLogger(__name__).error('only version 4 is supported')
                exit()
            self.state = 1
        elif self.state == 1:
            self.tmp_list.append(byte)
            if self.count > 2:
                self.package['time'] = int.from_bytes(b''.join(self.tmp_list),
                                                      byteorder='big')
                self.state = 2
        elif self.state == 2:
            self.package['algorithm'] = payload_byte
            self.state = 3
        elif self.state == 3:
            self.tmp_list.append(byte)
            if self.count > 0:
                self.amount_of_mpi_bytes = (int.from_bytes(
                    b''.join(self.tmp_list), byteorder='big') + 7) // 8
                self.state = 4
        elif self.state == 4:
            self.tmp_list.append(byte)
            if self.count + 1 >= (self.amount_of_mpi_bytes):
                tmp_int = int.from_bytes(b''.join(self.tmp_list), byteorder='big')
                if self.package['algorithm'] == 1:
                    if 'modul' not in self.package:
                        self.package['modul'] = tmp_int
                    else:
                        self.package['exponent'] = tmp_int
                    if 'modul' and 'exponent' in self.package:
                        self.package['RSA key'] = (
                            RSA.construct((self.package['modul'],
                                           self.package['exponent'],
                                          )))
                        self.package['fingerprint'] = self.fingerprint.digest()
                        self.package['Key ID'] = self.package['fingerprint'][-8:]
                self.amount_of_mpi_bytes = 0
                self.state = 3


        # if state didn't change increment otherwise set to zero
        if self.state == oldstate:
            self.count += 1
        else:
            self.count = 0
            self.tmp_list = []

    def __str__(self):
        return ('Payload: Version: %d\n'
                '         created at: %s\n'
                '         Algorithm: %s\n'
                '         n: %d\n'
                '         e: %d\n'
                '         RSA key: %s\n'
                '           Key-ID: %s\n'
                % (self.package['version'],
                   datetime.fromtimestamp(self.package['time']),
                   self.package['algorithm'],
                   self.package['modul'],
                   self.package['exponent'],
                   self.package['RSA key'],
                   self.package['Key ID'].hex(),
                  ))
