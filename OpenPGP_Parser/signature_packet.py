import logging

from package_parser import PackageParser

class SignaturePacket(PackageParser):
    """docstring for signature_packet."""
    def __init__(self, length):
        super(SignaturePacket, self).__init__()

        self.length = length
        self.package = {}
        self.state = 0
        self.states = []
        self.count = 0
        self.bytes_in_next_state = 0
        self.tmp = []

    def process(self, byte):
        old_state = self.state
        if self.state == 0:
            self.package['version'] = int.from_bytes(byte, byteorder='big')
            if self.package['version'] != 4:
                # TODO (Tommy Lehmann): implement version 3
                logging.getLogger(__name__).error('only version 4 is supported')
                exit()
            self.state = 1
        elif self.state == 1:
            self.package['signature type'] = int.from_bytes(byte, byteorder='big')
            self.state = 2
        elif self.state == 2:
            self.package['public-key algorithm'] = int.from_bytes(byte, byteorder='big')
            self.state = 3
        elif self.state == 3:
            self.package['hash algorithm'] = int.from_bytes(byte, byteorder='big')
            self.state = 4
        elif self.state == 4:
            self.tmp.append(byte)
            if self.count >= 1:
                self.package['hashed data count'] = (
                    int.from_bytes(b''.join(self.tmp), byteorder='big'))
                self.state = 5
        elif self.state == 5:
            self.tmp.append(byte)
            if self.count >= self.package['hashed data count'] - 1:
                self.package['hashed subpackets'] = _signature_subpacket_(self.tmp)
                self.state = 6
        elif self.state == 6:
            self.tmp.append(byte)
            if self.count >= 1:
                self.package['unhashed data count'] = (
                    int.from_bytes(b''.join(self.tmp), byteorder='big'))
                self.state = 7
        elif self.state == 7:
            self.tmp.append(byte)
            if self.count >= self.package['unhashed data count'] - 1:
                self.package['unhashed subpackets'] = _signature_subpacket_(self.tmp)
                self.state = 8
        elif self.state == 8:
            self.tmp.append(byte)
            if self.count >= 1:
                self.package['signature test bits'] = self.tmp
                self.state = 9
        elif self.state == 9:
            self.tmp.append(byte)
            if self.count > 0:
                if self.package['public-key algorithm'] == 1:
                    self.package['signature length'] = (int.from_bytes(
                        b''.join(self.tmp), byteorder='big') + 7) // 8
                else:
                    logging.getLogger(__name__).error(
                        'unsupported algorithm: %d', self.package['public-key algorithm'])
                    exit()
                self.state = 10
        elif self.state == 10:
            self.tmp.append(byte)
            if self.count >= (self.package['signature length']) - 1:
                tmp_int = int.from_bytes(b''.join(self.tmp), byteorder='big')
                if self.package['public-key algorithm'] == 1:
                    self.package['signature'] = tmp_int
                else:
                    logging.getLogger(__name__).error(
                        'unsupported algorithm: %d',
                        self.package['public-key algorithm'])
                    exit()
                self.state = 9
        else:
            logging.getLogger(__name__).error('reached unspecified state')
            exit()

        self.count += 1
        if old_state != self.state:
            self.tmp = []
            self.count = 0


    def __str__(self):
        ret = ('Signature Packet:\n'
               '               Version: %d\n'
               '               Sig Type: %d\n'
               '               Public-Key algorithm: %d\n'
               '               Hash algorithm: %d\n'
               '               hashed data count: %d\n'
               % (self.package['version'],
                  self.package['signature type'],
                  self.package['public-key algorithm'],
                  self.package['hash algorithm'],
                  self.package['hashed data count'])
              )
        for subpacket in self.package['hashed subpackets']:
            ret += _print_signature_subpacket_(subpacket)
        ret += ('               unhashed data count: %d\n'
                % (self.package['unhashed data count']))
        for subpacket in self.package['unhashed subpackets']:
            ret += _print_signature_subpacket_(subpacket)
        ret += ('               Sig Test octets: %s\n'
                '               Signature length: %d Byte ≙ %d bit\n'
                '               Signature: %d\n'
                % (self.package['signature test bits'],
                   self.package['signature length'],
                   self.package['signature length'] * 8,
                   self.package['signature']))
        return ret

def _print_signature_subpacket_(subpacket):
    """returns a pretty print string from a fully parsed signature subpacket"""
    # the following list is taken from
    # [RFC4880 5.2.3.1](https://tools.ietf.org/html/rfc4880#section-5.2.3.1)
    type_explination = {0 : 'Reserved',
                        1 : 'Reserved',
                        2 : 'Signature Creation Time',
                        3 : 'Signature Expiration Time',
                        4 : 'Exportable Certification',
                        5 : 'Trust Signature',
                        6 : 'Regular Expression',
                        7 : 'Revocable',
                        8 : 'Reserved',
                        9 : 'Key Expiration Time',
                        10 : 'Placeholder for backward compatibility',
                        11 : 'Preferred Symmetric Algorithms',
                        12 : 'Revocation Key',
                        13 : 'Reserved',
                        14 : 'Reserved',
                        15 : 'Reserved',
                        16 : 'Issuer',
                        17 : 'Reserved',
                        18 : 'Reserved',
                        19 : 'Reserved',
                        20 : 'Notation Data',
                        21 : 'Preferred Hash Algorithms',
                        22 : 'Preferred Compression Algorithms',
                        23 : 'Key Server Preferences',
                        24 : 'Preferred Key Server',
                        25 : 'Primary User ID',
                        26 : 'Policy URI',
                        27 : 'Key Flags',
                        28 : "Signer's User ID",
                        29 : 'Reason for Revocation',
                        30 : 'Features',
                        31 : 'Signature Target',
                        32 : 'Embedded Signature'}
    return ('                  subpacket length: %d\n'
            '                  subpacket type: %s\n'
            '                  subpacket payload: %s\n'
            % (subpacket['length'],
               type_explination[subpacket['subpacket type']],
               subpacket['payload']))

def _signature_subpacket_(byte_array):
    state = 0
    subpackets = []
    tmp = []
    count = 0

    current_subpackage = {}

    for byte in byte_array:
        old_state = state
        byte_integer = int.from_bytes(byte, byteorder='big')
        if state == 0: # subpacket length type
            if 'length type' not in current_subpackage: # we dont know the length type
                if byte_integer < 192:
                    current_subpackage['length type'] = 1
                    tmp.append(byte)
                elif byte_integer < 255:
                    current_subpackage['length type'] = 2
                    tmp.append(byte)

                else:
                    current_subpackage['length type'] = 5
            else:
                tmp.append(byte)

            if count >= current_subpackage['length type'] - 1:
                if current_subpackage['length type'] == 1:
                    current_subpackage['length'] = (
                        int.from_bytes(b''.join(tmp), byteorder='big'))
                elif current_subpackage['length type'] == 2:
                    current_subpackage['length'] = (
                        (int.from_bytes(b''.join(tmp[0]), byteorder='big')
                         - 192) << 8 + 192)
                    current_subpackage['length'] += (
                        int.from_bytes(b''.join(tmp[1]), byteorder='big'))
                else:
                    current_subpackage['length'] = (
                        int.from_bytes(b''.join(tmp), byteorder='big'))
                state = 1

        elif state == 1: # subpacket type (1 octet)
            current_subpackage['subpacket type'] = byte_integer
            state = 2
        elif state == 2: # subpacket type specific payload
            tmp.append(byte)
            # the length includes the type octet and we start counting at 0
            if count >= current_subpackage['length'] - 2: # successfully parsed
                current_subpackage['payload'] = tmp
                state = 0
                subpackets.append(current_subpackage)
                current_subpackage = {}

        count += 1

        if old_state != state:
            count = 0
            tmp = []

    return subpackets
