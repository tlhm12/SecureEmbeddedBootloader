from datetime import datetime
import hashlib

from package_parser import PackageParser

class LiteralData(PackageParser):
    """the class implements the OpenPGP Literal Data Packet (Tag 11)"""

    def __init__(self, length):
        super(LiteralData, self).__init__()

        self.length = length
        self.package = {}
        self.tmp_list = []
        self.count = 0
        self.state = 0
        self.amount_of_mpi_bytes = 0

    def get_hash(self, hash_algorithm):
        """computes the hash_value of the relevant data in this packet

        Args:
            hash_algorithm: a hashlib compatible hashobject supporting
                    update() and digest()
        Returns:
            the hash value as byte string as returned by digest()
        """
        hash_algorithm.update(b''.join(self.package['literal data']))
        return hash_algorithm.digest()

    def process(self, byte):
        """parses a literate data packet (Tag 4) as defined in RFC 4880 5.9

        Args:
            byte: the next input byte for the parser
        """
        payload_byte = int.from_bytes(byte, byteorder='big')
        oldstate = self.state

        if self.state == 0:
            self.package['formate type'] = byte
            self.state = 1
        elif self.state == 1:
            self.package['file name length'] = payload_byte
            self.state = 2
            if self.package['file name length'] == 0: # there wont be a file name
                self.state = 3
                self.package['file name'] = ''
        elif self.state == 2:
            self.tmp_list.append(byte)
            if self.count >= self.package['file name length'] - 1:
                self.package['file name'] = b''.join(self.tmp_list).decode()
                self.state = 3
        elif self.state == 3: # time associated
            self.tmp_list.append(byte)
            if self.count >= 4 - 1:
                self.package['time'] = int.from_bytes(b''.join(self.tmp_list),
                                                      byteorder='big')
                self.state = 4
        elif self.state == 4:
            if 'literal data' not in self.package: # add it for the first time
                self.package['literal data'] = []
            self.package['literal data'].append(byte)


        # if state didn't change increment otherwise set to zero
        if self.state == oldstate:
            self.count += 1
        else:
            self.count = 0
            self.tmp_list = []

    def __str__(self):
        return ('Literal Data Packet: formate: %s\n'
                '         file name length: %d\n'
                '         file name: %s\n'
                '         date: %s\n'
                '         data: %s\n'
                '         SHA256: %s\n'
                % (self.package['formate type'],
                   self.package['file name length'],
                   self.package['file name'],
                   datetime.fromtimestamp(self.package['time']),
                   self.package['literal data'],
                   self.get_hash(hashlib.sha256()).hex()))
