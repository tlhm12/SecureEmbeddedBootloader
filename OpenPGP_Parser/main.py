#!/usr/bin/env python3
"""The modules purpose is to parse openpgp Packages for further processing"""
import argparse
import sys
import logging

import public_key_packet
import signature_packet
import user_id_packet
import one_pass_signature_packet
import literal_data

class OpenPgpPackageParser:
    """the class parses openpgp packages and their content"""

    # pylint: disable=too-many-instance-attributes
    # openpgp packages simply have that many attributes

    tags = {6 : public_key_packet.PublicKeyPacket,
            13 : user_id_packet.UserIdPacket,
            2 : signature_packet.SignaturePacket,
            # RFC 4880 5.5.1.2. states that Public-Subkey Packets are the
            # same as Tag 6 Public-Key Packets but are dedicated to
            # subkeys instead of primary keys
            14 : public_key_packet.PublicKeyPacket,
            4 : one_pass_signature_packet.OnePassSignaturePacket,
            11 : literal_data.LiteralData,
           }

    def __init__(self, print_immediately):
        self.print_immediately = print_immediately
        self.state = 0

        self.states = {0 : self.__header,
                       1 : self.__length,
                       2 : self.__payload,
                      }

        self.length_bytes_to_read = 0
        self.payload_length = 0
        self.packet_tag = 0 #0 is reserved and presents a special value
        self.new_formate = False
        self.current_payload_parser = None

    def process_byte(self, byte):
        """process the next byte in input

        Args:
            byte: next byte to process
        """
        try:
            function = self.states[self.state]
        except KeyError:
            logging.getLogger(__name__).error(
                "detected unknown state in OpenPGP_Package_Parser")
            exit()
        if function:
            function(byte)

    def print_package(self):
        """print the last package in human readable way"""
        # TODO (ERFier): to be implemented, the debug output is sufficiant atm
        pass

    def __header(self, byte):
        """function to process the byte of a OpenPGP Package (the header octet).

        In this first octet are information like the type of the length field
        and the type of payload.
        """
        header_byte = int.from_bytes(byte, byteorder='big')
        #according to RFC 4880 page 14 bit 7 has to be 1
        if (header_byte & 0x80) == 0:
            logging.getLogger(__name__).error(
                'Header is invalid as bit 7 of PTag is not 1. See RFC 4880 page 14\n'
                'The recognized byte is: %x',
                header_byte)
            exit()
        self.packet_tag = (header_byte & 0x3C) >> 2
        self.length_bytes_to_read = header_byte & 0x03
        self.new_formate = (header_byte & 0x40) > 0
        if self.print_immediately:
            print(
                'Header: packet format: %s\n'
                '        packet tag: %d\n'
                '        length type: %d'
                % (self.new_formate, self.packet_tag, self.length_bytes_to_read))
        self.state = 1

    def __length(self, byte):
        """fuction to process the length information of the package header.

        RFC 4880 4.2 states that there are different types of length fields.
        """
        if self.length_bytes_to_read > 2:
            logging.getLogger(__name__).error('extended packet length is '
                                              'currently not implmented')
            exit()
        self.length_bytes_to_read = self.length_bytes_to_read - 1
        self.payload_length = ((self.payload_length << 8) +
                               int.from_bytes(byte, byteorder='big'))
        if self.length_bytes_to_read <= 0:
            if self.print_immediately:
                print(
                    '        payload length: %d Bytes/octets'
                    % (self.payload_length))
            self.current_payload_parser = type(self).tags[self.packet_tag](
                self.payload_length)
            self.state = 2

    def __payload(self, byte):
        """function to process data belonging to the payload.

        Each payload type aka. Tag has its own parsing class thats used to do
        the actual parsing, as the payload types vary much in there structure
        """
        self.payload_length = self.payload_length - 1
        self.current_payload_parser.process(byte)
        if self.payload_length <= 0:
            if self.print_immediately:
                print('%s' % self.current_payload_parser)
                print('---End of Package---\n')
            if self.packet_tag == 6: # is public_key_packet
                KNOWN_KEYS[self.current_payload_parser.package['Key ID']] = (
                    self.current_payload_parser.package['RSA key'])
            self.state = 0

# known_keys is a Key-ID - Crypto.PublicKey mapping
KNOWN_KEYS = {}

def main():
    """The function parses a given file or stdin for OpenPGP packages.

    The file can be given in commandline arguments.
    """
    parser = argparse.ArgumentParser(description='Parse an OpenPGP key')

    parser.add_argument('input_file', metavar='Input File', type=argparse.FileType('rb'),
                        nargs='?', default=sys.stdin.buffer)
    parser.add_argument('--version', action='version', version='v. 0.1')

    args = parser.parse_args()
    print(args)
    parse(args.input_file)

    print('=== Finished Parsing ===\n')
    print('we found the following keys: %s' % KNOWN_KEYS)

def parse(input_file):
    """
    Args:
        input_file: the file object to be parsed. Its expected that the file is
        in binary mode e.g. opened with 'rb' mode specified or sys.stdin.buffed
        if stdin should be read.
    """
    parser = OpenPgpPackageParser(True)
    for byte in iter(lambda: input_file.read(1), b''): # stop reading when b'' is returned
        parser.process_byte(byte)

#script as advised by
#[google style guide](https://google.github.io/styleguide/pyguide.html#Main)
if __name__ == '__main__':
    main()
