#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <byteswap.h>
#include "bigdigits.h"

int main( int argc, char* argv[] )
{
    while( !feof(stdin) )
    {
        uint16_t octets_to_read = 0;
        int bytes_read;
        if( ( bytes_read = fread( &octets_to_read, 2, 1, stdin ) ) < 1 )
        {
            /* reached end of file */
            exit(EXIT_SUCCESS);
        }
        octets_to_read = __bswap_16(octets_to_read);
        /* calculate the amount of bytes needed to store the bits */
        octets_to_read = ( octets_to_read + 7 ) / 8;
        uint8_t *octets = malloc( octets_to_read );
        uint8_t *writepointer = octets;
        while( ( bytes_read = fread( writepointer, 1, octets_to_read, stdin ) ) < octets_to_read )
        {
            if( bytes_read == 0 ) /* EOF reached */
            {
                exit(EXIT_FAILURE);
            }
            octets_to_read -= bytes_read;
            writepointer += bytes_read;
        }
        DIGIT_T big_number[ 1024 / sizeof(DIGIT_T) ];
        int real_size_of_bigdigit = mpConvFromOctets(big_number, sizeof( big_number), octets, octets_to_read);
        mpPrintDecimal("Number read: ", big_number, real_size_of_bigdigit, "\n");
        
        free(octets);
    }
    return 0;
}
