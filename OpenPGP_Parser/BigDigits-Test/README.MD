# BigDigits-Test

This programm uses the [BigDigit library][bigdigit] writen by David Ireland. It
takes a Bytestream from Stdin, converts it to a BigDigit and displays it. The
Bytestream has to be formated in the way descripted in RFC4880 [Section 3.2.][rfc4880_sec3.2] Mutliprecision Integers.

All numbers are in Big Endian format. The first two bytes or octets as the RFC
calls them contain the number of bits the following MPI consists of. Further
their are (length + 7)/8 octets if using integer division.

The file openpgp-integers.txt contains two such numbers. They are extracted
from a sample 1024 bit public key. You can run the programm e.g. with IO-redirection
`./bigdigit_converter < openpgp-integers.txt`.

The output should be:

```
Number read: 161987006036081326000968228822037352166007119908188352025228798993
3782922830505011824301262655939354632188541190603284474137928776536602175682280
9640785736941915104687088794216238810376860758390926438774521168185538722645043
6152079478445005382988135289163027794514231117858722034405894652201231337251431
789257
Number read: 65537
```

[bigdigit]: http://www.di-mgt.com.au/bigdigits.html
[rfc4880_sec3.2]: https://tools.ietf.org/html/rfc4880#section-3.2
