#include "protokoll.h"
#include <stdbool.h>
#include "uart_ptr.h"
#include "dyn_lib.h"
#include <string.h>
#include <util/delay.h>


#define SIGNATURE_LENGTH (1024/8)

static bool process(uint8_t c);
static int read_message(uint8_t c);
static void error(void);

enum Messagetype_t {
    request,
    ack,
    content,
    chat
};

struct Request_t {
    uint16_t port;
    uint32_t total_length;
};

struct Ack_t {
    uint16_t subtype;
    uint32_t additional_info;
};

struct Content_t {
    uint8_t packetnumber;
    uint16_t length;
    uint8_t *buffer;
};

struct Message_t {
    enum Messagetype_t type;
    
    union {
        struct Request_t req;
        struct Ack_t ack;
        struct Content_t cont;
    } m;
};

struct Message_t message;

enum Messagestate_t {
    ms_header,
    ms_length,
    ms_content,
    ms_crc
};

enum Messagestate_t message_state = ms_header;

enum Protokoll_State_t {
    start,
    request_received,
    wait_for_data,
    process_message,
    wait_for_next_message,
    fin_wait
};

/* DEBUG */
static char nibble_to_hex( uint8_t nibble )
{
    if( nibble < 10 )
    {
        return nibble + '0';
    } else
    {
        return nibble + ( 'a' - 10 );
    }
}

static void byte_to_hex( char *hexvalues, uint8_t byte )
{
    hexvalues[ 0 ] = nibble_to_hex( byte >> 4 );
    hexvalues[ 1 ] = nibble_to_hex( byte & 0x0f );
}
/* DEBUG */

int read(uint8_t* buffer, uint8_t max_bytes_to_read)
{
    while(true) {
        unsigned int c = uart_getc();
        if( !(c & UART_NO_DATA) ) {
            process(c);
        }
    }
}


static enum Protokoll_State_t protokoll_state = start;
static uint16_t bytes_in_current_state = 0;

Uart_Transfer_Application_t* next_layer[3];

uint8_t current_port;
uint8_t *current_buffer;
uint8_t buffersize;
uint8_t write_pointer;

void register_application(uint8_t port, Uart_Transfer_Application_t* application) {
    next_layer[port] = application;
}

void unregister_applicatoin(uint8_t port) {
    next_layer[port] = NULL;
}

#define BIT_TYPE_REQ    0x00
#define BIT_TYPE_ACK    0x08
#define BIT_TYPE_CONT   0x10

#define BIT_SUBTYPE_ACK 0x00
#define BIT_SUBTYPE_NACK 0x01
#define BIT_SUBTYPE_WAIT 0x02


/**
 * 
 * @param number the integer to be converted
 * @param buf is expected to be as large as needed 5 Byte max
 * 
 * @return the bytes accualy needed to transform i
 */
uint8_t eight_to_seven(uint32_t number, uint8_t* buf) {
    uint32_t tmp;
    tmp = number;
    
    uint8_t i = 0;
    while( tmp > 127 ) {
        i++;
        tmp = tmp >> 7;
    }
    for(int j = i; j >= 0 ; j--) {
        buf[j] = 0x7F & number;
        number = number >> 7;        
    }
    return i + 1;
}
    

/**
 * 
 * @param buffer musst have at least 10 byte
 */
uint8_t assamble_ack( uint8_t* buffer, uint8_t subtype, uint32_t info){
    buffer[0] = 0x80 | BIT_TYPE_ACK | subtype;
    uint8_t i = 1 + eight_to_seven( info, buffer + 1 );
    /* compute CRC */
    return i + 2;
}


static bool process(uint8_t c) {
    bool got_message = read_message(c);
    
    if(got_message) {
        char buf[10];
        switch( protokoll_state ){
            case start:
                if(message.type == request){
                    if(next_layer[message.m.req.port] != NULL) {
                        current_port = message.m.req.port;
                        uint32_t length;
                        Uart_Transfer_Application_t* upper_layer = next_layer[current_port];
                        uint8_t* buffer =  upper_layer-> get_buffer(&length);
                        uint8_t message_length = assamble_ack(buffer, 0, length);
                        current_buffer = buffer;
                        write_pointer = 0;
                        buffersize = length;
                        for(int i = 0; i < message_length; i++) {
                            uart_putc(buffer[i]);
                        }
                        
                        protokoll_state = wait_for_data;
                    }
                } else {
                    error();
                }
                break;
            case request_received:
                break;
            case process_message:
                break;
            case wait_for_data:
                if(message.type == content ) {
                    
                } else {
                    error();
                }
                break;
            case wait_for_next_message:
                break;
            case fin_wait:
                break;
                
            default:
                /* should never happen */
                error();
        }
    }
}

#define BIT_LONG_FORMATE    0x80
#define BIT_VERSION         0x60
#define BIT_TYPE            0x18
#define BIT_TYPEINFO    0x07

#define GET_VERSION( x )    ( ( x & BIT_VERSION ) >> 5 )
#define GET_TYPE( x )       ( ( x & BIT_TYPE ) >> 3 )
#define GET_TYPEINFO( x )    ( x & BIT_TYPEINFO )

struct Message_t message;

uint16_t state_counter;

static int read_message(uint8_t c){
    state_counter++;
    
    switch(message_state) {
        case ms_header:
            /* clear message before continue */
            memset(&message, 0, sizeof(message));
            if(c & BIT_LONG_FORMATE) {
                /* long message */
                uint8_t version = GET_VERSION(c);
                uint8_t type = GET_TYPE(c);
                message.type = type;
                switch(type) {
                    case 0:
                        message.m.req.port = GET_TYPEINFO(c);
                        if( message.m.req.port == 7 ) {
                            //TODO: support extended port formate
                            /* extended port */
                        } else {
                            message_state = ms_length;
                            state_counter = 0;
                        }
                        break;
                    case 1:
                        message.m.ack.subtype = GET_TYPEINFO(c);
                        message_state = ms_length;
                        state_counter = 0;
                        break;
                    case 2:
                        message.m.cont.packetnumber = GET_TYPEINFO(c);
                        message_state = ms_length;
                        state_counter = 0;
                        break;
                }
            } else {
                /* short message */
            }
            break;
        case ms_length:
            switch(message.type) {
                case request:
                    message.m.req.total_length = message.m.req.total_length << 7;
                    message.m.req.total_length |= ( 0x7F & c );
                    if( ! (0x80 & c) ) {
                        message_state = ms_crc;
                        state_counter = 0;
                    }
                    break;
                case ack:
                    break;
                case content:
                    break;
                case chat:
                    break;
                default:
                    /* should never happen */
                    error();
            }
            break;
        case ms_content:
            /* only posible for content and chat messages */
            break;
        case ms_crc:
            /* crc ok */
            if(state_counter == 2) {
                message_state = ms_header;
                state_counter = 0;
                return true;
            }
            break;
        default:
            /* should never happen */
            error();
    }
    return false;
}

static void error(void) {
    while(true)
        uart_putc('E');
}
