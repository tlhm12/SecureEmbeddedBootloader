

def eigth_to_seven(number):
    """converts an integer to base 128 = 2^7 """
    base_128 = bytearray()
    while(number > 0):
        tmp = number & 0x7f
        number = number >> 7
        if(len(base_128)):
            tmp = tmp | 0x80
        base_128 = tmp.to_bytes(1, byteorder ='big') + base_128
    return base_128


