#!/usr/bin/env python3
from message_creator import MessageCreator
import unittest


class TestRequest(unittest.TestCase):

    def test_creation(self):
        factory = MessageCreator()
        self.assertEqual(bytes([0x81, 0x84, 0x00, 0xBD, 0x9C]), factory.create_request(1, 512))
        self.assertEqual(bytes([0x87, 0x11, 0x88, 0x01, 0xF6, 0x83]), factory.create_request(17, 1025))


class TestAck(unittest.TestCase):

    def test_ack(self):
        factory = MessageCreator()
        self.assertEqual(bytes([0x88, 0x81, 0x00, 0x5F, 0x3A]), factory.create_ack(0, 128))


class TestContent(unittest.TestCase):

    def test_creation(self):
        factory = MessageCreator()
        content = "Hallo Welt".encode('ascii')
        self.assertEqual(bytes([0x93, 0x0A, 0x48, 0x61, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x65, 0x6C, 0x74, 0x2F, 0xCF]), factory.create_content(3, content))


class TestHelper(unittest.TestCase):

    def test_eight_to_seven(self):
        factory = MessageCreator()
        self.assertEqual(bytes([0x84, 0x00]), factory.eigth_to_seven(512))
        self.assertEqual(bytes([0x81, 0x80, 0x11]), factory.eigth_to_seven(16401))
        self.assertEqual(bytes([0xDB, 0x29]), factory.eigth_to_seven(11689))

if __name__ == "__main__":
    unittest.main()
