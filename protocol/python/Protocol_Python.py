#!/usr/bin/env python3
'''
Documentation, License etc.

@package Protocol_Python
'''

from message import Message
from message_creator import MessageCreator
from message_parser import MessageParser

def main():
    factory = MessageCreator()
    print("create request for port 1 with 512 byte payload")
    msg = factory.create_request( 1, 512 )
    print(":".join("{:02x}".format(c) for c in msg))

# script as advised by
# [google style guide](https://google.github.io/styleguide/pyguide.html#Main)
if __name__ == '__main__':
    main()
