import crcmod.predefined
from enum import Enum


class MessageCreator:

    class MessageTypes(Enum):
        REQUEST = 0
        ACK = 1
        CONTENT = 2

    def __init__(self):
        self.version = 0x00
        self.longformat = 0x80

        self.request = 0x00
        self.ack = 0x08
        self.content = 0x10

    def general_message(self, packagetype, package_info, extended_header, content):
        crc16 = crcmod.predefined.Crc('kermit')
        header = (self.longformat | ((packagetype << 3) & 0x18) | package_info & 0x07).to_bytes(1, byteorder='big')
        crc16.update(header)
        extended_header_bytes = self.eigth_to_seven(extended_header)
        crc16.update(extended_header_bytes)
        crc16.update(content)
        message = b''.join([header, extended_header_bytes, content, crc16.digest()])
        return message

    def eigth_to_seven(self, number):
        """converts an integer to base 128 = 2^7 """
        base_128 = bytearray()
        while(number > 0):
            tmp = number & 0x7f
            number = number >> 7
            if( len(base_128) ):
                tmp = tmp | 0x80 
            base_128 = tmp.to_bytes(1, byteorder ='big') + base_128
        return base_128

    def create_request(self, port, totallength ):
        crc16 = crcmod.predefined.Crc('kermit')
        if port > 6:
            if port > 256:
                raise ValueError("A Port greater than 256 was specified")
            header = (((0x80 | 7) << 8) | port).to_bytes(2, byteorder='big')
        else:
            header = (0x80 | port).to_bytes(1, byteorder='big')
        crc16.update(header)
        lengthbytes = self.eigth_to_seven(totallength)
        crc16.update(lengthbytes)
        message = b''.join([header,lengthbytes, crc16.digest()])
        return message

    def create_ack(self, subtype, info ):
        crc16 = crcmod.predefined.Crc('kermit')
        header = (0x80 | 0x08 | subtype).to_bytes(1, byteorder='big')
        crc16.update(header)
        infobytes = self.eigth_to_seven(info)
        crc16.update(infobytes)
        message = b''.join([header, infobytes, crc16.digest()])
        return message

    def create_content(self, packagenumber, content):
        crc16 = crcmod.predefined.Crc('kermit')
        packagenumber = packagenumber % 8
        header = ( self.longformat | self.version | 0x10 | packagenumber).to_bytes(1, byteorder='big')
        crc16.update(header)
        lengthbytes = self.eigth_to_seven(len(content))
        crc16.update(lengthbytes)
        crc16.update(content)
        message = b''.join([header, lengthbytes, content, crc16.digest()])
        return message
