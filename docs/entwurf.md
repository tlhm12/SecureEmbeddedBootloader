---
layout: page
title: Entwurf
---
# Entwurf

In diesem Kapitel wird der Entwurf des Gesamtsystems erläutert und auf spezifische
Herausforderungen bei der Implementierung auf der Versuchsplattform hingewiesen.
Die spezifischen Eigenheiten der Plattform werden in späteren Kapiteln genauer
ausgeführt.

## Ablauf des gesicherten Updateprozesses

Der gesicherte Updateprozess kann nach der folgenden Ablaufsequenz ablaufen. Dabei
werden sowohl der Sicherheit als auch den eingeschränkten Ressourcen des verwendeten
Controllers Rechnung getragen. Es kann dabei dazu kommen, dass im Falle eines Fehlers
oder einer nicht signierten Firmware das Gerät kein Anwendungsprogramm mehr startet.
Dies dürfte jedoch im Regelfall zu einem echten Fehler führen und leicht festzustellen
sein, da das Gerät z. B. per Feldbus nicht mehr antwortet oder seine Funktion eingestellt
hat. Anschließend kann eine autorisierte Person das Gerät durch einen erneuten
Flashvorgang wiederherstellen. Das Risiko eines nicht funktionierenden Gerätes
kann wohl im Regelfall als weniger kritisch eingestuft werden, als wenn ein Gerät
eine längere Zeit unbemerkt mit Schadsoftware infiziert ist.

Um sich zu vergegenwärtigen, dass im Zweifelsfall ein definierter Absturz besser
ist als eine nicht wie erwartet funktionierende Software, stelle man sich folgendes
Beispiel vor: Ein Embedded System in einer größeren
technischen Anlage überwacht an einer bestimmten Stelle mehrere Umgebungswerte wie
z. B. die Temparatur. Diese übermittelt sie per Feldbus an eine Steuerzentrale, welche
wiederum den gesamten Prozess so kontrolliert, dass an unserer Messstelle bestimmte
Werte nicht überschritten werden. Wird das Messsystem nun angegriffen und mit einer
Firmware versorgt, welche falsche Daten meldet, kann durch die Fehlinformation
eine Fehlsteuerung der Anlage eintreten. Diese könnte z. B. zu
Überhitzung führen, wodurch wiederum Personen-
und Sachschäden entstehen können. Wird das Gerät nun aber angegriffen, verwirft
es seine korrekte Software und bringt die Schadsoftware jedoch nicht zur Ausführung.
Somit übermittelt das Gerät keine Messdaten mehr an die Steuerzentrale. Das Ausbleiben
der Daten kann vergleichsweise schnell erkannt werden und Schäden können durch entsprechende
Maßnahmen vermieden oder zumindest minimiert werden. Denkbar sind hier
z. B. das Schalten in einen auf jeden Fall sicheren Betriebszustand.

Im Folgenden ist der mögliche Ablauf des Updateprozesses skizziert. @fig:activity-update-process
stellt veranschaulicht den Text in einem Aktivitätsdiagramm [@scheibl2016 Kapitel 12] dar.

-   Der Nutzer will eine Software flashen.
-   Der Bootlader wird aktiviert.
-   Der Nutzer überträgt sein eigenes Zertifikat zum Bootlader.
-   Der Bootlader prüft, ob das Nutzerzertifikat von einem bekannten Zertifikat signiert wurde
    und die Gültigkeitsdauer noch nicht abgelaufen ist (benötigt Realtimeclock).
-   optional: Der Bootlader generiert einen AES-Schlüssel und verschlüsselt diesen mit dem
    Nutzerzertifikat. Folglich kann niemand, der die Kommunikation mithört, den AES-Schlüssel
    sehen.

	*Hinweis*: Wird dieser Schritt ausgeführt, sind alle folgenden Schritte
    implizit mit diesem Schlüssel verschlüsselt.
-   Der Nutzer überträgt, mit seinem Schlüssel signiert, den kryptografischen Hashwert
    der Software an den Bootlader.
-   Der Bootlader empfängt die Software und berechnet laufend den Hashwert. Wenn der Speicher
    es zulässt, wird die Software erst zwischengespeichert bis die Verifikation abgeschlossen ist.
    Andernfalls werden schon Teilstücke der Software geflasht. In diesem Fall
    muss es ein disable-flag geben, welches bei noch nicht verifizierter Software
    gesetzt ist. Ansonsten könnte ein Angreifer den Ladeprozess infiltrieren
    und nach erfolgreicher Übertragung/Flashen den Strom abschalten oder die
    Prüfung anderweitig unterbrechen. Im Ergebnis hätte der Controller dann eine
    nicht verifizierte Anwendungssoftware geflasht, welche beim Neustart ausgeführt
    werden würde. Der Bootlader muss also zu Beginn des Flashvorgangs ein Verified-Flag
    auf false setzen und erst nach erfolgter Prüfung das Flag wieder auf true setzen.
    Beim Neustart wird im Bootlader das Flag geprüft und die Anwendungssoftware
    nur gestartet, wenn das Flag true ist.
-   Nachdem die gesamte Software übertragen ist, wird der berechnete Hashwert mit
    dem signiert übertragenen Hashwert verglichen. Sind die Hashwerte gleich, wird
    die Software akzeptiert und geflasht oder das Verified-Flag gesetzt. Sollte
    der Hashwert ein anderer sein, wurde die Software seit der Verifikation
    verändert und wird verworfen.

Zertifikat ist sowohl im Sinne eines X509 Zertifikates als auch im Sinne eines
OpenPGP Public-Keys zu sehen.

![Aktivitäten Diagram Updateprozess](activity_update_process.png){#fig:activity-update-process}

Während des gesamten Verlaufes wird auf dem Embedded System kein privater Schlüssel
benötigt. Man hat also nicht das Problem, dass das Embedded System entweder kryptografisch
gute und sichere Zufallszahlen zur Schlüsselerzeugung produzieren muss oder der Schlüssel auf
einem Fremdsystem erzeugt werden muss. Gute und sichere Zufallszahlen auf Embedded Systems zu
erzeugen ist im Regelfall ein Problem. Erzeugt man den Schlüssel auf einem Fremdsystem,
läuft man Gefahr, dass dieser geleakt wird.
In der Praxis dürfte das Problem mit geleakten Schlüsseln ein noch größeres sein,
da man aus Kostengründen den Herstellungsprozess einfach halten würde und im
Zweifel auf alle Geräte desselben Typs denselben Schlüssel flashen würde (siehe
[Philips Hue Hack][philips-hue-hack]).

[philips-hue-hack]: https://www.heise.de/security/meldung/Licht-an-Licht-aus-ZigBee-Wurm-befaellt-smarte-Gluehbirnen-3459004.html
