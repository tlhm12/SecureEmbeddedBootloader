---
layout: post
title: the responsibiliy of developers and users to deal with security
date: '2017-02-27 16:19'
categories:
  - Thoughts
  - Society
---
How should it be used to not deprive users right of decision. I beleave
the user should be able to choose freely how to use a device and what software
to use with it. The user should have the right to do so for several reasons

 1.  security: the user should be able to fix security issues
 2.  sustainability: the user should be able to use a given hardware to solve
     further tasks if he don't need it anymore for the original use-case.
     Often a hardware is suitable to solve a task but the software isn't.
     Why try to prevent the user from doing so? Only for economical reasons?
 3.  ethical: don't try to restrict the user by implementing security. Good
     security should not limit the freedom of the entity it tries to protect.
     If a user was able to use a device with custom software before security was
     implemented he should also be able afterwards.

 Well, you may have concerns about support. "I cann't provide support for this.
 The user could to everything.", you may say.

 *You don't have to provide support for everything!* You should provide support
 for your use-cases and maybe the interface to enable the user change software.
 If the user changes the software of the device the creator of the software is
 in charge to provide support or in doubt the user itself. Maybe we need a different
 ideology in society to not having users demand support were it couldn't be given
 but be more self-responsible. In my opinion it's a good thing to have people who
 take there reponsibilty for them, their devices and their contribution to the
 society. We should enable and encourage people to take resposiblity at least over
 their own devices and with them over part of their own and others privicy. We
 should not protect people from their self and lull them into a false sense of
 security.

 There might be another concern, a juristical and ethical concern. What if someone
 buys your device, uses the way you provided and probably supported to modify the
 use-case programmatically, and builds something you could not want. Maybe a weapon
 or a part of it, or something else that may harm people in some way and infringes
 a law. Are you guilty for what this person did cause your and your device enabled
 them to do it? I think no and the person would have done it anyway. Maybe with
 another technique but did it. If someone realy wants to harm people he will find
 a way to do so. You cann't realy stop the evil people to you may stop those good
 ones who pursue for sustainability, freedom, and a better society.

After all give people the freedom to modify your devices in software and maybe
also in hardware at their own risk and dont try to stop them doing so. Don't waste
time and efforts to try to protect your product from other use-cases than you
thought of.

In this sense: Let's build products that preserve the users responsibiliy and
encourage them to reuse products for sustainability.
