---
  title: Schluss
  layout: page
---

# Schluss

In diesem Schlusskapitel wird ein Fazit über die Arbeit gezogen, sowie einige
ethische Gedanken diskutiert, welche im Laufe der Arbeit aufkamen. Zu guter Letzt
gibt es einen Ausblick, aus das was noch entwickelt werden könnte.

## Fazit

Es ist wichtig das die intelligenten Geräte der Zukunft unsere Daten über Kryptographie
sichern. Vielleicht sogar wichtiger ist es jedoch das die Geräte sich nicht gegen
uns wenden. Dies kann verhindert werden, indem Angriffsvektoren geschlossen oder
zumindest verkleinert werden. Einen dieser Angriffsvektoren, die Geräte mit neuer
Firmware zu versorgen habe ich in dieser Arbeit diskutiert und bearbeitet. Ich halte
es für unumgänglich das es sich zum Standard entwickelt, dass Firmware signiert ist.
Diese Kontrolle sollte jedoch nicht nur für große, leistungsstarke Mikrocontroller
verwendet werden, sondern für alle. Genauso wie eine Kette nur so stark wie ihr
schwächstes Glied ist, so ist auch Sicherheit nur so gut wie der schwächste Punkt.
Da kleine intelligente Geräte in Zukunft in großer Zahl vorhandenen sein werden,
sollten wir uns ernsthafte Gedanken machen, wie diese gut und effizient abzusichern
sind. Ein Bootlader, welcher die Firmware sichert und auf möglichst vielen Plattformen
läuft, ist sicher ein Schritt in die richtige Richtung. Menschen sind häufig bequem.
Eine Sicherungsmaßnahme sollte weder den Nutzer noch den Entwickler einschränken,
da sie sonst mit hoher Wahrscheinlichkeit deaktiviert wird.

Ich habe bei meiner Recherche keinen Bootlader für so kleine Systeme gefunden,
welcher die Signatur der übertragenen Firmware geprüft hat. Nach Abschluss der
Arbeit kann ich sagen, dass für viele Anwendungen schlicht zu wenig Programmspeicher
übrig bleibt. Man muss also einen größeren Mikrocontroller verwenden um die Firmware
gesichert updaten zu können. Allerdings sehe ich gerade für sehr kleine Sensoren
einen Anwendungszweck. Sensoren, welche abgesetzt Umgebungswerte messen und übertragen,
und updatebar sein sollen. Um Werte zu messen und diese über ein Funknetz oder Feldbus
zu übertragen wird nicht viel Programmcode benötigt. Man kann jedoch auch darüber
Nachdenken ob der Sensor wirklich updatebar sein muss. Für eventuelle Protokolländerungen
oder Funktionserweiterungen könnte dies jedoch gewollt sein.

## Ethische Diskussion

In diesem Abschnitt möchte ich einige Gedanken zu potentiellen ethischen Problemen
mit gesicherter Firmware aufzählen.

So schön und wichtig Sicherheit auch ist, kann es allerdings auch verwendet werden
um den Nutzer zu entmündigen. Der Nutzer kann, wenn die Software signiert sein
muss eben nicht seine eigene Software aufspielen. Auch, wenn es heute schon üblich
ist dem Nutzer diese Freiheit abzusprechen und auch wenn es wohl die wenigsten
Nutzer wollen, so sollte man jedoch Möglichkeiten schaffen das Nutzer eigene
Software auf ihre Geräte bringen. Im besten Fall geschieht dies ohne die Sicherheit
zu deaktivieren. Eine Möglichkeit wäre z. B. dem Gerät einen zusätzlichen Button
ähnlich eines Resetbuttons zu geben, welcher nicht unabsichtlich betätigt werden
kann. Registriert der Bootlader das Drücken des Buttons, kann er z. B. ein neues
Root-Of-Trust Zertifikat annehmen und in den Speicher schreiben, wenn dies möglich
ist, oder ein für den aktuellen Flashvorgang unsigniertes Paket bzw. eine Signatur
erlaubt, welche nicht auf dem Root-Of-Trust verknüpft ist. Den Button sollte man
zusätzlich mit einem Siegel sichern, so das man Manipulationen zumindest erkennen
kann. Bricht der Nutzer das Siegel, um z. B. ein eigenes Zertifikat zu registrieren,
sollte es anschließend die Möglichkeit geben, dass der Nutzer den Button mit einem
eigenen Siegel neu versiegelt.

Eigene Software auf Geräten laufen lassen zu können halte ich für wichtig, weil
es andernfalls schwer ist, zu beurteilen ob das Gerät in seiner Blackbox den Nutzer
z. B. ausspioniert. Kann der Nutzer die Firmware mit eigener austauschen, hat er
die Möglichkeit diese auf Schwachstellen und Backdoors zu prüfen. Es sollte dem
Nutzer obliegen ob und wie weit er einem Hersteller vertraut. Man sollte als Nutzer
eher das physische Gerät, denn die Software kaufen.

## Ausblick

Nach der Arbeit ergeben sich einige weitere Themen, welche in Zukunft bearbeitet
werden können. RSA Schlüssel brauchen vergleichsweise viel Platz. Für die Arbeit
mussten Schlüssel verwendet werden, welche schon heute kaum noch als sicher gelten.
Elliptic Curve Cryptography könnte hier eine Lösung sein. Es müsste die Anwendbarkeit
von ECC auf so kleinen Prozessoren geprüft werden. Wenn der RAM-Verbrauch während
der Signaturprüfung größer ist als bei RSA bringt die Ersparnis beim Speichern nichts.

Der Bootlader könnte auf andere Systeme portiert werden. Wichtig dabei ist vor allem
das ein einheitliches Interface zum Flashen gefunden wird, also das PC-Programm
schlicht das gleiche Protokoll spricht. Somit könnte es möglich werden das der
Bootlader auf viele Systeme portiert wird, weil kein PC-Programm dafür geschrieben
werden muss.

Der Verifizierungsprozess für berechtigte Personen kann in einer Anwendung vereinfacht
werden. Der direkte Zusammenhang zwischen CA und Berechtigungs-Domäne kann dabei
durch ein gesondertes Datenformat aufgehoben werden. Durch eventuelle Schnittstellen
zu Verzeichnisdiensten kann die Nutzerfreundlichkeit auf der Administratorseite
erhöht werden. Berechtigungen können z. B. durch Gruppenzugehörigkeit vergeben werden.
Die Verifizierungsanwendung ist anschließend, z. B. über eine Weboberfläche für
jeden Mitarbeiter verfügbar. Der Mitarbeiter meldet sich an und lässt sein Zertifikat
erneuern. Die eingetragenen Berechtigungen und Gültigkeitsdauern setzen sich dann
aus den Gruppenzugehörigkeiten des Accounts, seiner Gültigkeitsdauer und einer
Standartgültigkeitsdauer für eine bestimmte Berechtigung zusammen.

Auf kleinen Systemen ohne Real Time Clock ist es, in Ermangelung einer Uhrzeit,
nicht möglich die Gültigkeitsdauer eines Zertifikats zu prüfen. Hierfür könnte man
[Trusted Timestamps][trusted-timestamp] verwenden. Der Bootlader erzeugt hierfür
einen Zufallswert, welcher an den PC-Übertragen wird. Die PC-Software kann anschließend
über ein Trusted Timestamp Protokoll, wie RFC 3161 oder X9.95, den Zufallswert
von einer vertrauenswürdigen Zeitquelle signieren lassen und wieder an den Bootlader
zurückübertragen. Der Bootlader kann anschließend die Signatur prüfen und den enthaltenen
Zeitstempel als aktuelle Uhrzeit ansehen. Die vertrauenwürdige Zeitquelle sollte
allerdings den Schlüssel der CA verwenden um die Zeitstempel zu signieren, da
mehr Schlüssel auch mehr Platz im Programmspeicher verbrauchen.

[trusted-timestamp]: https://en.wikipedia.org/wiki/Trusted_timestamping
