---
  title: Einleitung
  layout: page
---
# Einleitung

Eingebettete Systeme (engl. Embedded Systems) umgeben uns jeden Tag. In vielen
unserer Geräte sind kleine Computersysteme verbaut, welche oftmals vernetzt sind.
Wie bei ihren großen Brüdern, den PCs, sind auch diese Embedded Systems Ziel von
Angriffen. Da viele Embedded Systems mit der physischen Welt interagieren, haben
sie ein anderes Schadpotenzial im Falle eines erfolgreichen Angriffs. Stellen Sie
sich vor, ihr intelligenter Herd provoziert ein Feuer und ihr Smartphone gesteuertes
Türschloss lässt sie anschließend nicht mehr aus dem Haus.

## Motivation

Viele Geräte werden intelligenter. Solche Smart-Devices haben Software, die eventuell
upgedatet werden soll, und bieten deshalb entsprechende Schnittstellen an. Diese
Schnittstellen können z. B. UART, Feldbusse wie CAN oder auch ein LAN/WLAN, eine
Internetverbindung, Bluetooth oder ein Drahtloses Sensornetzwerk wie ZigBee sein.
Der Updateprozess muss hierbei besonders geschützt werden, da viele dieser Schnittstellen
prinzipiell ungeschützt sind und sich potentielle Angreifer durchaus leicht Zugang
zum entsprechenden Netz verschaffen können. Während bei einer UART-Verbindung vielleicht
noch ein Zugang zum Gerät notwendig ist, sieht es beispielsweise bei einer per CAN-Bus
kommunizierenden Schließanlage anders aus. Diese kann sich auch per CAN-Bus updaten
lassen.
Vor allem bei großen Installationen liegen Kabel oft in abgehangenen
Decken oder Kabelkanälen und ziehen sich über weite Bereiche. Es ist leicht vorstellbar,
dass es schwer ist, alle möglichen Zugangspunkte zur Hausverkabelung zu überwachen
und so sicherzustellen, dass der CAN-Bus physisch sicher ist.

Geräte, welche derart viel vernetzt sind, stellen oft beliebte Angriffspunkte dar
und sollen nur mit verifizierbarer Software upgedatet werden oder gar nur von
autorisiertem Personal mit neuer Software versorgt werden
können. Einige Embedded Bootlader bieten heute die Möglichkeit, verschlüsselte Firmware
zu flashen, mit einem Passwort gesichert zu werden oder die Firmware Signatur zu
überprüfen. Die verschlüsselte Firmware
dient dabei eigentlich eher dem Schutz des Intellektual Property, da der Anwender
zwar die Software selbst flashen, aber nicht disassemblieren kann. In der Praxis
stellt sich jedoch die Frage, ob man den Schlüssel zum Verschlüsseln der Firmware
geheim halten kann. Vorstellbar sind hier z. B. das Auslesen des Schlüssels aus verfügbaren
Geräten oder das Leaken z. B. durch Mitarbeiter. Ein Passwort
ist eine Möglichkeit, den Zugriff nur autorisierten Personen zu ermöglichen,
hat aber seine Schwächen. Das Passwort muss für die Übertragung ebenfalls ausreichend
geschützt werden, da ein mitlauschender Angreifer das Passwort sonst während eines
Updates mitschneiden kann. Wurde das Passwort ausreichend gegen Mitschneiden
gesichert, stellt sich das Problem, wie man z. B. ausscheidenden Mitarbeitern die
Berechtigung entziehen kann. Da die Authentisierung über ein Passwort erfolgt, das
auf dem jeweiligen Gerät gespeichert ist, müssten nun alle Passwörter geändert werden, um
einer einzelnen Person dauerhaft die Berechtigung zu entziehen.
Die dritte Möglichkeit ist sicher die bisher beste. Die Firmware wird gebaut
und anschließend von einer Trusted Instance signiert, dessen öffentlicher Schlüssel
dem Bootlader bekannt ist. Soll die Firmware nun geflasht werden, überprüft der
Bootlader die Signatur und flasht die Firmware nur, wenn diese von der Trusted
Instance signiert wurde. Dieses Prinzip ist bei Endnutzergeräten, welche in großen
Mengen verkauft werden, sicher auch praktikabel. Die Software wird geschrieben,
durchläuft anschließend einen Zertifizierungsprozess und wird am Ende mit dem privaten
Schlüssel des einen Zertifikats signiert.

Wenn es so einfach ist, stellt sich die Frage, warum es nicht jeder macht? Es
kann hierzu sicher die These aufgestellt werden, dass es einfach einer gewissen Bequemlichkeit
während der Entwicklung geschuldet ist und anschließend nicht aktiviert wird, weil
es eben funktioniert. Schwierig ist der zentrale Zertifizierungsprozess
wahrscheinlich bei individuellen Bedürfnissen. Man stelle sich hier eine Industrieanlage
im Bau vor, bei der der Prozess getestet und ggf. gebugfixt werden soll. Damit die
Ingenieure vor Ort die Möglichkeit haben, die Software zu fixen oder zu verbessern,
wird auf eine Verifizierung der Firmware verzichtet und am Ende, wenn alles funktioniert,
vergessen, diese einzuschalten.

An dieser Stelle
kann über signierte Zertifikate mit Gültigkeitsdauer nachgedacht werden. Jede berechtigte
Person bekommt ein von einer Root-of-Trust mit einer Gültigkeitsdauer ausgestelltes
Zertifikat. Beträgt die Gültigkeitsdauer z. B. 6 Monate, so verliert beispielsweise
ein ausscheidender Mitarbeiter nach spätestens dieser Zeit automatisch die Berechtigung,
Geräte zu updaten.

Die Mitarbeiter haben so eine Möglichkeit, auch während der Entwicklungs- oder Testphase
die Firmware schon signiert zu updaten, ohne einen unpraktischen Weg über eine zentrale
Instanz zu gehen, welche alles signieren muss.

Die Wahrscheinlichkeit, dass eine Sicherungstechnik eingesetzt wird, erhöht sich also auch
schon während der Entwicklungs- und Testphase und kann anschließend nicht einfach
vergessen werden.

## Zielsetzung

Ziel der Arbeit soll sein, die Grundlagen für die Entwicklung eines Embedded Bootloaders
zu schaffen, welcher die Authentizität der Software mittels Signaturen prüft. Die
Schlüssel, welche eine gültige Signatur erzeugen können und somit den Besitzer als
berechtigte Person identifizieren, sollen über eine Chain-of-Trust bestimmt werden.

Die Firmware sollte optional verschlüsselt werden können, um den Funktionsumfang
aktueller Bootlader zu bieten. Der Schlüssel für die Verschlüsselung sollte dynamisch
zwischen den Beteiligten ausgehandelt werden, damit keine Speicherung auf dem Embedded
System nötig ist.

Da die Erzeugung von sicheren kryptographischen Schlüsseln mit einigen Fallstricken
verbunden ist, sollte gut verfügbare und getestete Software zur Schlüsselerzeugung,
Signierung und Schlüsselverwaltung eingesetzt werden können.

## Aufbau der Arbeit

Die Arbeit gliedert sich wie folgt:

Als erstes werden Grundlagen geklärt, welche
für das Verständnis der restlichen Arbeit notwendig oder von Vorteil sind. Anschließend
werden die Anforderungen des Bootladers analysiert und bestehende Bootlader verglichen.
Die Analyseergebnisse werden in einem allgemeinen Entwurf für einen solchen Bootlader
weiterverarbeitet. Danach wird die Umsetzung auf der Versuchsplattform und die
damit einhergehenden Notwendigkeiten erläutert. Diese Notwendigkeiten sind in drei
größere Teile aufgeteilt. Diese drei Teile, Protokoll, dynamische
Bibliotheken und Kryptographie, sind jeweils unabhängig voneinander,
bedingen sich aber durch die gewählte Versuchsplattform. Jeder der drei Teile enthält
jeweils seine eigene Analyse, Entwurf und Implementierung.
