# Zielsystem {#sec:zielsystem}

Im Rahmen dieser Arbeit wird der Mikrocontroller Atmel Atmega 328
verwendet. Die verwendete Architektur ist die 8-bit AVR-RISC-Architektur.
Dieser Prozessor zeichnet sich durch seine sehr weite Verbreitung aus,
da er, beziehungsweise kompatible Controller, in den ersten Arduino-Modulen verbaut waren. Tabelle
@tbl:328_specs listet einige Leistungsmerkmale [@328_datasheet, 1-2] auf:

------  -------------
RAM     2 kB
Flash   32 kB
EEPROM  1 kB
Takt    bis zu 20 MHz
------  -------------

  : Spezifikationen Atmega AVR 328 (Auszug) {#tbl:328_specs}

Eine Besonderheit des Prozessors in Harvard-Architektur sind Instruktionen zum
Ändern des Programmspeichers. Diese lassen sich nur aus einem 4 kB großen Bootladerbereich
am oberen Ende des Flashspeichers ausführen.

Der Prozessor ist dabei auf einem Modularis® MOD000 Prototyping Board (siehe @fig:mod000)
mit angeschlossenem
DE_USB USB-Adapter verbaut.

![Modularis® MOD000 mit DE_USB und Programmieradapter](commons/mod000.png){#fig:mod000}

Für die Entwicklung und Programmierung des Bootladers auf den Prozessor
wird außerdem ein AVR ISP MKII (siehe @fig:avr_isp_mk2)
samt passendem Adapter auf
das MOD000 Board verwendet. Der AVR ISP MKII nutzt das ISP Protokoll, um das Programm
in den Programmspeicher zu schreiben.

![AVR ISP MKII](commons/avr_isp_mk2.png){#fig:avr_isp_mk2}

## Bootladerkompatibilität

Der Atmega 328 unterstützt die Verwendung von Bootladern durch spezielle Instruktionen
zum Ändern des als Flash ausgeführten Programmspeichers. Diese Instruktionen lassen
sich nur aus einem speziellen Bootladerbereich am oberen Ende des Programmspeichers
ausführen. Wie groß der Bootladerbereich ist, kann durch das Setzen so
genannter Fuses (engl. Sicherungen) zur Programmierzeit festlegt werden. Bis zu 4 kB
stehen hierfür zur Verfügung. Der Einsprungpunkt
nach dem Reset des Prozessors kann - ebenfalls durch Fuses - entweder auf den Beginn
des Flashs und oder auf den Beginn des
Bootladerbereichs gesetzt werden. Der Anfang des Flashspeichers ist üblicherweise auch der Start des Anwendungsprogramms.

## Toolchain und Standartbibliothek

Die zum Entwickeln eingesetzte avr-gcc-Toolchain [@avr-libc-manual, Kapitel 2.4] basiert auf der GCC-Suite, bietet jedoch
einige plattformspezifische Besonderheiten. Im Gegensatz zum GCC für x86-Systeme
unterstützt die AVR-Version keine dynamischen Bibliotheken. Die Programme werden
so kompiliert und gelinkt, dass sie ohne Betriebssystem direkt auf dem Prozessor
laufen. Die Programme können somit auch nicht auf Funktionen eines Betriebssystems
zurückgreifen.

Das durch avr-cc, avr-as und avr-ld erzeugte elf-executable wird durch avr-objcopy
in das Intel-Hex-Format gewandelt und mittels dem Programm avrdude und einem Programmer
in den Speicher des Controllers geschrieben.

Als Standartbibliothek steht unter anderem die avrlibc zur Verfügung, welche ebenso
wie der avr-gcc als Open-Source-Projekt entwickelt wird. Die avrlibc versucht die
C-Standartbibliothek so gut wie möglich zu implementieren, hat aber an einigen
Stellen wie z. B. den Standard-IO-Streams stdin, stdout und stderr Unterschiede.
Dies ist notwendig, da Embedded Systems keine einheitliche Nutzerschnittstelle
in Form von Tastatur und Bildschirm haben und Ein- und Ausgabe von Anwendung zu Anwendung
individuell ist.
Beispielsweise wäre es denkbar stdin und stdout über UART zu realisieren
und für stderr wiederum ein per SPI angeschlossenes Display zu verwenden. Da die Verfügbarkeit
und konkrete Verwendung der unterschiedlichen Kommunikationskanäle von der vorgesehen
Nutzung, der Software und der verwendeten Hardware abhängt, muss man diese z. B.
vor Benutzung mit einem entsprechenden Gerätetreiber zuweisen.

## Datenübertragung

Die Daten werden im Versuchsaufbau über die weit verbreitete serielle Schnittstelle,
kurz UART, übertragen. Bei dem verwendeten Prototyping-Board Modularis® MOD000 sind
über die Steckverbindungen die CTS[^cts]- und RTS[^rts]-Signale,
welche für eine Hardware Flusssteuerung benötigt würden, nicht verfügbar. Will man
eine Flusssteuerung nutzen, um z. B. einen Pufferüberlauf und damit Datenverlust
zu vermeiden, muss auf eine Softwarelösung zurückgegriffen werden.

[^cts]: Clear to Send
[^rts]: Ready to Send

Flusssteuerung
  : Eine Flusssteuerung stellt sicher, dass gesendete Daten auch vom Empfänger verarbeitet
    werden können. Man unterscheidet bei der seriellen Schnittstelle zwischen Hardware-
    und Software-Flusskontrolle.

    Hardware-Flusskontrolle wird über
    die beiden zusätzlichen Signale CTS[^cts] und RTS[^rts],
    welche Empfangsbereitschaft bzw. Sendewillen anzeigen, realisiert.

    [Software-Flusskontrolle][softwareflowcontrol-wikipedia]
    wird üblicherweise über die ASCII-Zeichen 0x11 (XON) und 0x13 (XOFF) implementiert.
    Da man hierdurch den entsprechenden Zeichen eine besondere Bedeutung zukommen lässt,
    müssen die Nutzdaten ggf. so kodiert werden, dass diese Zeichen darin nicht vorkommen.

Weder die Hardware noch die Softwareflusssteuerung bieten eine Kontrolle der Daten.
Der sehr hohe Übertragungs-Overhead bei einer Softwareflusssteuerung ist ein deutlicher
Nachteil. Eine dritte Möglichkeit bietet ein paketorientiertes Protokoll. Der Sender
schickt ein Paket und wartet anschließend auf eine Bestätigung des Empfängers, bevor
ein neues Paket gesendet wird.

Nach kurzer Internetrecherche scheint der einzige Defakto-Standard für die serielle
Schnittstelle das Protokoll XModem bzw. seine Abwandlungen und Weiterentwicklungen
zu sein. Laut [Wikipedia][xmodem-wikipedia] ist das Protokoll heutzutage vor allem
für Systemaktualisierungen beliebt. XModem ist jedoch für die vorliegende Anwendung
nicht tauglich, da es immer Blöcke von 128 Byte versendet und diese Datenmenge
somit zwingend zwischengespeichert werden müsste. Die Verwendung von exakt 128
Byte großen Blöcken trägt dem begrenzten Speicher von 2 kB auf dem Versuchsprozessor
keine Rechnung.

Der Bootlader soll auch mit anderen Schnittstellen wie I²C oder CAN funktionieren.
Es gibt zwar eine Vielzahl an Protokollen für den CAN-Bus, darunter auch welche,
die Nutzdaten übertragen können, die größer als der CAN-Frame (8 Byte Nutzdaten)
sind. Für I²C scheint es kein Protokoll zu geben.

Es wird deshalb ein eigenes Protokoll zur Flusssteuerung implementiert, welches
den Erfordernissen der geringen Speicher- und Verarbeitungskapazitäten besser
entspricht.

[softwareflowcontrol-wikipedia]: https://en.wikipedia.org/wiki/Software_flow_control
[xmodem-wikipedia]: https://de.wikipedia.org/wiki/XModem

### Anforderungen

Für das zu entwickelnde Übertragungsprotokoll kann man unter den gegebenen Bedingungen
folgende Anforderungen formulieren.

Übertragung ohne zusätzliche Kodierung
  : Die Übertragung, auch von Binärdaten, soll ohne eine Kodierung wie einer Hexadezimalnotation oder Base64 auskommen, somit möglichst schnell sein und auch den
    leistungsschwachen Prozessor während der Übertragung mit möglichst wenig zusätzlichem
    Kodieraufwand belasten.

Kleine oder dynamisch festlegbare Paketgrößen
  : Die Paketgröße sollte entweder klein sein, damit Pakete auch bei sehr wenig freiem
    RAM noch empfangen und verarbeitet werden können, oder anpassbar sein.
    Eine Möglichkeit, Paketgrößen anzupassen, ist diese, ähnlich dem [Receive Window][receivewindow-wiki]
    von TCP, dynamisch auszuhandeln.
    Damit ist es möglich, auf Kommunikationspartner in unterschiedlichen
    Situationen und unterschiedlicher Speicherkapazität zu reagieren.

Prüfung auf Übertragungsfehler
  : Die Übertragung sollte mittels eines geeigneten Sicherungsverfahrens, z. B.
    eines Cyclic Redandancy Checks (CRC), auf Fehler prüfbar sein.

Rückmeldung
  : Werden Daten empfangen, soll der Empfänger dem Sender eine Rückmeldung geben,
    ob die Daten korrekt empfangen wurden oder ob eine Neuübertragung notwendig
    ist. Aus der Rückmeldung sollten sich außerdem Informationen darüber ableiten
    lassen, ob direkt weitere Daten gesendet werden können oder ob die Daten erst
    verarbeitet werden müssen.

[receivewindow-wiki]: https://de.wikipedia.org/wiki/TCP_Receive_Window

### Entwurf

Aus den Anforderungen lassen sich einige Entwurfsentscheidungen ableiten.

1. Es gibt einen Verbindungsaufbau, in welchem der Sender seinen Sendewunsch mitteilt.
2. Ist der Empfänger bereit, teilt er seine mögliche Paketgröße mit.
3. Der Sender überträgt pro Paket zusätzlich eine Paketgröße und Sicherungsinformationen.
4. Der Empfänger bestätigt den Empfang oder verneint ihn, ggf. mit einer Warteinformation.
5. Es gibt eine Möglichkeit, kleine Daten mit reduziertem Overhead zu übertragen.

Es gibt zwei verschiedene Formate: Eine Langform, welche die Übertragung von beliebig
langen Daten ermöglicht und eine Kurzform, welche eine Kommunikation mit bis zu 7
Byte Nutzdaten und reduziertem Overhead ermöglicht.

Die beiden Formate werden anhand des obersten Bits im ersten Headerbyte unterschieden (siehe @tbl:formatbit)
und haben ansonsten ein unterschiedliches Headerformat. Allgemein ist ein Paket
immer wie in @lst:packageformate aufgebaut. Ob es einen Extended Header gibt, hängt
dabei vom konkreten Paket ab.

```{#lst:packageformate caption="Allgemeines Paketformat" }
+----------------+-----------------+-----------+-------+
| 1. Byte Header | Extended Header | Nutzdaten | CRC   |
+----------------+-----------------+-----------+-------+
```

Wert | Bedeutung
-----|-----------
 0   | Kurzformat
 1   | Langformat

  : Bedeutung Höchstwertiges Bit des ersten Headerbytes {#tbl:formatbit}

Das Design der Nachrichten entlehnt einige Vorgehensweisen von Spezifikationen wie
dem OpenPGP-Nachrichtenformat (@rfc4880) und dem DER-Encoding (@x690). Besonders
sind z. B. die Form von bestimmten Angaben - wie Größen - von diesen Spezifikationen
inspiriert.

Es können in beiden Formaten durch Portnummern unterschiedliche Dienste adressiert
werden. Das Konzept der Portnummern
ist hier von der TCP/IP-Adressfamilie entlehnt und folgt einer ähnlichen Logik.

Da es sich um ein Protokoll für Embedded Systems handelt, dürfte wohl kaum ein System
mehr als drei Dienste anbieten. Vorstellbar sind z. B. ein Managementdienst - z. B. zum
Neustarten, Updaten und Ähnlichem - und ein Dienst zum Abfragen bzw. Steuern von Sensoren
und Aktoren.

#### Kurzformat

Das Kurzformat oder Chatformat dient insbesondere der Reduzierung des Overheads bei sehr kurzen
Nachrichten. Sehr kurze, aber gleichzeitig häufige Nachrichten sind z. B. beim Abfragen
von Sensordaten und Schreiben von Aktordaten denkbar.

``` {#lst:packageformate_short caption="Aufbau einer Kurzformat Nachricht"}
+-----------------+------------+--------+
| 7 6 5 4 3 2 1 0 | Max 7 Byte | 1 Byte |
+-----------------+------------+--------+
| 0 [1] [2] [ 3 ] | Payload    | CRC    |
+-----------------+------------+--------+
```

1. Sendefolgenummer
2. Portnummer
3. Länge

Kurzformat-Nachrichten können bis zu 7 Byte Nutzdaten übertragen und werden mit
einem CRC-8 gesichert. Es lassen sich insgesamt vier Portnummern (0 - 3) adressieren.
Die Sendefolgenummer wird benötigt um Neuübertragungen zu erkennen. Dies ist in
@sec:contentpackage ausführlicher erläutert.

![Zustandsautomat Kurzform](fsm_short_message.png){#fig:fsm_short_message}

#### Langformat

```{#lst:packageformate_long caption="Aufbau einer Nachricht im Langformat" }
+-----------------+-----------------+-----------+--------+
| 7 6 5 4 3 2 1 0 | Max 6 Byte      |           | 2 Byte |
+-----------------+-----------------+-----------+--------+
| 1 [1] [2] [ 3 ] | Extended Header | Nutzdaten | CRC    |
+-----------------+-----------------+-----------+-------++
```

Bit   | Funktion/Bedeutung
------|------------------
1     | Version zZt. 0
2     | Pakettyp
3     | Pakettyp spezifisch

 : Zuordnung Header Bits {#tbl:header_bits}

Der Aufbau von Extended Header und die Existenz von Nutzdaten ist für jeden Pakettyp
unterschiedlich und kann den Beschreibungen der einzelnen Pakettypen entnommen
werden.

![Zustandsautomat Langformat](fsm_long_message.png){#fig:fsm_long_message}

Mit den 2 Bit, welche im Header für den Pakettyp reserviert sind, werden drei
verschiedene Typen unterschieden.

Bitmuster | Typ
----------|-----------
00        | Request Communication
01        | Ack/Nack/Wait
10        | Content Package
11        | Reserviert

  : Mögliche Bitmuster in Typidentifier des Headers {#tbl:bitpatterntypes}

Im Folgenden werden die Nachrichtenformate der einzelnen Typen näher erläutert.

##### Request Communication {#sec:request}

Mit diesem Paket initiiert der Sender eine Verbindung und teilt seinen Sendewunsch
mit. Der Sendewunsch kann vom Emfänger abgelehnt werden. Beide Kommunikationspartner
können gleichberechtigt Sendewünsche äußern.

```{#lst:requestpackage caption="Aufbau eines Request Pakets"}
+-----------------+---------------+------------+--------+
| 7 6 5 4 3 2 1 0 |  1 Byte       | Max 5 Byte | 2 Byte |
+-----------------+---------------+------------+--------+
| 1 [1] 0 0 [ 2 ] | Extended Port |   Länge    |  CRC   |
+-----------------+---------------+------------+--------+
```

1. Version zZt. 0
2. "Portnummer" bzw. Dienst

Portnummern
  : Portnummern werden im Header durch 2 Bit dargestellt. Der höchste darstellbare
    Wert 3 hat dabei eine besondere Bedeutung und zeigt an, dass die Portnummer im
    Langformat vorliegt. Es lassen sich also die ersten drei Nummern 0-2 im 1-Byte-
    Header ausdrücken. Will der Kommunikationspartner höhere Ports adressieren,
    trägt er den
    speziellen Wert 3 ein und gibt den Port im Extended-Port-Bereich an.

Extended Port
  : Der Extended-Port-Bereich ist ein optionaler Bereich, in welchem man Portnummern
    größer als zwei angeben kann. Es darf maximal 1 Byte verwendet werden. Es können
    also insgesamt 256 verschiedene Ports adressiert werden.

Länge
  : Die Länge kann bis zu 5 Byte lang sein. Jedes Bit trägt dabei 7 Bit Informationen
    1 Bit Metadaten um das Ende der Längeninformation erkennen zu können. Das
    oberste Bit ist 1, wenn noch weitere Bytes folgen, bzw. 0, wenn es sich um
    das Letzte Byte handelt. Auf diese Weise kann auf ein zusätzliches Byte zum
    Übertragen der Länge der Längeninformation verzichtet werden.

##### Ack/Nack/Wait

Dieser Pakettyp dient dem Antworten auf Request und Content Pakete und bietet
insgesamt drei Unterpakettypen.

```{#lst:ackpackage caption="Aufbau eines Acknolage Pakets"}
+-----------------+-------------+--------+
| 7 6 5 4 3 2 1 0 | Max 5 Byte  | 2 Byte |
+-----------------+-------------+--------+
| 1 [1] 0 1 [ 2 ] | [    3    ] |  CRC   |
+-----------------+-------------+--------+
```

1. Version zZt. 0
2. Untertyp
3. Untertyp spezifisch z. B. Länge oder Zeit

Bitmuser | Untertyp      | Beschreibung
---------|---------------|--------------
00       | Acknolage     | Paket erfolgreich empfangen
01       | Not Acknolage | Paket fehlerhaft empfangen
10       | Wait          | Paket erfolgreich empfangen, Verarbeitung dauert an
11       | Reserviert    |

  : Mögliche Bitmuster für Untertypen

Die drei möglichen Untertypen Ack, Nack und Wait stellen drei Antwortmöglichkeiten
auf das vorher empfangene Paket dar.

Ack bestätigt den erfolgreichen Empfang und
signalisiert Bereitschaft für das nächste Paket. Ein Ack-Paket sendet maximale
Größe des nächsten Pakets mit. Auf diese Weise kann ein eventuell vorhandener Puffer
voll ausgefüllt werden, auch wenn die vorherigen Paketgrößen keine Teiler der Puffer
größen waren.

Mit Nack teilt der Empfänger mit, dass das vorherige Paket fehlerhaft war und neu
gesendet werden muss oder der Verbindungsversuch per Request-Paket abgelehnt
wurde. Eine Zusatzinformation ist für diesen Untertyp nicht vorgesehen.

Das Wait-Paket versendet der Empfänger ebenfalls als Bestätigung für den erfolgreichen
Empfang des vorherigen Pakets. Im unterschied zum Ack-Paket wird allerdings kein
nächstes Paket angefordert, sondern mitgeteilt das die aktuelle Verarbeitung noch
Zeit in Anspruch nimmt. Die Zusatzinformation enthält die zu erwartende Zeit, bis
die Verarbeitung abgeschlossen ist. Der Versender, der diese Nachricht erhält kann
die Information nutzen um seine Timeouts anzupassen. Besonders sinnvoll ist dieser
Typ, wenn die Verarbeitung sehr lang dauert. Dies kann z.B. beim Verarbeiten von
kryptographischen Funktionen oder dem Schreiben auf Datenspeicher der Fall sein.
Nach erfolgreicher Verarbeitung sendet der Empfänger ein reguläres Ack-Paket an
den Sender um das nächste Paket anzufordern.

##### Content Package {#sec:contentpackage}

Das Content-Paket dient dem Übertragen von großen Nutzdaten. Und wird nach einem
erfolgreichen Request-Ack-Verbindungsaufbau gesendet.

```{#lst:contentpackage caption="Aufbau eines Content Pakets"}
+-----------------+-------------+-----------+--------+
| 7 6 5 4 3 2 1 0 | Max 5 Byte  |           | 2 Byte |
+-----------------+-------------+-----------+--------+
| 1 [1] 1 0 [ 2 ] | [    3    ] | Nutzdaten | CRC    |
+-----------------+-------------+-----------+--------+
```

1. Version zZt. 0
2. Paketnummer
3. Länge

Paketnummern sind Notwendigkeit um ein wiederholt gesendetes Paket zu erkennen.
Dies kann vorkommen, wenn der Empfänger den Empfang mittels Ack bestätigt dieser
aber nicht ankommt. Der Sender wird seine Nachricht, nach Ablauf des Timeouts,
erneut senden. Der Empfänger wartet allerdings schon auf das nächste Paket und braucht
eine Möglichkeit den erneuten Versand des vorherigen Pakets zu detektieren.

Die Längeninformation enthält die Anzahl der Bytes an Nutzdaten. Die Länge ist genauso
kodiert wie in @sec:request beschrieben.

#### Prüfsumme

Die im Versuch verwendete Bitübertragungsschicht UART/RS-232 bietet eine gewisse
Sicherheit durch zufällige Bitfehler in Form des Paritätsbits. Das Paritätsbit ist
nur 1 Bit lang und wird lediglich aus der Summe der 8 Datenbits gebildet. Mehrere
Fehler können sich also sehr leicht gegenseitig aufheben. Die Verwendung des Paritätsbits
ist nicht obligatorisch. Wenn beide Kommunikationspartner das Bit weder senden
noch erwarten, wird es nicht für eine erfolgreiche Kommunikation benötigt. Die
meisten Programme für die serielle Schnittstelle haben hierfür Einstellungsmöglichkeiten.
Wird durch das Paritätsbit ein Fehler erkannt, ist kein weiteres Verfahren spezifiziert.
Die serielle Schnittstelle sendet fehlerhafte Bytes nicht automatisch neu. Es ist
Aufgabe der Anwendung, bei der Gegenstelle die Bytes noch einmal anzufordern.

Aufgrund der vielen Unklarheiten und Unsicherheiten auch bzgl. anderen potentiellen
Schnittstellen soll das Protokoll eigene Sicherungsverfahren spezifizieren. Wird
ein Übertragungsfehler erkannt, kann eine gezielte Neuübertragung angefordert werden.

Als Sicherungsverfahren ist es möglich, eine einfache Prüfsumme zu erzeugen. Eine
bessere Möglichkeit bietet jedoch die Verwendung von Cyclic-Redancancy-Checks (CRCs).

Einige moderne Mikrocontroller haben dedizierte Periferieeinheiten, um CRCs zu berechnen.
Der, auch an der THM in der Lehre (Mikroprozessortechnik) eingesetzte, TI MSP430
bietet beispielsweise diese Funktion in einigen Varianten [@msp430_datasheet, Kapitel 14].
Die CRC-Einheit des MSP430 erzeugt den CRC mit dem Polynom @eq:crc16ccitt, das auch
als [CRC16-CCITT][crc16-ccitt] referenziert wird.

$$x^{16} + x^{12} + x^5 + 1$$ {#eq:crc16ccitt}

Werden nur sehr kleine Nachrichten ausgetauscht, z. B. durch Nutzung des Kurzformates,
wird die Effizienz der Übertragung durch einen langen CRC-Wert merklich eingeschränkt.

Der 2 Byte große CRC16 am Beispiel einer mit 7 Byte ausgelasteten Kurzformnachricht
hat eine Effizienz von 70% (siehe @eq:shortlengthefficiancy). Wird der CRC
auf 1 Byte reduziert, kann die Effizienz um 7,77% gesteigert werden (@eq:shortlengthefficiancy2).
Für den Fall, dass nur 1 Byte Nutzdaten übertragen werden, steigt die Effizienz
um 8,33% (Siehe @eq:shortlengthefficiancy3 und @eq:shortlengthefficiancy4) und die
gesamte Nachricht wird von 4 auf 3 Byte, also um 25%, verkürzt.

$$\begin{aligned}
1 Byte Header + 2 Byte CRC &= 3 Byte Overhead\\
7 Byte Nutzdaten + 3 Byte Overhead &=  10 Byte gesammte Nachricht\\
\frac{7 Byte}{10 Byte} * 100 &= 70\%
\end{aligned}$$ {#eq:shortlengthefficiancy}
$$\frac{7 Byte}{9 Byte} * 100 = 77,77\%$$ {#eq:shortlengthefficiancy2}
$$\frac{1 Byte}{4 Byte} * 100 = 25\%$$ {#eq:shortlengthefficiancy3}
$$\frac{1 Byte}{3 Byte} * 100 = 33,33\%$$ {#eq:shortlengthefficiancy4}

Um bei kleinen Nachrichten die Effizienz etwas zu steigern, wird für die Kurzform
ein 8-Bit-CRC genutzt. Laut einem Paper von Philip Koopman [@crc_embedded, Seite 5]
ist das Polynom C2 (@eq:crc_c2) etwas besser als das als CRC-8 ITU-T beschriebene
Polynom (@eq:crc8). Auf seiner [eigenen Webseite][koopman-crc]
findet sich jedoch das CRC-8 ITU-T Polynom gleichauf mit dem C2 Polynom. Es taucht sogar
als bestes Polynom bis zu einer Datenlänge von 119 Bit (14,875 Byte) auf. Auf Grund
der weiteren Verbreitung soll hier das CRC-8 ITU-T Polinom (@eq:crc8) gewählt werden.
Dieses wird laut dem CRC-Katalog [@crc_catalogue] in 3 von 17 CRC-Spezifikationen genutzt.
Darunter sind auch Intels SMBus 2.0 und eine ITU Recommondation.

$$x^8 + x^2 + x + 1$$ {#eq:crc8}
$$x^8 + x^5 + x^3 + x^2 + x + 1$$ {#eq:crc_c2}

Das CRC-8 Polynom wird in der Interpretation der SMBus Spezifikation verwendet.
Die Daten werden als Most-Significant-Bit First interpretiert, mit 0x00 initialisiert
und das Ergebnis nicht durch eine XOR (Exklusiv Oder) Funktion verändert [@crc_catalogue; @crc_guide].

[koopman-crc]: https://users.ece.cmu.edu/~koopman/crc/index.html
[crc16-ccitt]: http://reveng.sourceforge.net/crc-catalogue/16.htm#crc.cat.kermit

### Implementierung

In den Zustandsdiagrammen (@fig:fsm_short_message und @fig:fsm_long_message) sind
einige Timeout-Übergänge aufgeführt.
Diese Übergänge sind in der ersten Proof-of-Concept Implementierung nicht ausimplementiert.
