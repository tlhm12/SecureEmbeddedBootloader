---
title: Analyse
layout: page
---
# Analyse

In diesem Kapitel werden die eventuell bereits vorhandenen Bootlader behandelt
und eine Anforderungsanalyse durchgeführt.

## Recherche

Es gibt einige Bootloader für Microcontroller, welche teilweise auch mit Busnetzen
umgehen können. Im Folgenden sind für die beiden Mikrocontroller-Familien Microchip
PIC und Atmel Atmega AVR einige Beispiele aufgeführt. Man kann für diese und andere
Controller noch mehr Beispiele für Bootlader finden. Eines ist jedoch allen
Bootladern für Mikrocontroller bis 16 bit gemeinsam: Sie unterstützten keine
Verifikation der Firmware über asymmetrische Kryptographie. Erst Bootlader
für größere Mikroprozessoren wie ARM Cortex-M bieten teilweise diese Funktion.

### Microchip

Das Unternehmen Microchip, welches aktuell (August 2017) auch die
Atmel Atmega Mikrocontroller vertreibt und produziert, liefert einen [Bootlader][microchip-8-bit],
welcher über die Schnittstellen UART, I²C, USB und Ethernet mit einem PC kommunizieren
kann. Im Rahmen der Arbeit war es nicht möglich, zum Test einen Bootlader zu erzeugen,
da die angebotene Java Software "UnifiedHost" mit der Fehlermeldung
`Fehler: Hauptklasse toplevel.TopLevel konnte nicht gefunden oder geladen werden`
nicht startbar war. Es ist jedoch zu vermuten,
dass die erzeugte Software keine freie Software ist und nur mit Produkten des Unternehmens
eingesetzt werden darf. Diesen Standpunkt findet man auch in anderen Lizenzvereinbarungen
von Microchip. So finden sich Aussagen wie "For purposes of clarity, Licensee may NOT
embed the Software on a non-Microchip Product, except as described in this Section."
[@microchip_license, Seite 1]
in einigen Lizenzvereinbarungen des Unternehmens. Unter anderem fand sich bis etwa 2014 auch im [TCP/IP Stack][microchip-tcpip]
des Unternehmens eine ähnliche Vereinbarung. Dessen aktuelle Lizenzierung konnte im Rahmen der Arbeit nicht
geprüft werden.

[microchip-8-bit]: http://www.microchip.com/promo/8-bit-bootloader
[microchip-tcpip]: http://www.microchip.com/SWLibraryWeb/product.aspx?product=TCPIPSTACK

### AVR-Community

Die AVR Community hat mehrere Bootlader für die Atmega-AVR-Serie veröffentlicht.
Teilweise sind diese jedoch nicht mehr verfügbar. So z. B. bei fast allen verlinkten
I²C Bootladern in einem [mikrocontroller.net Thread][i2c-bootloader-thread].
Des Weiteren kann man hierzu z. B. noch [optiboot][], welcher auf Arduinos zum Einsatz
kommt, erwähnen. Der Wikieintrag [AVR Bootloader in C][bootloader_in_c] beschreibt
z.B. die Vorgehensweise und diente auch als Anhaltspunkt für diese Arbeit [@avr_bootloader_anleitung].

[i2c-bootloader-thread]: https://www.mikrocontroller.net/topic/12173
[optiboot]: https://github.com/Optiboot/optiboot
[bootloader_in_c]: https://www.mikrocontroller.net/articles/AVR_Bootloader_in_C_-_eine_einfache_Anleitung

## Anforderungen

Begriffsdefinitionen im Rahmen der Arbeit:

System
  : Das zu entwickelnde Softwaresystem aus Bootlader, kryptografischer Überprüfung
    und einer Software auf PC

Hardware
  : Das Embedded System samt der darauf vorhandenen Software

Firmware
  : Anwendungsprogramm des Embedded Systems

Busnetz
  : Feldbusse wie z. B. I²C oder CAN

Flashen
  : Das Speichern von Programmdaten im Flashspeicher

### Funktionale Anforderungen

Überprüfen einer Chain-of-Trust
  : Basierend auf einem bekannten öffentlichen Schlüssel, welcher als Root-of-Trust
    agiert, soll das System übermittelte Schlüssel zu einer Chain-of-Trust verbinden
    können, um Verifikationen vornehmen zu können.

Persistieren der übermittelten Firmware
  : Die übertragene Firmware soll nach erfolgreicher Überprüfung gespeichert werden
    und die vorherige Firmware ersetzen.

### Nicht-Funktionale Anforderungen

Lauffähig auf 8-bit-Mikrocontrollern
  : Wenn das System auf Hardware mit sehr wenigen Ressourcen läuft, kann
    es einfacher auf leistungsfähigere Hardware portiert werden. Anders herum ist
    dies jedoch oftmals nicht möglich.

Nutzung von Busnetzen und anderen Kommunikationsschnittstellen
  : Die Übertragung der signierten Firmware und zusätzlich benötigten Informationen
    wie Schlüssel soll über ein Busnetz oder anderen für Embedded Systems und
    Mikrocontroller üblichen Kommunikationsschnittstellen erfolgen.


### Akteure

Es können für das System mehrere Akteure identifiziert werden.

Berechtigungskontrollierende Instanz
  : Es gibt eine Instanz, welche den Root-of-Trust-Schlüssel kontrolliert. Diese
    Instanz signiert andere Schlüssel - zertifiziert sie - und
    bestätigt damit, dass der Inhaber des signierten Schlüssels berechtigt ist, den
    Updateprozess durchzuführen. Es kann sich dabei um eine natürliche Person,
    Funktion wie Administrator, juristische Person oder anderes handeln.

Nutzer
  : Ein Nutzer im Sinne der Arbeit ist jemand, der versucht, mithilfe des Bootladers
    eine neue Firmware zu flashen. Der Nutzer kann berechtigt sein oder auch
    nicht.

### Anwendungsfälle

Siehe @fig:use-case

- Firmware flashen
- Root-of-Trust Key ändern

![Use-Case-Diagramm](use-case.png){#fig:use-case}
