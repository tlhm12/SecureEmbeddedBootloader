---
title: cryptography
layout: page
---
# Kryptographie {#sec:kryptographie}

In diesem Kapitel soll erläutert werden, welche Signatur-, Verschlüsselungs- und
Hashalgorithmen im Rahmen dieser Arbeit verwendet werden. Die Entscheidungen
für oder gegen bestimmte Algorithmen und Technologien werden dabei natürlich von
den begrenzten Ressourcen der Zielhardware beeinflusst. Ebenso sollen möglichst
viele vorhandene, erprobte und weiterhin gepflegte Softwarebibliotheken und -produkte
zum Einsatz kommen. Die Verwendung von erprobter und gepflegter Software bietet
den Vorteil das gerade in dem sensiblen Feld der Kryptographie eigene Implementierungsfehler
vermieden werden, aber auch Softwarefehler behoben werden und man die Bibliothek
eventuell durch eine Version ersetzen kann. Alle Bibliotheken und Software im
Bereich der Kryptographie sollen für dieses Projekt freie Software oder mindestens
quelloffen sein. Die Quelloffenheit ermöglich eine Kontrolle auf Schwachstellen
durch möglichst viele unabhängige Personen und dient somit gerade in der Kryptographie
der Qualitätssicherung in Bezug auf Implementierungsfehler.

## Analyse und Entwurf

In diesem Abschnitt werden die vorhandenen Verfahren erörtert und auf ihre Tauglichkeit
hin untersucht und ausgewählt.

### Wahl des Algorithmus

Es gibt einige Algorithmen für asymmetrische Kryptographie, vor allem für Signaturen. Um
einige zu nennen:

- [RSA][rsa-paper]
- [DSA/ElGamal][ElGamal-paper]
- Elliptic Curve Cryptography (ECC) wie z. B. [Curve 25519][curve25519-paper]

Während Elliptic Curve Cryptography noch relativ jung ist - das Curve 25519 Paper
von Daniel J. Bernstein ist von 2005 - haben die beiden Alternativen RSA und DSA
bereits eine längere Geschichte. DSA bzw. das zugrunde liegende ElGamal wurde im
Jahr 1985 von Taher Elgamal veröffentlicht und RSA von R.L. Rivest, A. Shamir
und L. Adleman stammt aus 1977.

Elliptic Curve Cryptography soll in dieser Arbeit nicht weiter betrachtet werden
und kann Inhalt späterer Weiterentwicklungen werden. Insbesondere die, bei ähnlichem
Sicherheitsniveau, deutlich kürzeren Schlüssellängen
[@ecrypt2, Tabelle 7.2] gegenüber RSA und DSA
sind bei den typischerweise geringen Ressourcen Eingebetteter Systeme interessant.
Ebenfalls sollte hierbei der Berechnungsaufwand und mögliche Angriffe wie
[Timing Angriffe][timing-attack-ecdsa] berücksichtigt werden.

Informationen und Implementierungen zu RSA und DSA gibt es auf Grund des Alters
viele. Sowohl RSA als auch DSA werden heute noch mit ausreichender Schlüssellänge
als sicher betrachtet. Bei den RSA Laboratories ist zu lesen:
"DSA is, at present, considered to be secure with 1024-bit keys."[@is-dsa-secure] RSA bietet
gegenüber DSA für den gewollten Anwendungszweck einige Vorteile. Ein RSA Schlüsselpaar
kann sowohl zum Verschlüsseln als auch signieren verwendet werden. Es müssen somit
theoretisch weniger Schlüsselinformationen übertragen werden. DSA kann dagegen
nur zum signieren bzw. überprüfen einer Signatur verwendet werden. Wie die RSA
Laboratories [schreiben][dsa-rsalab] ist die Signaturerzeugung bei DSA schneller
als bei RSA und die Signaturüberprüfung dafür bei RSA schneller als bei DSA, so
lang die öffentlichen und privaten Exponenten entsprechend gewählt wurden. Die
Signaturerzeugung findet in diesem Fall auf einem PC statt, während die Signaturprüfung
auf einem resourcenärmeren Mikrocontroller stattfindet. Es macht also Sinn die
Signaturprüfung so effizient wie möglich zu gestalten. Im Rahmen dieser Arbeit
wird entsprechend RSA zum Einsatz kommen.

### Wahl der Public Key Infrastruktur (PKI){#sec:pki}

Hauptsächlich stehen sich hier die beiden etablierten Systeme OpenPGP (RFC4880)
mit dem dezentralen Web-Of-Trust und X.509 mit zentralen Certificate Authorities
gegenüber.

#### Analyse OpenPGP

Mit der Analyse von OpenPGP wurden mehrere Ziele verfolgt:

-   die von gpg2 erzeugten Zertifizierungen auf enthaltene Informationen untersuchen
-   erzeugte Schlüssel extrahieren
-   den Aufbau des Formats und mittels ASN.1 Modulen spezifizierten Formaten allgemein
    besser verstehen

Um diese Ziele zu erreichen ist das Python Programm OpenPGP_Parser entstanden,
welches das OpenPGP Paketformat implementiert. In diesem Format speicher gpg2 die
OpenPGP Schlüssel und Dateisignaturen außerhalb der programmeigenen Datenbank.
Das Programm
parst eine übergebene Datei und gibt für jedes gefundene Paket Daten wie Typ und
Größe aus. Für spezielle Pakettypen werden auch noch diese geparst und deren Inhalt
menschenlesbar ausgegeben.

Mittels gpg2 wurde ein 1024-bit RSA Schlüsselpaar erzeugt und ohne Nutzung der
ASCII-Armor Funktion exportiert. Verwendet wurde hierfür gpg2 in Version 2.1.21
und die Funktion für [Unattended Key Generation][unattended-key-gen].

```{ #lst:gpg_gen_key .bash caption="GPG Schlüsselerzeugung"}
>$ gpg --batch --gen-key <<__EOF
Key-Type: RSA
Key-Length: 1024
Subkey-Type: RSA
Subkey-Length: 1024
Name-Real: Test Benutzer
Name-Email: test@example.com
Expire-Date: 1
%no-protection
%commit
__EOF

>$ gpg2 --output public_key.gpg --export test@example.com
>$ gpg2 --output secret_key.gpg --export-secret-keys test@example.com
```

[unattended-key-gen]: https://www.gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html

##### OpenPGP Public Key package

```
>$ hexdump -C public_key.gpg
00000000  98 8d 04 58 ac 59 7a 01  04 00 e6 ad 59 f0 bc 53  |...X.Yz.....Y..S|
          | 1 | |2 |timestamp| |3| | 4 | |
00000010  09 14 8d 41 07 35 15 91  18 55 b5 25 fb 46 60 b3  |...A.5...U.%.F`.|
00000020  49 d1 0f 07 ff a5 81 6d  95 3f 76 89 8d b1 27 6f  |I......m.?v...'o|
00000030  1e 9d 95 77 3c 6b fc db  81 44 3d 18 8a 40 e5 04  |...w<k...D=..@..|
00000040  3c fd 0d 9b 72 90 56 d6  32 f1 98 9b b1 45 af ee  |<...r.V.2....E..|
00000050  7b fc c9 22 51 16 f5 cb  c7 be 1f ca 54 02 13 e6  |{.."Q.......T...|
00000060  ed 93 ee 37 a8 c8 e3 92  c9 1c f0 22 b9 1b be fa  |...7......."....|
00000070  78 46 24 47 05 52 57 89  95 24 24 10 53 09 0e bc  |xF$G.RW..$$.S...|
00000080  0e 40 15 e8 68 a6 59 ec  32 c9 00 11 01 00 01 b4  |.@..h.Y.2.......|
        5                              | | 6 | |  7   | |
00000090  20 54 65 73 74 20 42 65  6e 75 74 7a 65 72 20 3c  | Test Benutzer <|
          8|
...
```

1. Header with packet id 6; 1 byte length info -> 141 Byte payload
2. Public-Key Packet Version 4
3. Algorithm
4. length in bits of public modulus n -> 0x0400 = 1024 Bit = 128 Byte
5. public modulus n in bigendian notation
6. length in bit of public exponent e -> 0x11 = 17 Bit = 3 Byte
7. public exponent e in bigendian notation
8. Header with packet id

##### OpenPGP secret key

```
>$ hexdump -C secret_key.gpg
00000000  95 01 d8 04 58 ac 59 7a  01 04 00 e6 ad 59 f0 bc  |....X.Yz.....Y..|
          | 1    | 2| |timestamp| |3| | 4 |
00000010  53 09 14 8d 41 07 35 15  91 18 55 b5 25 fb 46 60  |S...A.5...U.%.F`|
00000020  b3 49 d1 0f 07 ff a5 81  6d 95 3f 76 89 8d b1 27  |.I......m.?v...'|
00000030  6f 1e 9d 95 77 3c 6b fc  db 81 44 3d 18 8a 40 e5  |o...w<k...D=..@.|
00000040  04 3c fd 0d 9b 72 90 56  d6 32 f1 98 9b b1 45 af  |.<...r.V.2....E.|
00000050  ee 7b fc c9 22 51 16 f5  cb c7 be 1f ca 54 02 13  |.{.."Q.......T..|
00000060  e6 ed 93 ee 37 a8 c8 e3  92 c9 1c f0 22 b9 1b be  |....7......."...|
00000070  fa 78 46 24 47 05 52 57  89 95 24 24 10 53 09 0e  |.xF$G.RW..$$.S..|
00000080  bc 0e 40 15 e8 68 a6 59  ec 32 c9 00 11 01 00 01  |..@..h.Y.2......|
            5                               | 6 | |  7   |
00000090  00 03 fc 09 27 e7 ca 3d  9b 59 20 aa f3 a2 0e da  |....'..=.Y .....|
         |8| | 9 | |
000000a0  16 8a 7d fa 64 f1 ca 8b  13 8a b2 44 d3 b7 32 b1  |..}.d......D..2.|
000000b0  46 b1 76 15 a5 23 8d dc  c8 15 11 c1 a9 3c 29 78  |F.v..#.......<)x|
000000c0  1d 99 4d a5 54 28 46 a7  a0 69 b0 4c 9e 37 27 a1  |..M.T(F..i.L.7'.|
000000d0  fe ba 02 58 16 a4 7c a7  f3 eb 2e c5 3f ac 0a c9  |...X..|.....?...|
000000e0  da 0c 7a a7 11 56 c3 aa  5e 6e ad 58 77 3a 1a d8  |..z..V..^n.Xw:..|
000000f0  e5 9f d1 f6 c8 5d 84 7b  10 10 da 08 25 58 92 f7  |.....].{....%X..|
00000100  bb 7c e2 8b 07 4d cc c5  9c 3b 38 f3 88 b8 2a 47  |.|...M...;8...*G|
00000110  f4 a2 d5 02 00 ef c6 c6  32 31 11 03 6e 03 72 06  |........21..n.r.|
            10   |
00000120  fc f4 d0 a4 27 1c 73 c0  9b 5a ce 03 43 12 31 57  |....'.s..Z..C.1W|
00000130  40 b3 d3 8d 17 eb 3b a1  b5 1b 65 b4 bc 16 4d 83  |@.....;...e...M.|
00000140  3a 95 06 5e 45 f1 1d 86  52 c8 01 fa de e0 a8 94  |:..^E...R.......|
00000150  1a ed 0d bb dd 02 00 f6  48 f7 40 08 af d7 b4 a4  |........H.@.....|
00000160  d2 ed 5f e9 b8 d9 5d a2  08 2e 47 bb ea c7 73 2a  |.._...]...G...s*|
00000170  d8 eb 00 6c 85 45 8e 9d  e1 3b 17 5b c2 3d 57 84  |...l.E...;.[.=W.|
00000180  e5 49 79 48 62 87 7d a3  79 be 10 c0 9c d0 5d c3  |.IyHb.}.y.....].|
00000190  4b 20 d7 18 0b 49 dd 02  00 e6 43 d7 da 16 85 94  |K ...I....C.....|
000001a0  85 2b 50 b5 df d0 bf 27  25 21 da a2 d0 63 7b 54  |.+P....'%!...c{T|
000001b0  57 55 63 a9 ec 75 d0 cc  01 7c 3c 33 4e 06 58 38  |WUc..u...|<3N.X8|
000001c0  bb 3c 8b e7 84 27 67 ac  88 90 9b 9a a9 3b a7 51  |.<...'g......;.Q|
000001d0  14 d9 c3 ca b1 b9 30 81  05 9f 20 b4 20 54 65 73  |......0... . Tes|
                                            | User - ID ...
...
```

1. Header with packet id 5; 2 byte length info -> 472 Byte payload
2. secret-Key Packet Version 4
3. Algorithm: RSA
4. length in bits of public modulus n -> 0x0400 = 1024 Bit = 128 Byte
5. public modulus n in bigendian notation
6. length in bit of public exponent e -> 0x11 = 17 Bit = 3 Byte
7. public exponent e in bigendian notation
8. string-to-key usage: 0 -> unencrypted key
9. length of private exponent d: 0x03fc = 1020 Bit = 128 Byte
10. private exponent d in bigendian notation

#### Fazit

Das OpenPGP Protokoll bietet die Möglichtkeit Zertifizierungen mit einer
Gültigkeitsdauer zu versehen. Das gpg2 besitzt besitzt jedoch keine Option
um diese zu setzen. Die Analyse, der von OpenPGP erzeugten Daten, zeigt das auch
keine Gültigkeitsdauer implizit gesetzt wird. Es gibt eine "Signature Creation Time",
welche man für so etwas wie eine Gültigkeitsdauer heranziehen kann. Dabei würde
die Gültigkeitsdauer z. B. 6 Monate auf der Hardware festgelegt werden. Es ist so
also nicht Möglich unterschiedlichen Personen unterschiedlich lange Gültigkeitsdauern
auszustellen. Bei X.509 hingegen ist die Gültigkeitsdauer ein zentraler Bestandteil
und wird grundsätzlich gesetzt.

In OpenPGP wird ein Web-Of-Trust aufgebaut. Es gibt keine Zentrale Instanz, welche
alle Zertifikate signiert sondern jeder Teilnehmer ist gleichberechtigt. Diese
Gleichberechtigung mag es einfacher erscheinen lassen, da keine besonderen Maßnahmen
,zum erzeugen besonderer Zertifikate, ergriffen werden müssen. Für X.509 muss eine
Certificate Authority^[CA] erzeugt und betrieben werden. Das erzeugen und betreiben
einer CA ist jedoch auf den meisten PCs und Notebooks problemlos möglich und sollte
deshalb kein Hindernis darstellen. Bei OpenPGP müsste man also ein spezielles Zertifikat
auswählen, mit dessen privaten Schlüssel anschließend alle berechtigten Zertifikate
signiert werden. In X.509 gibt es diese Logik, des einen Zertifikats, schon.
Es muss nur für jede Berechtigungs-Domäne eine CA geben. Eine Berechtigungs-Domäne
ist dabei eine Gruppe von Geräten, auf die
der gleiche Personenkreis die gleichen Zugriffsrechte haben soll.

X.509 bringt viele der benötigten Funktionen bereits in seiner Internen Logik mit.
Es wird deshalb für dieses Projekt zum Einsatz kommen. Es wäre zukünftig jedoch auch
denkbar ein eigenes Datenformat zu entwickeln, in dem unterschiedliche Berechtigungen
und Gültigkeitsdauern vermerkt sind. Diese Daten werden anschließend mit dem Root-of-Trust Zertifikat
signiert und können so geprüft werden. Auf diese Weise lassen sich unterschiedliche
Berechtigungs-Domänen mit einer CA verwalten. Der Bootlader bzw. die Firmware
müsste dann wissen, welcher Berechtigungs-Domäne sie welche Berechtigungen erteilt.
Die Zuordnung zu den Berechtigungs-Domänen passiert jedoch über die signierten Berechtigungsdaten.

### Wahl der Bibliotheken

Es gibt einige Bibliotheken, welche Kryptographie und BigIntegers implementieren.

AVR-Crypto-Lib:
-   viele Verschlüsselungs- und Hashalgorithmen
-   nur RSA-Verschlüsselung(rsaes_pkcs #1 1.5) und keine Signatur implementiert
-   letztes update 2015 und Hinweis auf der [Website](https://wiki.das-labor.org/w/AVR-Crypto-Lib/en)
    das diese veraltet ist

BigDigits:
- wird aktiv weiterentwickelt (letztes Update ende 2016)
- berücksichtigt Angriffsvektoren wie Timing- oder Leistungsmessangriffe
- nicht für Mikrocontroller optimiert bzw. nicht in Module gesplittet

Wolfssl:
- wird aktiv weiterentwickelt
- zu groß
- Unterstützung für X.509 Zertifikate

Die AVR-Crypto-Lib bietet eine Vielzahl an Algorithmen. Da für die Umsetzung ebenfalls
Hashalgorithmen verwendet werden müssen, würde sich eine Nutzung anbieten. Die Bibliothek
wird jedoch nicht mehr weiterentwickelt. Die BigDigits Bibliothek wird aktiv
weiterentwickelt und berücksichtigt eventuelle Seitenkanalangriffe. Wolfssl bringt
eigentlich alles mit ist allerdings viel zu groß um auf dem kleinen Mikrocontroller
Platz zu finden.

Es werden sowohl die AVR-Crypto-Lib als auch die BigDigits Bibliothek verwendet.
Die AVR-Crypto-Lib liefert die Hashalgorithmen und BigDigits die BigInteger Implementierung.
RSA Signaturen werden ohnehin von keiner der Beiden Bibliotheken unterstützt. Diese
müssen also selbst implementiert werden.

## Implementierung

### RSA

Es gibt eine [RSA Implementierung für Embedded Systems][pic18rsa], welche von
[Alfredo Ortega][ortega] für den Microchip PIC18 bzw. dsPIC30 geschrieben wurde
und auf David Irelands [BigDigits][bigdigits] Bibliothek in Version 2.1 basiert.
Diese Implementierung ändert die BigDigits Bibliothek in der Form das an einigen
Stellen Assembler verwendet wird, der Code für die 8-Architektur des PIC18 optimiert
wurde und auf Speicherallokierung im Heap durch `malloc` verzichtet wird. Das
Speichermanagement wurde in [BigDigits 2.2 vom 31 Juli 2008][bigdigits-version]
derart angepasst das durch Präprozessordirektiven umgeschaltet ebenfalls kein
`malloc` sondern statische Arrays verwendet werden. Da der Assemblercode für PIC18
nicht auf der ATMEGA Plattform läuft wurde die BigDigits Bibliothek direkt
als Grundlage genommen.

#### Probleme bei Portierung von BigDigit

Die BigDigit Bibliothek lässt sich nicht ohne weiteres für den Versuchsprozessor
kompilieren.
Die beiden Funktionen `void perror(const char *)` und `time_t clock(void)` werden
von avr-libc nicht implementiert. Für `clock()` soll die Anwendung eine
Implementierung bereit stellen [@avr-libc-manual, Section 23.12.1]. Da die beiden
Funktionen im Rahmen dieser Arbeit nicht zwingend benötigt werden, sind die entsprechenden
Programmteile auskommentiert. Die Funktionen werden für die
Fehlerausgabe bzw. die Schlüsselerzeugung verwendet.

Die Bibliothek ist zu groß für die 32 kB Flashspeicher. Der Bibliotheksteil ist
nach einfügen der Bibliothek von 1256 auf 23704 Byte Programmcode (.text,.data
und .bootloader-section) angewachsen. Der Unterschied ist in @lst:before_bigdigit
bzw. @lst:after_bigdigit zu sehen.

```{.bash #lst:before_bigdigit caption="vor BigDigit"}
$ avr-size -C --mcu=atmega328 lib.elf
AVR Memory Usage
----------------
Device: atmega328

Program:    1256 bytes (3.8% Full)
(.text + .data + .bootloader)

Data:        134 bytes (6.5% Full)
(.data + .bss + .noinit)
```

```{.bash #lst:after_bigdigit caption="nachdem BigDigit hinzugefügt wurde"}
$ avr-size -C --mcu=atmega328 lib.elf
AVR Memory Usage
----------------
Device: atmega328

Program:   24714 bytes (75.4% Full)
(.text + .data + .bootloader)

Data:       1092 bytes (53.3% Full)
(.data + .bss + .noinit)
```

Insgesamt stehen nur 32 kB Gesamtprogrammspeicher abzüglich 4 kB für Bootladerbereich
zur Verfügung. In 28 kB = 28672 Byte müssen also geteilte Bibliothek und Anwendungsprogramm
Platz finden. Um die Bibliothek zu verkleinern und die notwendige Funktion zu
erhalten bieten sich mehrere Maßnahmen an.

Die erste ist es die nicht benötigten Funktionen möglichst aus der fertigen Executable
heraus zu halten. Der Compiler kompiliert ganze Module und hat keine Grundlage für
Entscheidung, welche Funktionen benötigt werden und welche nicht. Die Funktionen,
die in einem Modul definiert sind, können schließlich in einem anderen aufgerufen
werden. Die Entscheidung welche Funktionen im gesamten Programm benötigt werden
muss also der Linker treffen und entsprechend optimieren. Zumindest der eingesetzte
GNU-ld kann nur auf Objekt-Ebene optimieren.
Das  heißt, wenn eine Funktion aus einer Objekt-Datei aufgerufen wird, dann
wird die gesamte Objekt-Datei inkludiert. Im vorliegenden Fall wird die Jakobifunktion
und der Primzahltest auch in die Executable übernommen, wenn man eigentlich
nur zwei BigDigits addieren und ausgeben will.

```{.bash #lst:bigdigit_first_optimisation caption="Trennung in viele Module"}
$ avr-size -C --mcu=atmega328 lib.elf
AVR Memory Usage
----------------
Device: atmega328

Program:   12042 bytes (36.7% Full)
(.text + .data + .bootloader)

Data:        360 bytes (17.6% Full)
(.data + .bss + .noinit)
```

Wie man in @lst:bigdigit_first_optimisation sieht verringert sich der Speicherbedarf
im Programmspeicher allein durch diese Maßnahme um etwa 12,3 kB.

Als zweite Maßnahme können einige plattformunabhängige Teile durch optimierten
Assemblercode ersetzt werden. Diese Optimierung hat zusätzlich den Effekt das die
Laufzeit verbessert wird. Die häufig aufgerufenen Funktionen `spMultiply`
und `spDivide` sind in ihrer generischen Implementierung vergleichsweise lang.
Die Assembler oder 64-bit Implementierung sind dagegen sehr kurz. Man könnte
durch verringern der Größe eines Digits auf 8-bit die 64-bit Implementierung
auf 16-bit anpassen oder eine plattformspezifische Assembler-Implementierung
schreiben.

Die Bibliothek enthält eine Copyright Notiz. Diese muss laut der aktuellen MIT Lizenz
zwar nicht mehr in der Executable enthalten sein, wird aber dennoch Standartmäßig
mit einkompiliert. Die Copyright Notiz musste laut der Lizenzierung der vorheriger
Versionen in der Executable vorhanden sein. Copyright Notiz und Versionsfunktionen
verbrauchen zusätzlich RAM - allein 164 Byte für das Copyright. Die Copyright Notiz
kann in den Programmspeicher gelegt werden, wenn für
AVR kompiliert wird, und verbraucht somit zumindest keinen Platz mehr im RAM.
Alle Funktionen samt ihren globalen Variablen, welche
für Versionsabfragen benutzt werden, kommen in eigenes Modul und verbrauchen
somit keinen Platz im gelinkten Programm, wenn sie nicht verwendet werden.

### X.509

X.509 bietet durch sein natives Root-of-Trust Modell, die weite Verbreitung und
die auch grundsätzlich verwendete Gültigkeitsdauer einige Vorteile gegenüber PGP
(siehe @sec:pki). Die X.509 Zertifikatsformate sind aktuell in RFC5280 (siehe @rfc5280)
beschrieben.

Wird das Zertifikat übertragen müssen die beiden RSA Parameter Modul und öffenticher
Exponent gespeichert werden. Das Gültigkeitsdatum kann direkt geprüft werden und
es muss nur ein boolescher Wert gespeichert werden. Da die Informationen nicht direkt
signiert werden, sondern nur der Kryptographische Hash über alle Informationen,
können die Daten laufend mitgehasht und die nicht benötigten direkt verworfen werden.
Nicht benötigte Daten sind an dieser Stelle z.B. Informationen über den Inhaber
oder Aussteller. Das Embedded System wird im Regelfall ohnehin keine Möglichkeit
haben zu prüfen ob die EMail-Adresse oder die URL im Zertifikat wirklich zum
Nutzer gehört. Stattdessen soll die aufgebaute Chain-of-Trust ausreichend sein.
Da für eine kurze Zeit zwei Schlüsseldaten im RAM gehalten werden müssen, können
die Schlüssel nicht groß sein. Die beiden Schlüssel sind der Root-of-Trust Schlüssel,
welcher verwendet wird um die Signatur der übertragenen Daten zu prüfen, und der
Schlüssel des übertragenen Zertifikats. Der Schlüssel des übertragenen Zertifikats
rückt nach erfolgreicher Prüfung an die Stelle des Root-of-Trust Schlüssels und
wird für nachfolgende Signaturprüfungen verwendet.

#### DER Encoding

X.509 Zertifikate werden im DER Encoding bzw. dessen Form PEM gespeichert und
übertragen. Das DER Encoding, spezifiziert in X.690, legt ein plattformunabhängiges
Format zum Encodieren von ASN.1 (siehe @sec:asn1) Datenstrukturen fest. Die Kodierregeln stellen dabei
sicher, dass es genau einen Weg gibt die Daten zu kodieren. Dadurch produzieren
alle Systeme und Implementierungen genau den gleichen Bytestrom, was vor allem für
Verschlüsselungen wichtig ist. Es gibt mehrere ASN.1 Compiler z. B. [asn1c][asn1c]
von Lev Walkin als Open Source Projekt oder eine [kommerzielle Lösung][asn1c-objective]
von Objective Systems. Bei Compiler erzeugen einen Parser für Daten, welche nach
dem kompilierten ASN.1 Modul aufgebaut sind. Als unterstützte Kodierregeln stehen
u. a. das benötigte DER als auch das allgemeinere BER zur Verfügung. Die erzeugten
ASN.1 Parser verwenden eine ganze Reihe von Supportbibliotheken um die allgemeinen
Aufgaben zu erfüllen und erzeugen eine Datenstruktur, welche die Daten enthält.
Im vorliegenden Fall sind die erzeugten Parser samt benötigten Bibliotheken zu groß
um in den 32 kB Flash Platz zu finden.

Darüber hinaus ist es nicht nötig die Daten in eine Datenstruktur für spätere Verwendung
zu legen, da nur wenige Daten - wie die Schlüsselteile - dauerhaft benötigt werden.
Elemente wie Gültigkeitsdatum und Signaturen können im Fluge überprüft werden bzw.
in einen Hash zur Überprüfung einfließen. Es wird für diesen Fall also ein spezifischer
Parser, welcher den Anforderungen der begrenzten Ressourcen genügt, entwickelt und
eingesetzt.

[asn1c]: https://github.com/vlm/asn1c
[asn1c-objective]: https://www.obj-sys.com/products/asn1c/index.php

[pic18rsa]: https://sites.google.com/site/ortegaalfredo/pic18rsa
[ortega]: https://sites.google.com/site/ortegaalfredo/home
[bigdigits]: http://www.di-mgt.com.au/bigdigits.html
[bigdigits-version]: http://www.di-mgt.com.au/bigdigits.html#changesinearlier
[rsa-paper]: http://people.csail.mit.edu/rivest/Rsapaper.pdf
[ElGamal-paper]: http://caislab.kaist.ac.kr/lecture/2010/spring/cs548/basic/B02.pdf
[curve25519-paper]: http://cr.yp.to/ecdh/curve25519-20060209.pdf
[timing-attack-ecdsa]: https://www.heise.de/security/meldung/Erfolgreiche-Timing-Angriffe-auf-Verschluesselung-mit-elliptischen-Kurven-1247697.html
[dsa-rsalab]: https://www.emc.com/emc-plus/rsa-labs/standards-initiatives/dsa-and-dss.htm
[signature-expiration]: https://tools.ietf.org/html/rfc4880#section-5.2.3.10
[avr-libc-manual]: http://savannah.nongnu.org/download/avr-libc/avr-libc-user-manual-2.0.0.pdf.bz2
