---
layout: page
title: Dynamische Bibliotheken
---
# Dynamische oder geteilte Bibliotheken

Um überhaupt einen Embedded Bootlader realisieren zu können, muss der verwendete
Mikroprozessor über eine Möglichkeit verfügen, seinen eigenen Programmspeicher zur
Laufzeit zu ändern.
Bei unserem Atmega 328 bzw. der ganzen Familie
desselben gibt es hierfür spezielle OP-Codes, welche nur aus einem besonderen Bootladerbereich
ausführbar sind.
Der Bootladerbereich bei unserem Beispielprozessor ist nur 4 kB groß, was es unmöglich
macht, darin den gesamten Code unterzubringen, welcher für die Kommunikation
mit dem PC, das Flashen der Anwendung und das Prüfen von asymmetrischen
Signaturen notwendig ist. Ein Teil des Programms muss also außerhalb des Bootladerbereichs
ablaufen. Es ist durchaus sinnvoll, die Sicherheitsfunktionen wie asymmetrische
Verschlüsselung, kryptographische Hashfunktionen und ähnliche auch dem Anwendungsprogramm
zur Verfügung zu stellen. Die Toolchain  bietet u. a. in Ermangelung eines Betriebssystems,
, welches für die notwendigen Funktionen zur Verfügung stellt,
keine Unterstützung für dynamische Bibliotheken.
Diese oder eine ähnliche Funktion muss im Rahmen dieser Arbeit implementiert
werden.

Eine Anpassung des Linkers würde den Rahmen der Arbeit übersteigen. Das Bestimmen
von Funktionsadressen zur Laufzeit wäre ohnehin nicht ohne passende, zur Laufzeit
verfügbare Funktionen möglich.

Die Beschreibung in diesem Kapitel konzentriert sich auf die Umsetzung auf anderen
Systemen wie z. B. Unix und Windows und die zur Laufzeit notwendigen Funktionen,
um ein "Linken zur Laufzeit" zu ermöglichen.

## Analyse

### Bestehende Verfahren auf höheren Betriebssystemen

Unter Unix, Windows und anderen Betriebssystemen generiert der Linker eine ausführbare Datei,
welche dann vom Lader in den Arbeitsspeicher geladen wird. Dabei werden Zugriffsadressen
eventuell mit der Verschiebung der tatsächlichen Ladeadresse angepasst. Erzeugt
der Linker ein Programm, das bei Adresse 0 beginnt, der Lader dieses Programm aber
z. B. an Adresse 0x2000 lädt, dann müssen die Adressen auf irgendeine Art und Weise
angepasst werden [@Tanenbaum2006, 568; @Patterson2011, 136-140]. Handelt
es sich um ein System, welches die Zuordnung virtueller
Adressen zu anderen physikalischen Adressen erlaubt, z. B. durch Paging, kann diese
Funktion verwendet werden. Manche Systeme bieten auch ein Relocationsregister. Alternativ können
Adressierungen relativ zum Programmzeiger verwendet werden.

Nicht alle Symbole bzw. Funkionen müssen dabei in der ausführbaren Datei vorhanden
sein. Es ist notwendig, dass diese dann zur Lade- oder Laufzeit dynamisch gebunden werden. Das dynamische
Binden hat dabei mehrere Vorteile:

1.  Die genutzte Bibliothek kann durch neue Versionen getauscht werden, ohne dass das Programm
    neu gelinkt werden muss.
1.  Bibliotheken, welche von mehreren Programmen genutzt werden, müssen nur einmal
    auf dem System vorhanden sein. Die Programme können durch die geteilten Bibliotheken
    kleiner sein und Speicherplatz sparen.

Um mit dynamischen oder geteilten Bibliotheken arbeiten zu können, müssen die Zugriffsadressen
über einen Indirektionsmechanismus in einem Speicherbereich liegen, welcher
zur Laufzeit eines Programms modifiziert werden kann. Zusätzlich sollte es einen Mechanismus
geben, um genau diese indirekten Adressen mit zur Laufzeit gültigen Adressen zu füllen.
Tanenbaum [-@Tanenbaum2006, 571 - 574] listet hierfür mehrere Möglichkeiten:

1.   Es gibt ein spezielles Linkage Segment, in dem ungültige Adressen stehen, welche bei
     Aufruf einen Trap auslösen, in dem dann die tatsächliche Adresse ermittelt
     und ausgeführt wird. Folgende Aufrufe verwenden dann die ermittelte Adresse,
     sodass der Trap nur bei der ersten Verwendung erfolgen muss.
2.   Implizites Binden: Eine Supportbibliothek, welche z. B. durch ein spezielles
     Dienstprogramm erzeugt wurde, lädt zum Start des Programms alle benötigten
     Bibliotheken und befüllt die Datenstrukturen für die Indirektion mit den gültigen
     Adressen. Dabei werden auch Bibliotheken geladen, welche eventuell nicht
     genutzt werden.
3.   Explizites Binden: Das Programm führt zur Laufzeit eine oder mehrere explizite
     Aufrufe aus, um eine benötigte Bibliothek zu laden und die notwendigen Adressen
     zu bestimmen. Wird die Bibliothek nicht mehr verwendet, kann das Programm die
     Bindung wieder aufheben.

### Anforderungen

Updatebar
  : Die Bibliothek sollte sich updaten lassen.

Flexibel einsetzbar
  : Die Bibliothek sollte nicht nur für den Bootlader einsetzbar sein, sondern
    auch für das oder die Anwendungsprogramme verwendbar sein.

Portierbar
  : Der Ansatz sollte sich möglichst gut auf andere Prozessoren der gleichen Serie,
    aber auch andere Architekturen portieren lassen.

## Entwurf

Man kann mehrere Möglichkeiten finden, die oben genannten Anforderungen zu erreichen.
Hier sind drei aufgezählt und direkt mit ihren Vor- und Nachteilen behandelt:

1.  Festes Eincodieren der Zugriffsadressen in den Bootlader:

    Sind die Funktionsadressen fest im Bootlader eincodiert, z. B. über Funktionszeiger,
    kann die Bibliothek nicht upgedatet werden, da hierdurch sehr wahrscheinlich
    die Adressen der einzelnen Funktionen geändert werden. Es wäre also nicht möglich,
    die Bibliothek zu updaten, ohne gleichzeitig den Bootlader zu aktualisieren. Sollen
    die Funktionen der Bibliothek auch für die Anwendung genutzt werden, muss man auch
    diese im Falle eines Bibliotheksupdates zwingend aktualisieren.

2.  Methode bzw. Funktionszeiger an bekannter Stelle:

    Der Bootlader kann hier z. B. den Offset von Funktionen erfragen. Im Falle eines
    Updates der Bibliothek muss nur der Einsprungpunkt an der selben Stelle
    liegen (ähnlich einem Interrupt Vector). Eine bestimmte Funktion oder einen
    Sprung auf dieselbe über Updates konstant zu halten, ist vergleichsweise problemlos
    machbar. Dabei muss man davon ausgehen, dass sich die Präambel z. B. zum Initialisieren
    der globalen Variablen nicht in ihrem Umfang ändert.

3.  Funktionsnamen/Nummern mit Zugriffsadressen im EEPROM speichern:

    Einige Mikrocontroller haben neben Arbeits- und Programmspeicher noch einen
    kleinen EEPROM, um z. B. zur Laufzeit änderbare Einstellungen und Parameter über
    eine Unterbrechung der Stromversorgung hinaus zu erhalten. Ein Teil dieses EEPROMs
    kann z. B. eine Key-Value-Liste mit Funktionsnamen und ihren Adressen enthalten.

    Ein Vorteil bei dieser Vorgehensweise wäre es, dass bei einem Update nur die Werte
    im separaten EEPROM geändert werden und nicht im Programmcode konstant
    gehalten werden müssen. Auf der anderen Seite ist der EEPROM recht klein
    (1 kB beim Versuchscontroller) und wird eventuell anders genutzt, z. B. zum Speichern
    von Einstellungen. Darüber hinaus hat nicht jeder Mikrocontroller einen
    eingebauten EEPROM. Der TI MSP430 bspw. verfügt nicht über integrierten EEPROM
    und müsste folglich einen externen zur Seite gestellt bekommen.

    Sollte das EEPROM durch Programmierfehler oder einen Einbruch der Spannungsversorgung
    korrumpiert werden, ist das Programm nicht mehr nutzbar. Es muss folglich auf eine
    stabile Spannungsversorgung geachtet werden und besondere Vorsicht beim Beschreiben
	des EEPROMs geboten werden.

Vergleicht man die drei Möglichkeiten, so fällt auf, dass Möglichkeit Nr. 2 die geringsten
Nachteile mit sich bringt. Den möglichen Ablauf stellt @fig:aufbau_flash graphisch
dar.

![Aufbau Flash](commons/bootloader_ablauf.png){#fig:aufbau_flash}

1.  Der Prozessor führt nach dem Start direkt Adresse 0x7000 aus.
2.  Wird keine Abbruchbedingung, z. B. bestimmtes Zeichen auf dem UART oder gedrückter
    Taster, erkannt, wird in das Anwendungsprogramm gesprungen.
3.  Bibliothek wird initialisiert (globale Variablen) und Funktionszeiger (Funktion
    `void* getFunctionAddress(char*)`) zum Abfragen einzelner Funktionen zurückgegeben.
4.  Die Adressen der einzelnen benötigten Funktionen werden bei `getFunctionAddress`
    abgefragt.
5.  Funktionen in der Bibliothek können über ihren Funktionszeiger aufgerufen werden.

## Implementierung

Die im Folgenden beschriebene Vorgehensweise gilt für Bootlader wie Anwendungsprogramm
analog, falls in beiden die Funktionen der Bibliothek genutzt werden sollen. Im Quellcode
spielen insbesondere die beiden C-Module `dyn_lib.c` und `lib.c` eine Rolle. Zum
Bootlader gehört dabei die `dyn_lib.c` und die Datei `lib.c` ist der Bibliothek
zuzuordnen.

Im Folgenden werden unterschiedliche Herausforderungen bei der Implementierung
und deren schrittweise Lösung dargestellt.

Die Bibliothek bietet eine Funktion `getFunctionAddress` (@lst:getFunctionAddress1),
welche mit einem übergebenen
String nach einer passenden Funktion sucht und die Adresse dieser Funktion zurückgibt.

```{.c #lst:getFunctionAddress1 caption="lib.c: getFunctionAddress"}
void* getFunctionAddress(char* name){
    if(strcmp(name, "mod000_rgb") == 0){
        return mod000_rgb;
    }else if(strcmp(name, "init_mod000") == 0){
        return init_mod000;
    }
    return NULL;
}
```

### Probleme

Ruft man die oben genannte Funktion `getFunctionAddress` direkt aus dem Bootlader
auf (@lst:load_addresses_direct), schlägt der Aufruf fehl. Die zurückgegebenen
Funktionszeiger waren immer `NULL`.

```{.c #lst:load_addresses_direct caption="Direktes Laden von Adressen"}

void (*mod000_rgb) (uint8_t);
void (*init_mod000)( void );

void load_addresses(void){
    void* (*lib_addr)(char *function);
    lib_addr = 0x0110;
    init_mod000 = lib_addr("init_mod000");
    mod000_rgb = lib_addr("mod000_rgb");
}
```

Offenbar schlug hier der Stringvergleich mit `strcmp` (@lst:getFunctionAddress1)
fehl. Zu diesem Effekt gibt es drei initiale Vermutungen.

1. Fehler bei der Werteübergabe
2. Fehler in der strcmp-Implementierung der AVR-libc
3. Fehler bei den verglichenen Werten

Die erste Vermutung konnte widerlegt werden, indem der übergebene String innerhalb
der Bibliothek über eine UART-Verbindung ausgegeben wurde.

Die zweite Vermutung
wurde in mehren Schritten evaluiert. Als erstes wurde statt eines Stringvergleiches
ein Charaktervergleich auf die Anfangsbuchstaben der Übergabeparameter durchgeführt.
Dieser Charaktervergleich war erfolgreich, allerdings schränkt der Vergleich der
Anfangsbuchstaben den Komfort der Bibliothek doch sehr stark ein. Im Weiteren
folgte eine eigene Implementierung eines Stringvergleiches (@lst:strEquals).

```{.c #lst:strEquals caption="Selbst implementierter Stringvergleich"}
bool strEquals(char *str1, char *str2){
    printString(str1);

    printString(str2);
    while(*str1 == *str2){
        if(*str1 == '\0')
            return true;
        str1++;
        str2++;
    }
    return false;
}
```

Dieser selbst implementierte
Stringvergleich zeigte das selbe Verhalten und gab trotz gleicher Eingabestrings
ungleich zurück. Um das Phänomen genauer zu analysieren, wurde eine Ausgabefunktion
(die Funktion `printString(char*)` in @lst:strEquals)
geschrieben, welche neben der eigentlichen Stringinterpretation der Daten auch
die ersten 20 Byte der selben Daten in einer hexadezimalen Byteinterpretation
ausgibt. Beim Vergleich der Byteinterpretation von Übergabeparameter an die Bibliothek
und Stringliterale in der Bibliothek ist aufgefallen, dass die Stringliterale in
der Bibliothek tatsächlich andere Daten enthielten als erwartet wurde. Bei einer
genauen Betrachtung der Werte ist eine bestimmte Bytefolge aus dem Bootlader
aufgefallen. Es waren die zwei Byte der Zugriffsadresse zur Bibliothek, welche freilich
nirgends in der Bibliothek vorkamen.
Es kann also auch die zweite Vermutung ausgeschlossen werden, da eine Erklärung für
das Verhalten gefunden wurde.

Stattdessen konnten allerdings zwei Erkenntnisse hinzugewonnen werden:

1.  Die Bibliothek verwendet einen Teil des RAMs, der ebenfalls vom Bootlader
    verwendet wird.
2.  Die Stringliterale werden scheinbar im RAM gesucht.

Diesen beiden Erkenntnissen wird im Folgenden nachgegangen.

#### Falsch platzierte Static Data Area

Obwohl der Linkeraufruf für den Bootlader die Option `-Tdata=0x800180` enthielt,
verwendeten Bootlader und Bibliothek offenbar die gleichen Speicherbereiche.
Die ld Implementierung des AVR-gcc zeigt ein anderes Verhalten als spezifiziert.
In der Manpage zu ld ist dokumentiert, dass `-Tdata=org` äquivalent zu `--section-start=.data=org`
ist. Für `.text` und `.bss` stimmt dies auch, aber scheinbar nicht für `.data`. In der
Online Referenz der AVR Libc [@avr-libc-manual, Seite 18] findet sich als Nebennotiz
der Hinweis, dass `-Tdata` bereits vom GCC Frontend benutzt wird. Die Verwendung von
`--section-start=.data=org` bringt in diesem Fall die Lösung und der Linker verschiebt
den verwendeten Bereich im RAM auch tatsächlich.

#### SDA initialisieren

Der Compiler scheint lokale Stringliterale ("bsp. String") nicht in den Stack
einer Funktion zu legen, sondern in einen bestimmten Bereich des RAMs, die Static
Data Area.

Der RAM wird in der vom Linker hinzugefügten Programmpräambel initialisiert.
Mit dem Programm `objdump` kann man die fertige Binärdatei disassemblieren und so
die notwendigen Stellen analysieren. Da es sich um Binärcode für den AVR 328 handelt,
verwendet man die Version aus der avr-gcc Toolchain. `avr-objdump -S --disassemble lib.elf`
gibt ein Disassembly aus. Im Auszug ist dies in @lst:preample zu sehen.
Um den RAM zu initialisieren, müssen Teile der Programmpräambel als Inlineassembler
in eine Init-Funktion kopiert werden. Aus dem Disassembly kann man die folgenden
Teile extrahieren: `__do_copy_data` und `__do_clear_bss`. Zu `__do_clear_bss` gehören
noch `.do_clear_bss_loop` und `.do_clear_bss_start`, wobei die letzten beiden Zeilen
`call` und `jmp` das Ausführen der main und das Beenden des Programms nach sich ziehen
würden und entsprechend nicht mit kopiert werden dürfen.

```{.asm #lst:preample caption="Ausschnitt Programmpreamble lib.elf"}
00000074 <__do_copy_data>:
  74:   11 e0           ldi     r17, 0x01       ; 1
  76:   a0 e0           ldi     r26, 0x00       ; 0
  78:   b1 e0           ldi     r27, 0x01       ; 1
  7a:   ee e9           ldi     r30, 0x9E       ; 158
  7c:   f4 e0           ldi     r31, 0x04       ; 4
  7e:   02 c0           rjmp    .+4             ; 0x84 <__do_copy_data+0x10>
  80:   05 90           lpm     r0, Z+
  82:   0d 92           st      X+, r0
  84:   a0 34           cpi     r26, 0x40       ; 64
  86:   b1 07           cpc     r27, r17
  88:   d9 f7           brne    .-10            ; 0x80 <__do_copy_data+0xc>

0000008a <__do_clear_bss>:
  8a:   21 e0           ldi     r18, 0x01       ; 1
  8c:   a0 e4           ldi     r26, 0x40       ; 64
  8e:   b1 e0           ldi     r27, 0x01       ; 1
  90:   01 c0           rjmp    .+2             ; 0x94 <.do_clear_bss_start>

00000092 <.do_clear_bss_loop>:
  92:   1d 92           st      X+, r1

00000094 <.do_clear_bss_start>:
  94:   a6 38           cpi     r26, 0x86       ; 134
  96:   b2 07           cpc     r27, r18
  98:   e1 f7           brne    .-8             ; 0x92 <.do_clear_bss_loop>
  9a:   0e 94 4a 02     call    0x494   ; 0x494 <main>
  9e:   0c 94 4d 02     jmp     0x49a   ; 0x49a <_exit>
```

### Optimierungen

Man kann die bisherige Lösung in einigen Aspekten optimieren, um beispielsweise den durch
die Bibliothek entstehenden Speicher-Overhead zu reduzieren oder die Fehleranfälligkeit
durch vergessene Anpassungen, z. B. der `_init`-Funktion, zu minimieren.

#### Speicher sparen

Liegen die Strings mit den Funktionsnamen im RAM, blockieren sie den wertvollen
RAM mit Daten, welche nur einmal zum Abfragen der Funktionsadressen benötigt werden.
Stellt man viele Funktionen über die Bibliothek bereit, welche dann auch noch
einen großen Stack erzeugen wie z. B. die RSA Funktionen aus @sec:kryptographie,
kommt es sehr schnell zum Stackoverflow und Teile der Static Data Area werden
überschrieben.

Strings belegen viel Platz im RAM und müssen weder verändert noch häufig gelesen
werden. Es ergibt folglich Sinn, sie im Programmspeicher zu speichern, wo sie
ohnehin gespeichert werden müssen, um von der Programmpräambel (`__do_copy_data`)
in den RAM kopiert zu werden. Man kann hierfür eine Funktion der avr-libc [@avr-libc-manual, Kapitel 5]
nutzen, welche es erlaubt, Daten im Programmspeicher abzulegen und erst, wenn sie
benötigt werden, zur Laufzeit in einen beliebig allozierten Puffer zu laden. Zu
bedenken ist dabei, dass dieses Vorgehen Komplexität hinzufügt, welche sich wiederum
in mehr Instruktionen und folglich mehr benötigtem Programmspeicher niederschlägt.
Das ist insbesondere im Bootladerbereich, wo nur 4 kB zur Verfügung stehen,
relevant.

Wenn man nun wie in @lst:load_addresses_direct die Funktionen in einer Sequenz
abfragt, werden viele Instruktionen erzeugt. Vermieden werden kann dies, indem
die ähnlichen, sich wiederholenden Zeilen in eine Schleife gepackt werden. Um
das zu erreichen, benötigt man eine Möglichkeit, die Zuordnung von String zur Variable
auf eine Art und Weise zu realisieren, die für den Zugriff in Schleifen tauglich
ist, z. B. über einen Index.

Ein zweidimensionales Array funktioniert nicht, da es sich um zwei unterschiedliche
Datentypen handelt: einen char-Zeiger im Programmspeicher und einen Funktionszeiger
im RAM. Die Zuordnung kann in diesem Fall über zwei Arrays unterschiedlichen
Typs geschehen, bei dem die gleichen Indizes die Verbindung zwischen beiden Elementen
darstellt. Diese beiden Arrays sind im Quellcode als `lib_string_tbl` für die
Funktionsnamen und `lib_addr_tbl` für die Funktionszeiger zu finden.

Wenn man die Funktionszeiger auf diese Weise speichert, muss man die Funktionen
auf andere Art und Weise aufrufen. Ein transparentes Aufrufen des Funktionszeigers
wie in @lst:load_addresses_direct zu sehen, funktioniert dann nicht mehr komfortabel.
Möglichkeiten, das Aussehen des Funktionsaufrufs und somit die Nutzerfreundlichkeit zu
erhalten, sind funktionsartige Präprozessormakros und Wrapperfunktionen, welche
die selbe Signatur haben. Die Wrapperfunktionen haben den Nachteil, das sie zu
jedem Funktionsaufruf den Aufruf der Wrapperfunktion hinzufügen. Auf dem Versuchscontroller
benötigten die call- und return-Instruktionen jeweils vier Taktzyklen [@avr_instruction_set, Seite 24],
was besonders bei häufigem Zugriff auf Funktionen mit kurzer Laufzeit vermieden
werden sollte. Man kann den Overhead des zusätzlichen Funktionsaufrufs durch
funktionsartige Makros verhindern. Man legt hierzu für jede Funktion in der Bibliothek
ein Makro wie in @lst:functionlike_macros an.

```{#lst:functionlike_macros .c caption="dyn_lib.h: Funktionsartige Makros"}
#define mod000_rgb(a) ((void (*)(uint8_t))(lib_addr_tbl[1]))(a)
#define init_mod000() ((void (*)(void))(lib_addr_tbl[0]))()
```

Abschließend kann man eine Einsparung von 166 Byte RAM und 152 Byte Programmspeicher
durch diese Optimierung feststellen. Die Größenänderungen wurden bei 13 abgefragten
Funktionen mittels des Programms "`size` ermittelt und sind in @lst:before_optimisation
und @lst:after_optimisation zu sehen. Diese Optimierung findet sich so auch in der
Bibliothek wieder, da hier ebenfalls die Strings für die Ermittlung der benötigten Funktion
in den Programmspeicher ausgelagert werden können.

```{.bash #lst:before_optimisation caption="Vor Optimierungen"}
$ avr-size -C --mcu=atmega328 boot.elf
AVR Memory Usage
----------------
Device: atmega328

Program:    3718 bytes (11.3% Full)
(.text + .data + .bootloader)

Data:        584 bytes (28.5% Full)
(.data + .bss + .noinit)
```

```{.bash #lst:after_optimisation caption="Nach Optimierungen"}
$ avr-size -C --mcu=atmega328 boot.elf
AVR Memory Usage
----------------
Device: atmega328

Program:    3566 bytes (10.9% Full)
(.text + .data + .bootloader)

Data:        418 bytes (20.4% Full)
(.data + .bss + .noinit)
```

#### Eliminieren des Assemblercodes

Bei fast jeder Änderung der Bibliothek ändert sich die Präambel. Den in @lst:preample
zu sehenden Assemblercode bei jeder Änderung der Bibliotheksfunktionen anzupassen, ist
umständlich und vor allem fehleranfällig. Es wäre schön, wenn die Variablen automatisch
initialisiert werden würden. Die "Funktion" `__do_copy_data`, von welcher ich den
Assemblercode kopiert habe, direkt aufzurufen, funktioniert nicht, da anschließend
die `main`-Funktion aufgerufen wird, welche wiederum in `_exit()` mündet.

Es besteht die Möglichkeit, `longjmp` zu verwenden. Dabei wird wieder `_init()` aufgerufen.
`_init()` führt nun aber keinen kopierten Assemblercode mehr aus, sondern speichert
mittels `setjmp` den Kontext. Dieser Kontext kann nun in der `main`-Funktion wiederhergestellt
werden. Die `main` kann auch versehentlich, z. B. durch einen Funktionszeiger,
auf `NULL` aufgerufen werden. In diesem Fall liegt erstens ein Fehler vor
und zweitens ist der Kontext ungültig. Um diesen Fehler zu erkennen, müssen aufrufende Funktion und `main` eine
Magicnumber austauschen, welche als Anhaltspunkt für einen absichtlichen Aufruf verwendet
wird. Entspricht der übergebene Wert der Magicnumber, springt das Programm mittels longjmp
wieder in die `_init`-Funktion. Andernfalls führt die `main` eine Endlosschleife
aus und zeigt einen Fehler an.

Um Magicnumber und Jump-Buffer für die `main` lesbar zu übergeben, gibt es mehrere Möglichkeiten:

1. Auf den Stack legen
2. Variable in Register
3. .noinit Bereich

Die Magicnumber und der Jump-Buffer können nicht einfach als globale Variablen angelegt
werden, da sie genau wie alle anderen globalen Variablen während der `__do_clear_bss`-
Phase mit null überschrieben werden würden.

Man kann die Werte auf den Stack legen, indem dem Funktionsprototypen `__do_copy_data`
Übergabeparameter gegeben werden, die aber nicht konsumiert werden. Der `main`-Funktion werden
die gleichen Übergabeparameter gegeben, wie es in @lst:falseargs mit dem `uint8_t` für die Magicnumber
zu sehen ist. Diese Vorgehensweise hat das Problem, dass die `main`-Funktion den Wert
natürlich auch dann vom Stack nimmt, wenn er eigentlich nicht darauf gelegt wurde. So wird
der Stack korrumpiert und läuft ggf. in einen Stack-underrun. In diesem Fall handelt es sich um
einen Fehler, weil die `main`-Funktion der
Bibliothek nicht über `_init` aufgerufen wurde. Dieser ist weitgehend unproblematisch, wenn
man in einen Fehlerzustand übergeht.

Man kann Variablen dauerhaft einem bestimmten Register zuweisen [@avr-libc-manual, Seite 47],
wodurch diese auch aus der üblichen Initialisierung herausfallen. In @lst:falseargs
sieht man die Vorgehensweise an dem Zeiger `*env`. Man kann in dem Register freilich
keine ganzen Strukturen speichern, sondern nur einzelne Werte, weshalb der Speicherbereich
in diesem Fall durch den Aufrufer von `_init` z. B. in dessen Stack bereitgestellt werden
muss. Da wir im gegebenen Fall nur den Teil der Programmpräambel zum Initialisieren des
RAMs und nicht den zum Initialisieren des Prozessors samt Registern ausführen, bleibt
der Wert über die Initialisierung hinaus erhalten. Diese Möglichkeit beinhaltet
jedoch das Problem, dass dieses Register im Bootlader oder Anwendungsprogramm bereits
eine solche Sonderbedeutung haben könnte. Hier würde man den Wert überschreiben.
Um dem vorzubeugen, könnte man Konventionen einführen wie "Dieses Register darf nicht
genutzt werden" oder "Die Bibliothek muss ganz am Anfang initialisiert werden, bevor
etwas sinnvolles in dem Register steht."

```{.c #lst:falseargs caption="Falsche Parameter und Variablen in Registern"}
#define MAGICNUMBER 165

register jmp_buf *env asm("r2");

/* sensless but nesasary to have the linker working
 */
int main(uint8_t i){
    if( i == MAGICNUMBER)
        longjmp( env, 1);
    init_mod000();
    while(true)
    {
        mod000_rgb(rot);
        _delay_ms(500);
        mod000_rgb(schwarz);
        _delay_ms(500);
    }
    return 0;
}

extern void __do_copy_data( uint8_t);

void* _init(jmp_buf* jmp) {
    env = jmp;
    if(setjmp(*jmp)){
         /*handle return*/
         return getFunctionAddress;
    }
    __do_copy_data(MAGICNUMBER);
    return getFunctionAddress;
}
```

Die dritte Möglichkeit ist die Nutzung des noinit-Bereichs [@avr-libc-manual, Seite 18-19].
Dieser Bereich ist Teil des bss-Bereichs und enthält ebenfalls - im Quellcode -
uninitialsisierte Variablen, welche jedoch nicht mit null initialisiert werden.
Variablen, welche an dieser Stelle liegen, sind folglich nicht von den Initialisierungsroutinen
in der Programmpräambel betroffen. Der Compiler kann mit einem zusätzlichen
Attribut, wie in @lst:_init bei den Variablen `*env` und `magicnumber` zu sehen,
angewiesen werden, die Variablen in den noinit-Bereich zu legen.

### Endergebnis

Beide Teile, Bootlader wie auch Bibliothek, werden unabhängig voneinander mit jeweils
anderen Offsets gelinkt.

```{.bash #lst:linker_commands caption="Beide Linker Aufrufe für Bootlader und Bibliothek"}

LD = avr-gcc -mmcu=atmega328

$(LD) -Wl,-Map=lib.map -o lib.elf lib.o uart.o mod000.o ...

$(LD) -Wl,-Ttext=0x7000,--section-start=.data=0x800180 -o boot.elf ...
```

Um die Bibliothek vor allem in Hinblick auf globale Variablen zu initialisieren
und einen Funktionszeiger für die weitere Arbeit mit der Bibliothek zu bekommen,
ruft der Bootlader eine feste bekannte Adresse, die Funktion `_init()`, auf.

```{#lst:load_addresses1 .c caption="dyn_lib.c: load_addresses() 1"}
void load_addresses(void){
    jmp_buf jmp;
    void* (*_init)(jmp_buf *);
    void* (*lib_addr)(const char *function);
    _init = 0x026b;
    /* _init() laut lib.map an Stelle 0x04b0 -> 4d6/2=26b;
     * Funktionsaufrufe werden an Word-Adressen gemacht
     */
    lib_addr =_init( &jmp );
...
}
```

`_init` benötigt einen `jmp_buf`, welcher nicht in ihrem eigenen Stack liegen kann.

Der `jmp_buf` wird durch die Funktion `setjmp(jmp_buf)` so initialisiert, dass ein
späterer Aufruf von `longjmp(jmp_buf)` genau den Kontext wiederherstellt, welcher
zum Zeitpunkt des Aufrufs von `setjmp` war. Der Kontext besteht vor allem aus dem
Inhalt der Register wie z.B. Stack- und Programmzeiger. Dieser Schritt ermöglicht
es, später zu dieser Stelle zurückzukehren und dabei den eigentlichen Kontrollfluss
zu umgehen.

Wie man in @lst:_init sieht, wird eine `MAGICNUMBER` verwendet. Dieser Kunstgriff
ermöglicht es, mit einiger Wahrscheinlichkeit einen unbeabsichtigten Aufruf z.B.
durch einen fehlerhaften Funktionszeiger zu detektieren. Die aufgerufene Funktion
`__do_copy_data` ist ein Label aus der Programmpräambel, welche der Linker zum
Initialisieren des Prozessors hinzufügt.

```{.c #lst:_init caption="lib.c: _init()"}

jmp_buf *env __attribute__ ((section (".noinit")));
uint32_t magicnumber __attribute__ ((section (".noinit")));

extern void __do_copy_data( void);

void* _init(jmp_buf* jmp) {
    env = jmp;
    if(setjmp(*jmp)){
         /*handle return*/
         magicnumber = 0;
         return getFunctionAddress;
    }
    magicnumber = MAGICNUMBER;
    __do_copy_data();
    return getFunctionAddress;
}
```

Um Funktionszeiger zu bekommen, fragt eine Funktion aus dem Bootlader
über eine feste bekannte Adresse einen Funktionsnamen an. Dieser übergebene Funktionsname
wird nun innerhalb der Bibliothek per Stringvergleich mit allen der Bibliothek
bekannten Funktionen verglichen. Der Vergleich sollte wie in @lst:getFunctionAddress1 zu sehen erfolgen.
Die Werte mod000_rgb und init_mod000 sind hierbei Funktionen aus einem
eingebundenen Modul und werden als Funktionszeiger zurückgegeben.

```{.c #lst:load_addresses2 caption="dyn_lib.c: load_addresses() 2"}
...
  lib_addr =_init( &jmp );
  for( int i = 0; i < (sizeof( lib_string_tbl ) / sizeof( char * )); i++ )
  {
      char buf[20];
      strcpy_P( buf, lib_string_tbl[ i ] );
      /*
       * strcpy_P(buf, (PGM_P)pgm_read_word(&(lib_string_tbl[i])));
       */
      lib_addr_tbl[ i ] = lib_addr( buf );
  }
}
```
