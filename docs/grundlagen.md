---
  title: Grundlagen und Definitionen
  layout: page
---
# Grundlagen und Definitionen

In diesem Kapitel werden Grundlagen erläutert oder definiert, auf welche in
der Arbeit aufgebaut wird.

## Embedded System

Eingebettete Systeme, im englischen Embedded Systems, in ein oder zwei Sätzen
klar zu definieren ist keine leichte, wenn nicht gar eine unmögliche Aufgabe.
Es ist vielmehr eine ganze Gruppe von Systemen, die eine gewisse Menge an Eigenschaften
teilen, jedoch nicht alle besitzen müssen, um als Embedded System klassifiziert zu werden.
Auch ehemals klare Zuordnungen können sich mit der Zeit ändern, wie wir später am
Beispiel des Mobiltelefons sehen werden.

Wolf [-@Wolf2012, 2] schreibt zur Einführung in
Embedded Systems: "Loosely defined, it is any device that includes a programmable
computer but is not itself inteded to be a general-purpose computer." Die Definition
trifft den Kern des Wortlautes und deckt sich sinngemäß mit @Marwedel2011, der es im
Weiteren vor allem auf Cyber-Physical-Systems eingrenzt, und schreibt: "Embedded
systems are information processing systems embedded into enclosing products."

Eingebettete Systeme sind also prinzipiell beliebige Computersysteme, die
- eingebunden in einen bestimmten Kontext oder Produkt - bestimmte klar umrissene
Aufgaben wie z. B. die Steuerung oder das Erfassen von Messwerten vornehmen. Diese Systeme
unterscheiden sich also vom klassischen Desktop-PC, Laptop oder Server dadurch, dass
sie nicht dafür gedacht sind, nahezu beliebige Aufgaben durch verschiedenste Software
zu erfüllen, sondern immer nur die vorher spezifizierten. Nicht festgelegt ist dabei,
welche Art von Computersystem oder Architektur dabei verwendet wird. Wenn die Anwendung
es erfordert, kann auch ein entsprechend mit Soft- und Hardware ausgestatteter
Desktop-PC mit x86-Hardware als eingebettetes System dienen.
Embedded Systems interagieren häufig z. B. durch Sensoren und Aktoren mit der physischen
Welt. In diesem Fall nennt man sie Cyber-Physical Systems.

Die meisten Embedded und Cyber-Physical Systems haben bestimmte häufig vorkommende
gemeinsame Eigenschaften und Erfordernisse. @Marwedel2011 listet hierfür die folgenden auf:

- verlässlich
    - Zuverlässlichkeit
    - Wartbarkeit
    - Verfügbarkeit
    - Safety
    - Security
- Effizienz
    - Energieeffizienz
    - Laufzeiteffizienz
    - Programmgröße
    - Gewicht
    - Kosten
- Echtzeitbedingungen
- reaktiv (Ausführungsgeschwindigkeit wird von der Umwelt bestimmt)
- sind hybrid (verarbeiten analoge und digitale Daten)
- haben eine spezielle Nutzerschnittstelle
- sind spezialisiert auf eine bestimmte Anwendung
- sind in Lehre und öffentlicher Diskussion unterrepräsentiert

Den letzten Punkt führt Marwedel zwar auf, aber er eignet sich nicht um ein System
als Embedded System zu klassifizieren. Ob etwas in unterrepäsentiert ist oder nicht,
kann sich innerhalb kurzer Zeit ändern und ist somit für eine Zuordnung untauglich.

Echtzeitbedingungen
  : Unter Echtzeitbedingungen versteht man zeitliche Mindestanforderungen,
    eine Berechnung zu beenden bzw. auf ein Ereignis zu reagieren. Man unterscheidet zwischen
    weichen und harten Echtzeitbedingungen.

	Harte Echtzeitbedingungen sind
    laut einer Definition von Kopetz [@Marwedel2011, Seite 9] solche Bedingungen,
    bei denen die Nichteinhaltung der zeitlichen Vorgaben zu einer Katastrophe
    führen können. Solche Katastrophen sind dann z. B. Un- oder gar Todesfälle,
    weil z. B. das ESP oder der Airbag im Auto nicht zeitgerecht auf Ereignisse
    reagiert haben.

	Weiche Echtzeit ist laut @Marwedel2011 alles andere.
    [Wikipedia][wiki-realtime] führt noch Unterklassifizierungen von weicher
    Echtzeit auf. Die Spanne reicht von "Ergebnis hat keinen Wert mehr" bis
    "Ergebnis wird angenommen, wenn es vorliegt". Beispiele hierfür sind z. B.
    das Dekodieren einer Audio- oder Videodatei. Kann der Computer die Datei
    nicht schnell genug verarbeiten, ruckelt das Video und es beeinträchtigt
    das Nutzererlebnis, aber es geht keine Information verloren. Bei einem
    Telefongespräch allerdings gehen eventuell Informationen verloren, wenn
    z. B. bei VoIP einige Pakete länger brauchen, da der Gesprächspartner
    inzwischen schon mehr erzählt hat. Die verzögerte Information und die
	darauf folgende unverzögerte Information kommen zeitgleich an und führen zu
	Informationsverlust, da nur eine Information zur gleichen Zeit verarbeitet
	werden kann.

Marwedel konstatiert weiter, dass man den Begriff "Embedded System" wie folgt definieren
könne: "Information processing systems meeting most of the characteristics listed
above are called embedded systems."

Ob eine Geräteklasse oder ein spezielles Gerät von der Definition erfasst wird oder nicht, ändert sich
mit der Zeit. Nehmen wir als Beispiel das Mobiltelefon. Ein Mobiltelefon wie es
vor 10 - 15 Jahren noch üblich war, hatte einige klar umrissene Funktionen wie
Telefonieren, SMS schreiben beziehungsweise Kalender und Adressbuch nutzen. Die enthaltenen Mikroprozessoren dienten
genau diesen Zwecken und auch die Software war genau darauf abgestimmt. Wohl
die wenigsten Menschen
waren sich bei diesen Telefonen darüber im Klaren, dass sie kleine Computer in den
Händen hielten. Der Computer war einfach von dem Produkt Handy umgeben.
Auch, wenn wir die Definition nach den gemeinsamen Charakteristika bedienen, erfüllen
diese alten Mobiltelefone die meisten und können somit als Embedded Device betrachtet
werden.

Moderne Smartphones jedoch haben die Telefoniefunktion nur noch als eine von vielen
Apps installiert und die Telefonie-App ist auf den meisten Smartphones auch die
einzige mit Echtzeitanforderungen. Diese werden durch die Sprache und deren Verarbeitung durch die
Menschen vorgegeben. Die Echtzeitanforderungen
der meisten anderen Apps dürften nicht über die allgemeine "responsiveness"
hinausgehen, die auch auch von PC-Software erwartet wird. Die Telefontastaturen sind universell
einsetzbaren Touchscreens gewichen und auch der Funktionsumfang ist durch beliebige
Apps riesig. Das Fortschreiten der Speichertechnik verschiebt auch Effizienzbetrachtungen.
Die Software auf Smartphones muss dank mehreren GB RAM und Massenspeicher kaum sparsamer
mit derartigen Ressourcen umgehen als auf PCs. Mobiltelefone haben sich vom spezialisierten
Embedded System weg hin zu Multi-Purpose Systemen entwickelt. Eine ähnliche Entwicklung
durchlaufen noch Fernseher auf ihrem Weg zum allumfassenden SmartTV.

[wiki-realtime]: https://de.wikipedia.org/wiki/Echtzeitsystem#Definition

## Mikrocontroller

Auch wenn die Definition zu Embedded System keine Einschränkung in der Art der verwendeten Computer
macht, dürfte man am häufigsten spezielle Mikroprozessoren mit integriertem
Arbeits-, Festwertspeicher und integrierter spezialisierter Peripherie finden. Solche
vollintegrierten Mikroprozessoren nennt man dann Mikrocontroller, kurz MCU für
Micro Controller Unit. Die integrierte Peripherie ist in vielen Fällen, z. B.
beim Analog-Digital-Wandler oder der Kommunikationsperipherie, dafür geeignet, mit der
Welt außerhalb zu interagieren. Es gibt jedoch auch Peripherie, welche keine
unmittelbare Auswirkung auf die Umwelt oder andere Controller hat, z. B. eine
CRC- oder AES-Einheit, welche nur Berechnungen auf dem Controller selbst beschleunigt.
Kommunikationsperipherie kann
Kommunikationsschnittstellen wie UART, I²C/TWI, SPI, aber auch zunehmend Bluetooth
und IEEE 802.15.4, was z. B. ZigBee zugrunde liegt, zur Verfügung stellen.

Verglichen mit Desktopsystemen haben Mikrocontroller häufig wenig Rechenleistung und Speicher.
Für die sehr beschränkten Aufgaben eines Embedded Systems ist es in vielen Fällen
ausreichend, mit wenigen MHz Taktfrequenz und wenigen kB RAM zu rechnen, um die
Ergebnisse anschließend zu übermitteln. In vielen Einsatzszenarien ist auch der
Stromverbrauch von entscheidender Bedeutung - beispielsweise wenn das Gerät z. B. per Akku oder
Solarzelle versorgt werden soll. Hierfür gibt es besonders stromsparende, aber auch
leistungsarme Mikrocontroller, deren Stromaufnahme teilweise im Bereich einiger Milliampere
- bei 3,3 Volt Versorgungsspannung -
liegt. Der Atmel Atmega AVR 328 verbraucht laut Datenblatt [@328_datasheet, Seite 2] bei
1 MHz Takt und 1,8 V nur 0,2 mA während der Prozessor aktiv ist, was einer Leistungsaufnahme
von 0,38 mW entspricht.
Mikrocontroller mit einer derart geringen Leistung und Leistungsaufnahme stellen
die Hardwareplattform dieser Arbeit dar. Konkret wird ein Atmel Atmega AVR 328
verwendet (siehe @sec:zielsystem).

## Embedded Bootlader

Mikrocontroller haben typischerweise nur einen kleinen Speicher und Programme
laufen ohne Betriebssystem direkt auf dem Prozessor. Zur Übersetzungszeit werden
die Programme von der Compilertoolchain statisch mit den verwendeten Bibliotheken
gelinkt und mit diesen gemeinsam als Maschinencode in den Programmspeicher des
Controllers kopiert. Ohne einen Bootlader werden häufig in Hardware implementierte
SPI-Protokolle, z. B. ISProgramme
laufen ohne Betriebssystem direkt auf dem Prozessor. Zur Übersetzungsze (In System Programmer), über spezielle Programmieradapter
verwendet, um das Programm in den Speicher zu kopieren. Im Falle des vorliegenden
atmega328 ist dies z. B. der Atmel AVR ISP MkII (@fig:avr_isp_mk2).
Bootlader kompatible Mikrocontroller wie z. B. Atmel ATMega 328 bieten die
Möglichkeit, ihren Programmspeicher zur Laufzeit mittels spezieller OP-Codes zu verändern.
Somit können sie z. B. ein neues Anwendungsprogramm über eine Peripherieschnittstelle wie UART
empfangen, in den RAM kopieren und dann eine bestimmte Speicherseite im
Programmspeicher ersetzen.
Der Bootlader hat nun auf dem Mikrocontroller nicht die Aufgabe, das Betriebssystem
zu laden, sondern beim Start auf ein bestimmtes Signal zu lauschen. Bleibt das Signal
aus, wird das Anwendungsprogramm gestartet. Kommt es, erwartet der Bootlader anschließend
Daten, welche er als neues Anwendungsprogramm in den Programmspeicher schreibt.

## Asymmetrische Kryptographie

Asymmetrische Kryptographie bezeichnet im Kontrast zu symmetrischer Kryptographie
eine Form der Verschlüsselung, für die der Schlüssel zum Ver- bzw. Entschlüsseln
jeweils unterschiedlich ist. Müssen bei symmetrischer Kryptographie ver- und entschlüsselnde
Instanz den selben Schlüssel kennen und diesen auf irgendeinem Wege austauschen
oder gegenseitig aushandeln (vgl. [Diffie-Hellman Schlüsselaustausch][wiki-diffie-hellman]),
ist dies bei asymmetrischer Kryptographie nicht nötig. Hier erzeugt jeder Teilnehmer
ein Schlüsselpaar, bestehend aus einem öffentlichen und einem privaten Schlüssel.
Der öffentliche Schlüssel kann veröffentlicht werden oder dem Kommunikationspartner
auf anderen unsicheren Wegen zur Verfügung gestellt werden, während der private
Schlüssel geheim gehalten werden muss. Anschließend wird eine Nachricht
mit dem öffentlichen Schlüssel verschlüsselt und kann nur mit dem privaten wieder entschlüsselt
werden. Die Signatur verläuft genau anders herum. Mit dem privaten Schlüssel wird
signiert und mit dem öffentlichen verifiziert.

Das BSI weist in seiner Technischen Richtlinie [@bsi-krypto, Seite 28] zudem auf
die geringe Effizienz von asymmetrischen Verfahren gegenüber symmetrischen hin. Deshalb
werden asymmetrische Verfahren in der Praxis häufig kombiniert mit symmetrischen
verwendet. So wird im Falle der Verschlüsselung die eigentliche Information symmetrisch
verschlüsselt und der symmetrische Schlüssel anschließend wiederum asymmetrisch.
Diese Kombination nennt man Hybrid-Verschlüsselung.
Im Falle einer elektronischen Signatur wird ein kryptographischer Hashwert wie
MD-5^[Nicht mehr sicher], SHA-2 oder SHA-3 über die Daten gebildet und dieser anschließend signiert.

Es gibt unterschiedliche Algorithmen und Wege, einen veröffentlichten Schlüssel
einer bestimmten Instanz zuzuordnen. Auf diese wird in @sec:kryptographie weiter
eingegangen und diese gegeneinander verglichen.

[wiki-diffie-hellman]: https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange

## ASN.1 {#sec:asn1}

Die ASN.1, ausgeschrieben Abstract Syntax Notation number One, wurde in ihrer
ersten Spezifikation bereits im Jahre 1984 durch die CCITT
(Comité Consultatif International Téléphonique et Télégraphique),
heute ITU-T (International Telecommunication Union - Telecommunication Standardization Sector),
veröffentlicht [@itu-asn1]. Seitdem wurde sie in zahlreichen Neuauflagen erweitert
und verbessert und ist nach wie vor in Benutzung. So verwendet z. B. die 3GPP die
ASN.1 in den [Spezifikationen moderner Mobilfunkstandarts][3gpp-asn1] wie LTE.

Die ITU-T definiert ASN.1 wie folgt: "Abstract Syntax Notation number One
is a standard that defines a formalism
for the specification of abstract data types." [@itu-asn1]

Im Rahmen dieser Arbeit hat die ASN.1 Relevanz durch ihre Verwendung in unterschiedlichen
Spezifikationen. Einerseits ist die X.509 Public-Key-Infrastructure
inkl. des verwendeten Zertifikatsformats und der verwendeten Algorithmen zu nennen
[@rfc5280; @rfc3279]. Andererseits wird auch in der PKCS#1-Spezifikation [@rfc8017]
die ASN.1 verwendet, um die verwendeten Formate in Zusammenhang mit RSA zu definieren.

Eine besondere Bedeutung kommen im Rahmen der Kryptographie auch den unterschiedlichen
Kodierungsrichtlinien für ASN.1 Module zu. Hier sind vor allem BER, DER und PER
[@x690; @laymansguide_asn1] aufzuzählen. Für die Kryptographie hat besonders DER
Relevanz, da es für jedes mögliche Konstrukt genau einen Weg festlegt, es zu kodieren.
Durch Vermeidung von Mehrdeutigkeiten ist so ein systemübergreifender Austausch
von verschlüsselten Inhalten und eine einfache Verifikation möglich.

[3gpp-asn1]: http://www.3gpp.org/specifications/61-asn-1
