import message
import crcmod.predefined

class MessageParser:
    
    def __init__(self):
        self.state = 0
        
        self.states = { 0 : self.__header,
                       1 : self.__extended_header,
                       2 : self.__length,
                       3 : self.__payload,
                       4 : self.__crc,
            }
        self.current = null
        self.bytes_in_state = 0
        self.state_tmp = b''
        self.message_complete = false
        self.crc = crcmod.predefined.Crc('kermit')
        
    def process(self, byte):
        self.bytes_in_state +=1
        self.states[self.state](byte)
        if( self.message_complete ):
            tmp = self.current
            self.message_complete = False
            self.current = null
            return tmp
        
    def __change_state(state):
        self.state = state
        self.bytes_in_state = 0
        self.state_tmp = b''
        
    def __header(self, byte):
        if(byte & 0x80):
            # long message
            self.crc = crcmod.predefined.Crc('kermit')
            version = byte & 0x60
            packagetype = byte & 0x18
            packageinfo = byte & 0x07
            self.current = new message(packagetype, packageinfo)
            if(packagetype == 0 and packageinfo == 7):
                __change_state(1)
            else:
                __change_state(2)
        else:
            # there would be the chat style message implemented
            raise NotImplementedError
            self.crc = crcmod.predefined.Crc('crc-8')
    def __extended_header(self, byte):
        self.current.packageinfo = byte
        __change_state(2)
    def __length(self, byte):
        self.current.length = byte & 0x7f
        if(byte & 0x80):
            self.current.length = current.length << 7
        else:
            if(self.current.packagetype == 3):
                __change_state(3)
            else:
                __change_state(4)
        
    def __payload(self, byte):
        self.current.length -= 1
        self.current.payload += byte
        if(self.current.length == 0):
            __change_state(4)
        
    def __crc(self, byte):
        if(self.current.packagetype <= 4):
            state_tmp += byte
            if(bytes_in_state == 2):
                if( self.crc.digest == state_tmp):
                    self.message_complete = true
                else:
                    #debug
                    if(state_tmp)
                __change_state(0)
        else:
            #thats for the CRC-8 in chatstyle messages
            raise NotImplementedError
            __change_state(0)
            
        
        
