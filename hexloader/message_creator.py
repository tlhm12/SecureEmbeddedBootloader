import crcmod.predefined

class MessageCreator:
    
    def __init__(self):
        ;
    

    def eigth_to_seven( number ):
        """converts an integer to base 128 = 2^7 """
        base_128 = bytearray()
        while(number > 0):
            tmp = number & 0x7f
            number = number >> 7
            if( len(base_128) ):
                tmp = tmp | 0x80 
            base_128 = tmp.to_bytes(1, byteorder ='big') + base_128
        return base_128

    def create_request( port, totallength ):
        crc16 = crcmod.predefined.Crc('kermit')
        header = (0x80 | port).to_bytes(1, byteorder = 'big')
        crc16.update(header)
        lengthbytes = eigth_to_seven(totallength)
        crc16.update(lengthbytes)
        message = b''.join([header,lengthbytes, crc16.digest()])
        return message

    def create_ack( subtype, info ):
        ;
        
    def create_content( packagenumber, content):
        crc16 = crcmod.predefined.Crc('kermit')
        packagenumber = packagenumber % 8
        header = ( 0x80 | 0x10 | packagenumber).to_bytes(1, byteorder= 'big')
        lengthbytes = eigth_to_seven(len(content))
            
        message = b''.join([header, lengthbytes, content, crc16.digest()])
        return message
