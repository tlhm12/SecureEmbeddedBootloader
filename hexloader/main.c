#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>
#include <stdint.h>
#include <bits/fcntl-linux.h>
#include <ctype.h>

int open_serialLine(const char* port, int baudrate);
void trim(char * s);

int main(int argc, char *argv[])
{
    int serialfd = open_serialLine(argv[1], B9600);

    char boot_magic_string[] = "p";
    bool entered_bootloader = false;
    printf("waiting for bootloader:");
    fflush(stdout);
    do{
        uint8_t buf = 0;
        write(serialfd, boot_magic_string,sizeof(boot_magic_string));
        printf(".");
        fflush(stdout);
        int cnt = read(serialfd, &buf, 1);
        if(buf == '!')
            entered_bootloader = true;
    }while(!entered_bootloader);
    printf("\nwe entered bootloader!\n");
    
    FILE *hexfile = fopen(argv[2], "r");
    if(hexfile == NULL){
        perror("can't open hexfile");
        return 1;
    }
    int cnt = 0;
    //uint8_t buf[20];
    char line[255];
    while(fgets(line,sizeof(line),hexfile) != NULL){
        trim(line);
        printf("%s\r\n", line);
        char *remaining_line = line;
        uint8_t charsToSend;
        while(remaining_line - line < strlen(line)){
            char buf;
            charsToSend = strlen(remaining_line) > 1 ? 1 : strlen(remaining_line);
            write(serialfd, remaining_line,charsToSend);
            remaining_line += charsToSend;
            usleep(10000);
        }
      }
    printf("ready\n");

    return EXIT_SUCCESS;
}

/**
open a serial port
@param port the port to open e.g. /dev/ttyUSB0
@param baudrate the baudrate to use. Have to be one of the folling
  B0, B50, B75, B110, B134, B150, B200, B300, B600, B1200, B1800, B2400,
  B4800, B9600, B19200, B38400, B57600, B115200, B230400
@return filedescriptor or errorcode if < 0
*/
int open_serialLine(const char* port, int baudrate){
  int fd;
  struct termios serialConfig;

  switch (baudrate) {
    case B0:
    case B50:
    case B75:
    case B110:
    case B134:
    case B150:
    case B200:
    case B300:
    case B600:
    case B1200:
    case B1800:
    case B2400:
    case B4800:
    case B9600:
    case B19200:
    case B38400:
    case B57600:
    case B115200:
    case B230400:
      break;
    default:
      printf("unsupported baudrate\n" );
      return -2;
  }

  fd = open(port, O_RDWR); //open serial device in READ Write MODE

  if( fd == -1){
      perror("unable to open port");
      exit(2);
  }

  memset(&serialConfig,0, sizeof(serialConfig)); /* clear the struct */

  serialConfig.c_cflag = CS8 | CLOCAL | CREAD;

  cfsetspeed(&serialConfig, baudrate); //setze baudrate
  serialConfig.c_iflag = IGNPAR | ICRNL | IXON; // ignoriere paritätsfehler
  serialConfig.c_oflag = 0;

  serialConfig.c_lflag = 0;

  serialConfig.c_cc[VTIME] = 1;
  serialConfig.c_cc[VMIN] = 1; /* blockiere read() bis mindestens 1 zeichen empfangen wurde */

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &serialConfig);

  return fd;
}

void trim(char * s) {
    char * p = s;
    int l = strlen(p);

    while(isspace(p[l - 1])) p[--l] = 0;
    while(* p && isspace(* p)) ++p, --l;

    memmove(s, p, l + 1);
}

