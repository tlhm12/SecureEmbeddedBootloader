import argparse
import serial
import re
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA

message = "Hallo Welt!\n"

def connect():
    ser = serial.Serial("/dev/ttyUSB0")  # Open named port
    ser.baudrate = 9600            # Set baud rate to 9600
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    data = ser .read(1)
    while data != b'B':
        data = ser .read(1)
    ser.write(b's')
    signature_verified = False
    line = ""
    ser.write( bytes([0x83, 0x07, 0x00, 0x00]) )
    while(not signature_verified):
        data = ser.read_all()
        if len(data) > 0:
            for( b in data ):
                process(b)
            print(":".join("{:02x}".format(c) for c in data))
            print("end\n")
    ser.close()
    
    

def main():
    """The function parses a given file or stdin for OpenPGP packages.

    The file can be given in commandline arguments.
    """
    parser = argparse.ArgumentParser(description='Parse an OpenPGP key')

    #parser.add_argument('input_file', metavar='Input File',
     #                   type=argparse.FileType('rb'), nargs='?',
     #                   default=sys.stdin.buffer)
    parser.add_argument('--version', action='version', version='v. 0.1')

    args = parser.parse_args()
    print(args)
    connect()

# script as advised by
# [google style guide](https://google.github.io/styleguide/pyguide.html#Main)
if __name__ == '__main__':
    main()
