
https://datacenteroverlords.com/2012/03/01/creating-your-own-ssl-certificate-authority/

- openssl genrsa -out rootCA.key 2048
- openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem
- openssl genrsa -out updater.key 512
- openssl req -new -key updater.key -out updater.csr
- openssl x509 -req -in updater.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out updater.crt -days 500 -sha256
- openssl rsa -inform PEM -outform DER -out rootCA.der< rootCA.key
- openssl rsa -inform PEM -text < rootCA.key
