# Embedded Secure Bootloader
Bootloader for Embedded Systems that is
- small enough to run on common 8 and 16 bit MCUs
- Authentifies the new firmware via Public Key Infrastructure e.g. All keys signed by a specific root CA known by the system
- plugable communication interfaces e.g. UART, I2C, CAN
- easy to port to new MCUs

## write address to eeprom
- makeeepromhex.pl > lib.eep
- avrdude -c avrispmkII -p m328 -P usb -U eeprom:w:lib.eep
