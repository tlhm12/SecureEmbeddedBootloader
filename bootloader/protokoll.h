#ifndef __PROTOKOLL_H__
#define __PROTOKOLL_H__

#include <stdint.h>

typedef uint8_t* (*func_get_buffer)(uint32_t * );
typedef uint8_t* (*func_buffer_full)();
typedef uint8_t* (*func_transmission_finished)();

typedef struct uart_transfer_application {
    func_get_buffer get_buffer;
    func_buffer_full buffer_full;
    func_transmission_finished transmission_finished;
} Uart_Transfer_Application_t;

int read(uint8_t *buffer, uint8_t max_bytes_to_read);

void register_application(uint8_t port, struct uart_transfer_application* application);
void unregister_applicatoin(uint8_t port);

#endif
