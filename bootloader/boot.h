#ifndef _BOOT_H_
#define _BOOT_H_

#include <stdint.h>
#include <avr/boot.h>

/* Zustände des Bootloader-Programms */
#define BOOT_STATE_EXIT	        0
#define BOOT_STATE_PARSER       1

void program_page (uint32_t page, uint8_t *buf);


struct bootloader_state {
    uint16_t        /* Intel-HEX Checksumme zum Überprüfen des Daten */
                    hex_check;
                    /* Positions zum Schreiben in der Datenpuffer */
    uint16_t        flash_cnt;
                    /* Empfangszustandssteuerung */
    uint16_t        /* Intel-HEX Zieladresse */
           	    hex_addr,
                    /* Zu schreibende Flash-Page */
                    flash_page;

    uint8_t         parser_state;

                    /* Position zum Schreiben in den HEX-Puffer */
    uint8_t         hex_cnt,
                    /* Puffer für die Umwandlung der ASCII in Binärdaten */
                    hex_buffer[5],
                    /* Intel-HEX Datenlänge */
                    hex_size,
                    /* Zähler für die empfangenen HEX-Daten einer Zeile */
                    hex_data_cnt,
                    /* Intel-HEX Recordtype */
                    hex_type,
                    /* empfangene HEX-Checksumme */
                    hex_checksum,
                    /* Flag zum ermitteln einer neuen Flash-Page */
                    flash_page_flag,
                    /* Datenpuffer für die Hexdaten*/
                    flash_data[SPM_PAGESIZE]; //SPM_PAGESIZE defined in avr/boot.h
};

typedef struct bootloader_state bootloader_state_t;
#endif
