#include "dyn_lib.h"
#include <avr/pgmspace.h>
#include <setjmp.h>

const char s_init_mod000[] PROGMEM = "init_mod000";
const char s_mod000_rgb[] PROGMEM = "mod000_rgb";
const char s_uart_init[] PROGMEM = "uart_init";
const char s_uart_getc[] PROGMEM = "uart_getc";
const char s_uart_putc[] PROGMEM = "uart_putc";
const char s_uart_puts[] PROGMEM = "uart_puts";
const char s_sha256_init[] PROGMEM = "sha256_init";
const char s_sha256_nextBlock[] PROGMEM = "sha256_nextBlock";
const char s_sha256_lastBlock[] PROGMEM = "sha256_lastBlock";
const char s_sha256_ctx2hash[] PROGMEM = "sha256_ctx2hash";
const char s_rsa_sign[] PROGMEM = "rsa_sign";
const char s_mpConvFromOctets_P[] PROGMEM = "mpConvFromOctets_P";
const char s_compute_hash[] PROGMEM = "compute_hash";

/* Strings take a lot of space in RAM and doesn't need to be modified or accessed
 * often. Thus it makes sense to store these in PROGMEM where they need to be stored
 * nevertheless to be loaded into RAM in the Programm preample (__do_copy_data).
 * So we store the them in PROGMEM. An issue resulting from this is the more complex
 * access that produces more Instructions during access and hence enlarge the space
 * needed in Progmem. Especially in the bootloader section this space is very rare
 * (4 kB).
 *
 * We need a way to reduce instruction needed. A casual way is to put the similarly
 * repeating lines in a loop. To do this we need the both the string we search for
 * as well as the variable we want to assign a the pointer to be linked and
 * accessible through a way appropriate for a loop e.g. an index.
 *
 * A two dimentional array doesn't work for us cause we have two different data
 * types. One char pointer in PROGMEM and a void pointer in RAM cause we need to
 * modify it. We ensure the link by the same index. Thus the array lib_string_tbl
 * - containing the function name strings for query - have to have the same amount
 * of items than the lib_addr_tbl array containing the function pointer.
 *
 * If we store function pointers this way we need another way to call the functions.
 * Posibilties involve function like macros and wrapper functions that have the
 * same signature as the real function but simply call the real function via
 * the function pointer. If the order of functions is changed for some reason thats
 * not a problem cause the order is only relevant in the current executable object.
 */


/* TODO(lehmann@tommy-lehmann.de): lib_string_tbl should also be in PROGMEM */
static PGM_P lib_string_tbl[] =
{
    s_init_mod000,
    s_mod000_rgb,
    s_uart_init,
    s_uart_getc,
    s_uart_putc,
    s_uart_puts,
    s_sha256_init,
    s_sha256_nextBlock,
    s_sha256_lastBlock,
    s_sha256_ctx2hash,
    s_rsa_sign,
    s_mpConvFromOctets_P,
    s_compute_hash
};

void* lib_addr_tbl[ sizeof( lib_string_tbl ) / sizeof( char * ) ];

void load_addresses(void){
    jmp_buf jmp;
    void* (*_init)(jmp_buf *);
    void* (*lib_addr)(const char *function);
    _init = 0x026b;
    /* _init() laut lib.map an Stelle 0x04b0 -> 4d6/2=26b;
     * Funktionsaufrufe werden an Word-Adressen gemacht
     */
    lib_addr =_init( &jmp );
    for( int i = 0; i < (sizeof( lib_string_tbl ) / sizeof( char * )); i++ )
    {
        char buf[20];
        strcpy_P( buf, lib_string_tbl[ i ] );
        /*
         * strcpy_P(buf, (PGM_P)pgm_read_word(&(lib_string_tbl[i])));
         */
        lib_addr_tbl[ i ] = lib_addr( buf );
    }
}
