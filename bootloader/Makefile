uartlibdir = uartlibrary

# MCU name, MOD000, MOD015=atmega328, MOD020=atmega168
MCU = atmega328
#MCU = atmega168
#MCU =atmega88
#MCU =atmega8
#MCU = at90s2313

# Processor frequency.
#     This will define a symbol, F_CPU, in all source code files equal to the
#     processor frequency. You can then use this symbol in your source code to
#     calculate timings. Do NOT tack on a 'UL' at the end, this will be done
#     automatically to create a 32-bit value in your source code.
#     Typical values are:
#         F_CPU =  1000000
#         F_CPU =  1843200
#         F_CPU =  2000000
#         F_CPU =  3686400
#         F_CPU =  4000000
#         F_CPU =  7372800
#         F_CPU =  8000000
#         F_CPU = 11059200
#         F_CPU = 14745600
#         F_CPU = 16000000
#         F_CPU = 18432000
#         F_CPU = 20000000
F_CPU = 16000000

CC = avr-gcc -c -mmcu=$(MCU)
LD = avr-gcc -mmcu=$(MCU)
RM = rm

OBJCOPY = avr-objcopy
SIZE = avr-size

OPT = s

CSTANDARD = -std=gnu99

CDEFS = -DF_CPU=$(F_CPU)UL

EXTRAINCDIRS = ./Library ./Library/avr-crypto-lib ./Library/BigDigits-2.6.1

CFLAGS = -g$(DEBUG)
CFLAGS += $(CDEFS)
CFLAGS += -O$(OPT)
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -fshort-enums
CFLAGS += -Wall
CFLAGS += -Wstrict-prototypes
CFLAGS += -fstack-usage
#CFLAGS += -mshort-calls
#CFLAGS += -fno-unit-at-a-time
#CFLAGS += -Wundef
#CFLAGS += -Wunreachable-code
#CFLAGS += -Wsign-compare
#CFLAGS += -Wa,-adhlns=$(<:%.c=$(OBJDIR)/%.lst)
#CFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))
CFLAGS += $(CSTANDARD)

CPPFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))

LDFLAGS = -Wl,-Map=$(patsubst %.elf,%.map,$@)

HEXSIZE = $(SIZE) --target=$(FORMAT) $(TARGET).hex
ELFSIZE = $(SIZE) $(TARGET).elf

.PHONY: all

all: boot.hex application.hex lib.hex

programm_boot: boot.hex
	avrdude -c avrispmkII -p m328 -P usb -U flash:w:$<

programm: boot.hex application.hex lib.hex
	cat application.hex | grep -v :00000001FF |cat - lib.hex | grep -v :00000001FF | cat - boot.hex  > tmp.hex
	avrdude -c avrispmkII -p m328 -P usb -U flash:w:tmp.hex

.PHONY: application.hex lib.hex
application.hex:
	$(MAKE) -C Application application.hex
	cp Application/application.hex ./

lib.hex:
	$(MAKE) -C Library lib.hex
	cp Library/lib.hex ./

.PHONY: install
install: programm

boot.hex: boot.elf
	$(OBJCOPY)  -O ihex -R .eeprom -R .fuse -R .lock -R .signature $< $@

boot.elf: boot.o hexparser.o dyn_lib.o key.o protokoll.o application_hash.o
	$(LD) $(LDFLAGS) -Wl,-Ttext=0x7000,--section-start=.data=0x800180 -o $@ $^

boot.o: boot.c boot.h

hexparser.o: hexparser.c hexparser.h

dyn_lib.o: dyn_lib.c dyn_lib.h

key.o: key.c key.h

protokoll.o: protokoll.c protokoll.h

application_hash.o: application_hash.c application_hash.h

%.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

fuse:
	avrdude -c avrispmkII -p m328 -P usb -U hfuse:w:0xD8:m -U lfuse:w:0xFF:m -U efuse:w:0x05:m

.PHONY: clean
clean:
	$(RM) -rf *.o
	$(RM) -rf *.elf
	$(RM) -rf *.hex
	$(MAKE) -C Application -f Makefile clean
	$(MAKE) -C Library -f Makefile clean
