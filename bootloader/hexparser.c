#include "hexparser.h"
#include <string.h>
#include "uart.h"
#include <util/delay.h>
#include "dyn_lib.h"

/* Zustände des Hex-File-Parsers */
#define PARSER_STATE_START      0
#define PARSER_STATE_SIZE       1
#define PARSER_STATE_ADDRESS    2
#define PARSER_STATE_TYPE       3
#define PARSER_STATE_DATA       4
#define PARSER_STATE_CHECKSUM   5
#define PARSER_STATE_ERROR      6

#define START_SIGN              ':'      /* Hex-Datei Zeilenstartzeichen */

static uint16_t hex2num(const uint8_t *ascii, uint8_t num);

void state_start(bootloader_state_t *state, uint16_t c){
    if((uint8_t)c == START_SIGN)
    {
        state->parser_state = PARSER_STATE_SIZE;
        state->hex_cnt = 0;
        state->hex_check = 0;
    }
}

void state_size(bootloader_state_t *state, uint16_t c){
    state->hex_buffer[state->hex_cnt++] = (uint8_t)c;
    if(state->hex_cnt == 2)
    {
        state->parser_state = PARSER_STATE_ADDRESS;
        state->hex_cnt = 0;
        state->hex_size = (uint8_t)hex2num(state->hex_buffer, 2);
        state->hex_check += state->hex_size;
    }
}

void state_address(bootloader_state_t *state, uint16_t c){
    state->hex_buffer[state->hex_cnt++] = (uint8_t)c;
    if(state->hex_cnt == 4)
    {
        state->parser_state = PARSER_STATE_TYPE;
        state->hex_cnt = 0;
        state->hex_addr = hex2num(state->hex_buffer, 4);
        state->hex_check += (uint8_t) state->hex_addr;
        state->hex_check += (uint8_t) (state->hex_addr >> 8);
        if(state->flash_page_flag)
        {
            state->flash_page = state->hex_addr - state->hex_addr % SPM_PAGESIZE;
            state->flash_page_flag = 0;
        }
    }
}

void state_type(bootloader_state_t *state, uint16_t c){
    state->hex_buffer[state->hex_cnt++] = (uint8_t)c;
    if(state->hex_cnt == 2)
    {
        state->hex_cnt = 0;
        state->hex_data_cnt = 0;
        state->hex_type = (uint8_t)hex2num(state->hex_buffer, 2);
        state->hex_check += state->hex_type;
        switch(state->hex_type)
        {
            case 0: state->parser_state = PARSER_STATE_DATA; break;
            case 1: state->parser_state = PARSER_STATE_CHECKSUM; break;
            default: state->parser_state = PARSER_STATE_DATA; break;
        }
    }
}

void state_data(bootloader_state_t *state, uint16_t c){
    state->hex_buffer[state->hex_cnt++] = (uint8_t)c;
    if(state->hex_cnt == 2)
    {
        uart_putc('.');
        state->hex_cnt = 0;
        state->flash_data[state->flash_cnt] = (uint8_t)hex2num(state->hex_buffer, 2);
        state->hex_check += state->flash_data[state->flash_cnt];
        state->flash_cnt++;
        state->hex_data_cnt++;
        if(state->hex_data_cnt == state->hex_size)
        {
            state->parser_state = PARSER_STATE_CHECKSUM;
            state->hex_data_cnt=0;
            state->hex_cnt = 0;
        }
        /* Puffer voll -> schreibe Page */
        if(state->flash_cnt == SPM_PAGESIZE)
        {
            uart_puts("P\n\r");
            _delay_ms(100);
            program_page((uint16_t)state->flash_page, state->flash_data);
            memset(state->flash_data, 0xFF, sizeof(state->flash_data));
            state->flash_cnt = 0;
            state->flash_page_flag = 1;
        }
    }
}

uint8_t state_checksum(bootloader_state_t *state, uint16_t c){
    state->hex_buffer[state->hex_cnt++] = (uint8_t)c;
    if(state->hex_cnt == 2)
    {
        state->hex_checksum = (uint8_t)hex2num(state->hex_buffer, 2);
        state->hex_check += state->hex_checksum;
        state->hex_check &= 0x00FF;
        /* Überprüfe Checksumme -> muss '0' sein */
        if(state->hex_check == 0) state->parser_state = PARSER_STATE_START;
        else state->parser_state = PARSER_STATE_ERROR;
        /* Dateiende -> schreibe Restdaten */
        if(state->hex_type == 1)
        {
            uart_puts("P\n\r");
            _delay_ms(100);
            program_page((uint16_t)state->flash_page, state->flash_data);
            return BOOT_STATE_EXIT;
        }
        return BOOT_STATE_PARSER;
    }
    return BOOT_STATE_PARSER;
}

uint8_t parser(bootloader_state_t *state, uint16_t c){
    switch(state->parser_state)
    {
        /* Warte auf Zeilen-Startzeichen */
        case PARSER_STATE_START: {
           state_start(state, c);
            break;
        }
        /* Parse Datengröße */
        case PARSER_STATE_SIZE:{
            state_size(state,c);
            break;
        }
        /* Parse Zieladresse */
        case PARSER_STATE_ADDRESS:{
            state_address(state, c);
            break;
        }
        /* Parse Zeilentyp */
        case PARSER_STATE_TYPE:{
            state_type(state,c);
            break;
        }
        /* Parse Flash-Daten */
        case PARSER_STATE_DATA:{
            state_data(state, c);
            break;
        }
        /* Parse Checksumme */
        case PARSER_STATE_CHECKSUM:{
            return state_checksum(state,c);
            break;
        }
        /* Parserfehler (falsche Checksumme) */
        case PARSER_STATE_ERROR:{
            uart_putc('#');
            break;
        }
        default:
            break;
    }
    return BOOT_STATE_PARSER;
}

static uint16_t hex2num(const uint8_t *ascii, uint8_t num){
    uint8_t i;
    uint16_t val = 0;

    for(i = 0; i<num; i++){
        uint8_t c = ascii[i];

        if (c >= '0' && c <= '9')
            c -= '0';
        else if ( c >= 'A' && c <= 'F')
            c-= 'A' - 10;
        else if (c >= 'a' && c <= 'f')
            c-= 'a' - 10;

        val = 16 * val + c;
    }

    return val;
}
