#ifndef __KEY_H__
#define __KEY_H__

#include <stdint.h>
#include <avr/pgmspace.h>

extern const unsigned char n[64] PROGMEM;

/* RSA private exponent */
extern const unsigned char d[64] PROGMEM;


/* RSA Public exponent */
extern const uint8_t e[3] PROGMEM;

#endif
