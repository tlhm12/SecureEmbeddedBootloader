#ifndef __DYN_LIB_H_
#define __DYN_LIB_H_
#include <stdint.h>
#include <sha256/sha256.h>
#include <rsa.h>
#define _need_size_t
#include <stddef.h>

    void load_addresses(void);


    extern void* lib_addr_tbl[];


    #define mod000_rgb(a) ((void (*)(uint8_t))(lib_addr_tbl[1]))(a)
    #define init_mod000() ((void (*)(void))(lib_addr_tbl[0]))()

    #define uart_init(a) ((void (*)(unsigned int))(lib_addr_tbl[2]))(a)
    #define uart_getc(a) ((unsigned int(*)(void))(lib_addr_tbl[3]))(a)
    #define uart_putc(a) ((void (*)(unsigned char))(lib_addr_tbl[4]))(a)
    #define uart_puts(a) ((void (*)(const char *))(lib_addr_tbl[5]))(a)

    # define sha256_init(state) ((void (*)(sha256_ctx_t *))(lib_addr_tbl[6]))(state)
    #define sha256_nextBlock(state, block) ((void (*) (sha256_ctx_t *, const void *))(lib_addr_tbl[7]))(state, block)
    #define sha256_lastBlock(state, block, length_b) ((void (*)(sha256_ctx_t *, const void *, uint16_t))(lib_addr_tbl[8]))(state, block, length_b)
    #define sha256_ctx2hash(dest, state) ((void (*)(sha256_hash_t *, const sha256_ctx_t *))(lib_addr_tbl[9])(dest,state)
    #define rsa_sign(algorithm, key, hash_digest, cb_hash, signature) ((int16_t (*)( enum algorithm_t, RsaPrivateKey_t *, uint8_t *, uint16_t, uint8_t *))(lib_addr_tbl[10]))(algorithm, key, hash_digest, cb_hash, signature)
    #define mpConvFromOctets_P(a, ndigits, c, nbytes) ((uint16_t (*)(DIGIT_T *, size_t, const unsigned char *, size_t))(lib_addr_tbl[11]))(a, ndigits, c, nbytes)
    #define compute_hash(algorithm, message, cb_message, hash_value) ((int16_t (*)(hfgen_ctx_t *, uint8_t *, uint16_t, uint8_t *))(lib_addr_tbl[12]))(algorithm, message, cb_message, hash_value)
#endif
