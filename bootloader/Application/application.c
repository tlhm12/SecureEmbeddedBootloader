#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "uart.h"
#include "mod000.h"

#define UART_BAUD_RATE	9600

int main(void)
{
    unsigned int 	c;
    void (*bootloader)( void ) = 0x0C00;  // Achtung Falle: Hier Word-Adresse

    uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
    init_mod000();
    sei();

    mod000_rgb(weiss);
    uart_puts_P("\n\rHier ist das Anwendungsprogramm...\n\r");

    for(;;)
    {
        c = uart_getc();
        if(!(c & UART_NO_DATA))
        {
            switch( (unsigned char)c)
            {
                case 'b':
                    uart_puts("\n\rSpringe zum Bootloader...\n\r");
                    _delay_ms(1000);
                    bootloader();
                    break;
                default:
                    uart_puts("\n\rDu hast folgendes Zeichen gesendet: ");
                    uart_putc((unsigned char)c);
                    break;
            }
        }
    }
    return 0;
}
