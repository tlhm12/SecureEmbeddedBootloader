#include "mod000.h"


void init_mod000(void){
    /* PB1 als Ausgang "GRUEN" und PB2 "BLAU"*/
    DDRB=0x06; PORTB=0x06;
    DDRC=0x00; PORTC=0xff;  // Eingang mit Pull Ups
    DDRD=0x40; PORTD=0x40;  // PD6 als Ausgang "Rot"
    return;
}


void mod000_rgb(uint8_t RGB){
    /* ROT == PD6(OC0A);
     * GRUEN ==PB1(OC1A);
     * BLAU==PB2(OC1B)
     */
    if(RGB&0x02){PORTB&=0xfd;} else{PORTB|=0x02;} //GRUEN
    if(RGB&0x04){PORTB&=0xfb;} else{PORTB|=0x04;} //BLAU
    if(RGB&0x01){PORTD&=0xbf;} else{PORTD|=0x40;} //ROT

    return;
}

void mod000_rgb_toggel(uint8_t RGB){
    static uint8_t TOG=0;

    if (TOG==0){mod000_rgb(0); TOG=1;} else {mod000_rgb(RGB); TOG=0;}

}

/*
void init_mod000_pwmrgb(void){
    INIT_T0_PK_PWM(1,0,1,0);

    PWM0ANABKOPPELN(1,0);
    INIT_T1_PK_PWM(1,1,1,0,255);
    PWM_T0(0, 0);
    PWM_T1(0,0);
}

void mod000_pwmrgb(RGBColor Farbe){
    PWM_T0(Farbe.R,0);
    PWM_T1(Farbe.G,Farbe.B);
}

*/
