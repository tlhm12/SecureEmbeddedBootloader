#ifndef _HEXPARSER_H_
#define _HEXPARSER_H_

#include "boot.h"
#include <stdint.h>

uint8_t parser(bootloader_state_t *state, uint16_t c);

#endif
