/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/* Compute x = one square root of a (mod p). Return -1 if root does not exist, 0 if successful. */
int mpModSqrt(DIGIT_T x[], const DIGIT_T a[], DIGIT_T p[], size_t ndigits)
{
	DIGIT_T r, m;
	int result;

	/* Allocate temp storage */
#ifdef NO_ALLOCS
	DIGIT_T q[MAX_FIXED_DIGITS];
	DIGIT_T n[MAX_FIXED_DIGITS];
	DIGIT_T y[MAX_FIXED_DIGITS];
	DIGIT_T b[MAX_FIXED_DIGITS];
	DIGIT_T k[MAX_FIXED_DIGITS];
	DIGIT_T e[MAX_FIXED_DIGITS];
	DIGIT_T t[MAX_FIXED_DIGITS];
	assert(ndigits <= MAX_FIXED_DIGITS);
#else
	DIGIT_T *q, *n, *y, *b, *k, *e, *t;
	q = mpAlloc(ndigits);
	n = mpAlloc(ndigits);
	y = mpAlloc(ndigits);
	b = mpAlloc(ndigits);
	k = mpAlloc(ndigits);
	e = mpAlloc(ndigits);
	t = mpAlloc(ndigits);
#endif

	/* Shanks-Tonelli Algorithm from [BLAKE] Algorithm II.8 and [INF] */

	/* 1. Let r, q be integers such that q is odd and p-1 = q*2^r */
	mpShortSub(q, p, 1, ndigits);
	for (r = 0; mpISEVEN(q, ndigits); r++) {
		mpShiftRight(q, q, 1, ndigits);
	}

	/* Catch special case */
	if (1 == r) {
		/* We have p == 3 (mod 4) so
		* set x <-- a^((p+1)/4) mod p and return.
		* { Note that in this case (p+1)/4 = (p>>2) + 1 } */
		mpShiftRight(k, p, 2, ndigits);
		mpShortAdd(k, k, 1, ndigits);
		mpModExp(x, a, k, p, ndigits);
		goto do_check;
	}

	/* 2. Choose (random) number n until one is found such that (n|p) = -1 */
	/* for n <-- 2 until (n|p)= -1 do n <-- n + 1 */
	mpSetDigit(n, 2, ndigits);
	while (mpJacobi(n, p, ndigits) != -1) {
		mpShortAdd(n, n, 1, ndigits);
	}

	/* Initialize: { Steps 1 to 3 could be pre-computed }*/
	/* 3. y <-- n^q */
	mpModExp(y, n, q, p, ndigits);

	/* 4. b <-- a^((q-1)/2) */
	/* { (q-1)/2 = q >> 1 since q is odd } */
	mpShiftRight(k, q, 1, ndigits);
	mpModExp(b, a, k, p, ndigits);
	/* 5. x <-- a*b = a^((q+1)/2) */
	mpModMult(x, a, b, p, ndigits);
	/* 6. b <-- b*x = a^q */
	mpModMult(b, b, x, p, ndigits);
	/* 7. while b != 1 do: */
	while (!mpShortIsEqual(b, 1, ndigits)) {
		/* 8. Find smallest m such that b^(2^m) == 1 mod p */
		mpSetEqual(t, b, ndigits); /* t = b^(2^0) = b */
		for (m = 0; m < r && !mpShortIsEqual(t, 1, ndigits); m++) {
			/* t = t^2 = b^(2^(m-1))^2 = b^(2^m) */
			mpModSquare(t, t, p, ndigits);
		}
		/* 9. t <-- y^(2^(r-m-1)) */
		mpSetDigit(e, 1, ndigits);
		mpShiftLeft(e, e, (r - m - 1), ndigits); /* = 2^(r-m-1) */
		mpModExp(t, y, e, p, ndigits);
		/* 10. y <-- t^2 = y^(2^(r-m)) */
		mpModSquare(y, t, p, ndigits);
		/* 11. r <-- m */
		r = m;
		/* 12. x <-- x*t = x.y^(2^(r-m-1)) */
		mpModMult(x, x, t, p, ndigits);
		/* 13. b <-- b*y = b.y^(2^(r-m)) */
		mpModMult(b, b, y, p, ndigits);
	}

	/* 14. return x or NO_ROOT_EXISTS */

do_check:
	/* Check that x^2 = a */
	mpModSquare(k, x, p, ndigits);
	result = (mpEqual(k, a, ndigits) ? 0 : -1); /* 0 => OK, -1 => NO_ROOT_EXISTS */

	/* Clear up */
	mpDESTROY(q, ndigits);
	mpDESTROY(n, ndigits);
	mpDESTROY(y, ndigits);
	mpDESTROY(b, ndigits);
	mpDESTROY(k, ndigits);
	mpDESTROY(e, ndigits);
	mpDESTROY(t, ndigits);

	return result;
}

