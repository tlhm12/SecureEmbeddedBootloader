/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/** Set w = u/2 (mod p) - actually works modulo any odd integer p */
void mpModHalve(DIGIT_T w[], const DIGIT_T u[], const DIGIT_T p[], size_t ndigits)
{
	int carry;

	if (mpISODD(u, ndigits)) {
		/* If u is odd then add p and then right-shift by one bit */
		/* w <-- (u + p)/2 {do not reduce sum modulo p} */
		carry = mpAdd(w, u, p, ndigits);
		mpShiftRight(w, w, 1, ndigits);
		/* Cope with overflow {NB assumes exact number of digits} */
		if (carry)
			mpSetBit(w, ndigits, (ndigits * BITS_PER_DIGIT) - 1, 1);
	}
	else {
		/* If u is even then u/2 mod p is same as u/2 i.e. u right-shifted by one bit */
		mpShiftRight(w, u, 1, ndigits);
	}
}
