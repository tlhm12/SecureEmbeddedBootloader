/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/** Returns true if a == b, else false. Not constant-time. */
int mpEqual(const DIGIT_T a[], const DIGIT_T b[], size_t ndigits)
{

	/* if (ndigits == 0) return -1; // deleted [v2.5] */

	while (ndigits--)
	{
		if (a[ndigits] != b[ndigits])
			return 0;	/* False */
	}

	return (!0);	/* True */
}

/** Returns sign of (a - b) as 0, +1 or -1. Not constant-time. */
int mpCompare(const DIGIT_T a[], const DIGIT_T b[], size_t ndigits)
{
	/* if (ndigits == 0) return 0; // deleted [v2.5] */

	while (ndigits--)
	{
		if (a[ndigits] > b[ndigits])
			return 1;	/* GT */
		if (a[ndigits] < b[ndigits])
			return -1;	/* LT */
	}

	return 0;	/* EQ */
}

/** Returns true if a == 0, else false. Not constant-time. */
int mpIsZero(const DIGIT_T a[], size_t ndigits)
{
	size_t i;

	/* if (ndigits == 0) return -1; // deleted [v2.5] */

	for (i = 0; i < ndigits; i++)	/* Start at lsb */
	{
		if (a[i] != 0)
			return 0;	/* False */
	}

	return (!0);	/* True */
}
