/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/* Compute w = u - v (mod m) where 0 <= u,v < m and w != v */
void mpModSub(DIGIT_T w[], const DIGIT_T u[], const DIGIT_T v[], const DIGIT_T m[], size_t ndigits)
{
	/* We need a temp variable t [to allow mpModSub(w,w,v,...)] */
#ifdef NO_ALLOCS
	DIGIT_T t[MAX_FIXED_DIGITS];
	assert(ndigits <= MAX_FIXED_DIGITS);
#else
	DIGIT_T *t;
	t = mpAlloc(ndigits);
#endif
	/* w <-- m - v [always > 0] */
	mpSubtract(t, m, v, ndigits);
	/* w <-- w + u (mod m) */
	mpModAdd(w, u, t, m, ndigits);

	mpDESTROY(t, ndigits);
}

