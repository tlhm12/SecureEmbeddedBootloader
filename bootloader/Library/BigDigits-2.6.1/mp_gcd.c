/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

int mpGcd(DIGIT_T d[], const DIGIT_T aa[], const DIGIT_T bb[], size_t ndigits)
{	
	/* Computes d = gcd(a, b) */
	/* Changed to Binary GCD in [v2.3] 
	 * Ref: Menezes Algorithm 14.54 plus some of Cohen Algorithm 1.3.5. 
	 */

	unsigned int k;

/*	Allocate temp storage */
#ifdef NO_ALLOCS
	DIGIT_T a[MAX_FIXED_DIGITS];
	DIGIT_T b[MAX_FIXED_DIGITS];
	DIGIT_T r[MAX_FIXED_DIGITS];
	DIGIT_T t[MAX_FIXED_DIGITS];
	assert(ndigits <= MAX_FIXED_DIGITS);
#else
	DIGIT_T *a, *b, *r, *t;
	a = mpAlloc(ndigits);
	b = mpAlloc(ndigits);
	r = mpAlloc(ndigits);
	t = mpAlloc(ndigits);
#endif
	
	/* Copy input into temp vars */
	mpSetEqual(a, aa, ndigits);
	mpSetEqual(b, bb, ndigits);

	/* 1. [Reduce size once] */
	if (mpCompare(a, b, ndigits) < 0)
	{	/* Exchange a and b */
		mpSetEqual(t, a, ndigits);
		mpSetEqual(a, b, ndigits);
		mpSetEqual(b, t, ndigits);
	}
	/* If b = 0 output a and stop */
	if (mpIsZero(b, ndigits))
	{
		mpSetEqual(d, a, ndigits);
		goto done;
	}
	/* Set r <-- a mod b, a <-- b, b <-- r */
	mpModulo(r, a, ndigits, b, ndigits);
	mpSetEqual(a, b, ndigits);
	mpSetEqual(b, r, ndigits);
	/* If b = 0 output a and stop */
	if (mpIsZero(b, ndigits))
	{
		mpSetEqual(d, a, ndigits);
		goto done;
	}

	/* 2. [Compute power of 2] */
	k = 0;	/* g = 2^k <-- 1 */
	while (mpISEVEN(a, ndigits) && mpISEVEN(b, ndigits))
	{	/* While a and b are even */
		mpShiftRight(a, a, 1, ndigits);	/* a <-- a/2 */
		mpShiftRight(b, b, 1, ndigits);	/* b <-- b/2 */
		k++;	/* g <-- 2g */
	}
	while (!mpIsZero(a, ndigits))
	{
		/* 3. [Remove initial powers of 2] */
		while (mpISEVEN(a, ndigits))
			mpShiftRight(a, a, 1, ndigits);	/* a <-- a/2 until a is odd */
		while (mpISEVEN(b, ndigits))
			mpShiftRight(b, b, 1, ndigits);	/* b <-- b/2 until b is odd */
		/* 4. [Subtract] t = |a - b|/2 */
		if (mpCompare(b, a, ndigits) > 0)
			mpSubtract(t, b, a, ndigits);
		else
			mpSubtract(t, a, b, ndigits);
		mpShiftRight(t, t, 1, ndigits);
		/* if a >= b then set a <-- t otherwise set b <-- t */
		if (mpCompare(a, b, ndigits) >= 0)
			mpSetEqual(a, t, ndigits);
		else
			mpSetEqual(b, t, ndigits);

		/* 5. [Loop] */
	}
	/* Output (2^k.b) and stop */
	mpShiftLeft(d, b, k, ndigits);	
done:

	mpDESTROY(a, ndigits);
	mpDESTROY(b, ndigits);
	mpDESTROY(r, ndigits);
	mpDESTROY(t, ndigits);

	return 0;	/* gcd is in d */
}
