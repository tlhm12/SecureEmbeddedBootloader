/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/***************************/
/* RANDOM NUMBER FUNCTIONS */
/***************************/

/* New in [v2.4] */
/** Generate a quick-and-dirty random mp number a <= 2^{nbits}-1 using plain-old-rand */
size_t mpQuickRandBits(DIGIT_T a[], size_t ndigits, size_t nbits)
{
	size_t ndig, nodd, i;
	DIGIT_T r;

	mpSetZero(a, ndigits);

	/* Catch too long nbits */
	if (nbits / BITS_PER_DIGIT > ndigits)
		nbits = ndigits * BITS_PER_DIGIT;

	ndig = nbits / BITS_PER_DIGIT;	/* # of complete digits */
	nodd = nbits % BITS_PER_DIGIT;	/* # of odd bits, perhaps zero */

	/* Fill each complete digit with random bits */
	for (i = 0; i < ndig; i++)
	{
		a[i] = spSimpleRand(0, MAX_DIGIT);
	}
	if (nodd)
	{
		r = spSimpleRand(0, MAX_DIGIT);
		r >>= BITS_PER_DIGIT - nodd;
		a[ndig] = r;
		i++;
	}

	return i;
}

/* Internal functions used for "simple" random numbers */

void rand_seed()
/* [v2.2] Moved seeding process inside this function.
   Added clock() to time() to improve precision. 
   [v2.4] Extra fudge with time shifted left by 16
*/
{
	/* Seed with system time and clock */
	unsigned int seed = (((unsigned int)time(NULL) & 0xFFFF) << 16) ^ (unsigned int)clock();
	srand(seed);
}

static DIGIT_T rand_between(DIGIT_T lower, DIGIT_T upper)
/* Returns a single pseudo-random digit between lower and upper.
   Uses rand(). Assumes srand() already called. */
{
	DIGIT_T d, range;
	unsigned char *bp;
	int i, nbits;
	DIGIT_T mask;

	if (upper <= lower) return lower;
	range = upper - lower;

	do
	{
		/* Generate a random DIGIT byte-by-byte using rand() */
		bp = (unsigned char *)&d;
		for (i = 0; i < sizeof(DIGIT_T); i++)
		{
			bp[i] = (unsigned char)(rand() & 0xFF);
		}

		/* Trim to next highest bit above required range */
		mask = HIBITMASK;
		for (nbits = BITS_PER_DIGIT; nbits > 0; nbits--, mask >>= 1)
		{
			if (range & mask)
				break;
		}
		if (nbits < BITS_PER_DIGIT)
		{
			mask <<= 1;
			mask--;
		}
		else
			mask = MAX_DIGIT;

		d &= mask;

	} while (d > range); 

	return (lower + d);
}

DIGIT_T spSimpleRand(DIGIT_T lower, DIGIT_T upper)
{	/*	Returns a pseudo-random digit.
		Handles own seeding using time.
		NOT for cryptographically-secure random numbers.
		NOT thread-safe because of static variable.
		Changed in Version 2 to use internal funcs.
	*/
	static unsigned seeded = 0;

	if (!seeded)
	{
		rand_seed();
		seeded++;
	}
	return rand_between(lower, upper);
}

