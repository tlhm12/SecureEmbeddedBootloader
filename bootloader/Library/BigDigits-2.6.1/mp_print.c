/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

/*******************/
/* PRINT FUNCTIONS */
/*******************/
/* [v2.1] changed to use C99 format types */
void mpPrint(const DIGIT_T *a, size_t len)
{
	while (len--)
	{
		printf("%08" PRIxBIGD " ", a[len]);
	}
}

void mpPrintNL(const DIGIT_T *a, size_t len)
{
	size_t i = 0;

	while (len--)
	{
		if ((i % 8) == 0 && i)
			printf("\n");
		printf("%08" PRIxBIGD " ", a[len]);
		i++;
	}
	printf("\n");
}

void mpPrintTrim(const DIGIT_T *a, size_t len)
{
	/* Trim leading digits which are zero */
	while (len--)
	{
		if (a[len] != 0)
			break;
	}
	len++;
	/* Catch empty len to show 0 */
	if (0 == len) len = 1;

	mpPrint(a, len);
}

void mpPrintTrimNL(const DIGIT_T *a, size_t len)
{
	/* Trim leading zeroes */
	while (len--)
	{
		if (a[len] != 0)
			break;
	}
	len++;
	/* Catch empty len to show 0 */
	if (0 == len) len = 1;

	mpPrintNL(a, len);
}

void mpPrintHex(const char *prefix, const DIGIT_T *a, size_t len, const char *suffix)
{
	if (prefix) printf("%s", prefix);
	/* Trim leading digits which are zero */
	while (len--)
	{
		if (a[len] != 0)
			break;
	}
	len++;
	if (0 == len) len = 1;
	/* print first digit without leading zeros */
	printf("%" PRIxBIGD, a[--len]);
	while (len--)
	{
		printf("%08" PRIxBIGD, a[len]);
	}
	if (suffix) printf("%s", suffix);
}

void mpPrintDecimal(const char *prefix, const DIGIT_T *a, size_t len, const char *suffix)
{
#ifdef NO_ALLOCS
	char s[MAX_ALLOC_SIZE*3];	// [v2.6] increased
#else
	char *s;
#endif
	size_t nc;
	/* Put big digit into a string of decimal chars */
	nc = mpConvToDecimal(a, len, NULL, 0);
	ALLOC_BYTES(s, nc + 1);
	nc = mpConvToDecimal(a, len, s, nc + 1);
	if (prefix) printf("%s", prefix);
	printf("%s", s);
	if (suffix) printf("%s", suffix);
	FREE_BYTES(s, nc + 1);
}

/* ADDED [v2.5] */
void mpPrintDecimalSigned(const char *prefix, DIGIT_T *a, size_t len, const char *suffix)
{
#ifdef NO_ALLOCS
	char s[MAX_ALLOC_SIZE*3];	// [v2.6] increased
#else
	char *s;
#endif
	size_t nc;
	int isneg = 0;
	if (prefix) printf("%s", prefix);
	if (mpIsNegative(a, len)) {
		/* NB changes a in situ temporarily */
		mpChs(a, a, len);
		printf("-");
		isneg = 1;
	}
	/* Put big digit into a string of decimal chars */
	nc = mpConvToDecimal(a, len, NULL, 0);
	ALLOC_BYTES(s, nc + 1);
	nc = mpConvToDecimal(a, len, s, nc + 1);
	printf("%s", s);
	if (suffix) printf("%s", suffix);
	if (isneg) {
		mpChs(a, a, len);
	}
	FREE_BYTES(s, nc + 1);
}

/* ADDED [v2.6] */
void mpPrintBits(const char *prefix, DIGIT_T *a, size_t ndigits, const char *suffix)
{
	int nbits, i, v;
	if (prefix) printf("%s", prefix);
	nbits = (int)mpBitLength(a, ndigits);
	for (i = nbits; i > 0; i--) {
		// Could use a mask here to avoid slightly more expensive calls to mpGetBit(), but hey!
		v = mpGetBit(a, ndigits, i - 1);
		printf("%c", (v ? '1' : '0'));
	}
	if (0 == nbits) printf("0");
	if (suffix) printf("%s", suffix);
}
