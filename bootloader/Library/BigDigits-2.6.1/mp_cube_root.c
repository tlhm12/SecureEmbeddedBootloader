/***** BEGIN LICENSE BLOCK *****
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (c) 2001-16 David Ireland, D.I. Management Services Pty Limited
 * <http://www.di-mgt.com.au/bigdigits.html>. All rights reserved.
 *
 ***** END LICENSE BLOCK *****/

#include "bigdigits.h"
#include "bigdigits_interals.h"

int mpCubeRoot(DIGIT_T s[], const DIGIT_T n[], size_t ndigits)
	/* Computes integer cube root s = floor(cuberoot(n)) i.e. 
	the largest integer whose cube is less than or equal to n */
	/* [Added v2.3] */
{
/*	Allocate temp storage */
#ifdef NO_ALLOCS
	DIGIT_T x[MAX_FIXED_DIGITS];
	DIGIT_T y[MAX_FIXED_DIGITS];
	DIGIT_T q[MAX_FIXED_DIGITS];
	DIGIT_T r[MAX_FIXED_DIGITS];
	assert(ndigits <= MAX_FIXED_DIGITS);
#else
	DIGIT_T *x, *y, *q, *r;
	x = mpAlloc(ndigits);
	y = mpAlloc(ndigits);
	q = mpAlloc(ndigits);
	r = mpAlloc(ndigits);
#endif

	/* if (n <= 1) return n */
	if (mpShortCmp(n, 1, ndigits) <= 0)
	{
		mpSetEqual(s, n, ndigits);
		goto done;
	}

	/* 1. [Initialize] Set x = n */
	mpSetEqual(x, n, ndigits);

	while (1)
	{
		/* 2. [Newtonian step] Set y = [2x + [n/x^2]]/3 */
		mpDivide(y, r, n, ndigits, x, ndigits);
		mpDivide(q, r, y, ndigits, x, ndigits);
		mpAdd(y, q, x, ndigits);
		mpAdd(q, y, x, ndigits);
		mpShortDiv(y, q, 3, ndigits);

		/* 3a. [Finished?] If y < x set x = y and go to step 2 */
		if (mpCompare(y, x, ndigits) >= 0)
			break;

		mpSetEqual(x, y, ndigits);
	}

	/* 3b. Otherwise output x and stop. */
	mpSetEqual(s, x, ndigits);

done:
	mpDESTROY(x, ndigits);
	mpDESTROY(y, ndigits);
	mpDESTROY(q, ndigits);
	mpDESTROY(r, ndigits);

	return 0;
}

