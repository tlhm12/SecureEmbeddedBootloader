#ifndef _BIGDIGIT_AVR_H_
#define _BIGDIGIT_AVR_H_

#include <bigdigits.h>
#define need_size_t
#include <stddef.h>

size_t mpConvFromOctets_P(DIGIT_T a[], size_t ndigits, const unsigned char *c, size_t nbytes);
#endif
