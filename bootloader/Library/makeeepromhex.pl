#! /usr/bin/perl -w

# Author: Jörg Wunsch
# found on: http://www.avrfreaks.net/forum/eep-file-format-how-create-custom-eep-file
# date found: 06.11.2016
# usage:
# stdin accepts <address> <space> <Byte value> lines
# output is the encoded eep file for usage with avrdude
use strict;

my @a; # array to collect data within

while (<>) {
	# discard anything but the lines with XXXX XX
	next unless /^([0-9a-fA-F]+)\s+([0-9a-fA-F]+)/;
	$a[hex($1)] = hex($2);
}

# preamble: length field, start at address 0, record type 0
printf ":%02X000000", $#a + 1;

# initialize checksum with length field
my $s = $#a+1;

# now, print each byte, and update checksum
for(my $i = 0; $i <= $#a; $i++) {
	# fill in zeros for any gaps
	my $val = defined($a[$i])? $a[$i]: 0;
	printf "%02X", $val;
	$s += $val;
}

# print checksum and termination record
printf "%02X\r\n:00000001FF\r\n", (-$s & 0xFF);
