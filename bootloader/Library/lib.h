#ifndef __LIB_H_
#define __LIB_H_

#include <setjmp.h>

void* _init(jmp_buf *);

void* getFunctionAddress(const char* name);
#endif
