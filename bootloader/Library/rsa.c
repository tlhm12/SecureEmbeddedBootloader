#include "rsa.h"
#define __need_size_t
#include <stddef.h>

#include <hashfunction_descriptor.h>
#include <avr/pgmspace.h>
#include <string.h>

#define INTENDED_MESSAGE_TOO_SHORT (-1)
#define MEMORY_ERROR (-2)

static void prv_rsa_function( DIGIT_T *exponent, DIGIT_T *modul, DIGIT_T *in_data, DIGIT_T *out_data, size_t cb_len )
{
    /*
     * m = message
     * c = cryptotext
     * e = public exponent
     * d = private exponent
     * n = RSA modul
     *
     * To decrypt/verify:
     *      m(c) = c^d mod n
     *
     * To encrypt/sign:
     *      c(m) = m^e mod n
     */
    mpModExp( out_data, in_data, exponent, modul, cb_len / ( BITS_PER_DIGIT/8) );
}

int rsa_encrypt( RsaPublicKey_t *key, uint8_t *message, uint16_t cb_message )
{
    DIGIT_T m[RSA_KEYSIZE/BITS_PER_DIGIT];
    DIGIT_T c[RSA_KEYSIZE/BITS_PER_DIGIT];
    mpConvFromOctets( m, RSA_KEYSIZE/BITS_PER_DIGIT, message, cb_message );
    prv_rsa_function( key->public_exponent, key->modul, m, c, RSA_KEYSIZE/BITS_PER_DIGIT );
    mpConvToOctets( c, RSA_KEYSIZE/BITS_PER_DIGIT, message, cb_message );
    return 0;
}

int rsa_decrypt( RsaPrivateKey_t *key, uint8_t *message, uint16_t cb_message )
{

}

const uint8_t sha256_AlgorithmIdentifier[19] PROGMEM =
{
    0x30, 0x31, 0x30, 0x0d,
    0x06, 0x09, 0x60, 0x86,
    0x48, 0x01, 0x65, 0x03,
    0x04, 0x02, 0x01, 0x05,
    0x00, 0x04, 0x20
};

int16_t compute_hash( hfgen_ctx_t *algorithm, uint8_t *message, uint16_t cb_message, uint8_t *hashValue)
{
    int16_t length_of_signature;
    uint8_t *msg_ptr = message;
    uint8_t *msg_end_ptr = message + cb_message;
    hfdesc_t *hash_description = algorithm -> desc_ptr;
    void *state = algorithm -> ctx;
    const uint16_t blocksize = hash_description -> blocksize_b;
    hash_description -> init( state );

    /* if msg_end_ptr is the first pointer that doesn't point somewhere in the
     * message than (msg_end_ptr -blocksize) is the first pointer where one
     * cant get blocksize bytes belonging to the message
     */
    uint8_t *end_of_complete_blocks = msg_end_ptr - blocksize;
    while( msg_ptr < end_of_complete_blocks )
    {
        hash_description -> nextBlock( state, msg_ptr );
        msg_ptr += blocksize;
    }

    /* lastBlock function expects length in bits */
    hash_description -> lastBlock( state, msg_ptr, (msg_end_ptr - msg_ptr) * 8 );

    hash_description -> ctx2hash( hashValue,  state );
    length_of_signature = hash_description -> hashsize_b;

    return length_of_signature;
}

int16_t pkcs1_emsa_encode( enum algorithm_t algorithm, uint8_t *hash_digest, uint16_t cb_hash, uint8_t *encoded_message, uint16_t emLen )
{
    /*
     * EM = EMSA-PKCS1-v1_5-ENCODE (M, emLen)
     *
     * Input:
     *  M        message to be encoded
     *  emLen    intended length in octets of the encoded message*, at
     *          least tLen + 11, where tLen is the octet length of the
     *          Distinguished Encoding Rules (DER) encoding T of
     *          a certain value computed during the encoding operation
     *
     * Output:
     *  EM       encoded message, an octet string of length emLen
     */

    /*
     * EM = 0x00 || 0x01 || PS || 0x00 || T.
     *
     * T for SHA-256 (see RFC8017 page 47)
     * SHA-256: (0x)30 31 30 0d 06 09 60 86 48 01 65 03 04 02 01 05 00
     *              04 20 || H.
     *
     */

    int16_t tLen = sizeof(sha256_AlgorithmIdentifier) + cb_hash;//algorithm->desc_ptr->hashsize_b;
    uint8_t ps_len;
    if( emLen < ( tLen + 11 ) )
    {
        return INTENDED_MESSAGE_TOO_SHORT;
    }
    ps_len =  emLen -tLen - 3;
    uint16_t i = 0;
    encoded_message[i++] = 0x00;
    encoded_message[i++] = 0x01;
    while( i < ps_len + 2 )
    {
        encoded_message[i++] = 0xFF;
    }
    encoded_message[i++] = 0x00;
    if( memcpy_P(&(encoded_message[i]),sha256_AlgorithmIdentifier,sizeof(sha256_AlgorithmIdentifier)) == NULL )
    {
        return MEMORY_ERROR;
    }
    i += sizeof(sha256_AlgorithmIdentifier);

    /* copy hash digest to the right place in encoded_message */
    memcpy( &( encoded_message[i] ), hash_digest, cb_hash );
    return emLen;
}


int16_t rsa_sign( enum algorithm_t algorithm, RsaPrivateKey_t *key, uint8_t *hash_digest, uint16_t cb_hash, uint8_t *signature )
{
    int16_t emLen = RSA_KEYSIZE / 8;
    DIGIT_T sig[ ( RSA_KEYSIZE / BITS_PER_DIGIT ) ];
    DIGIT_T encoded_message[ ( RSA_KEYSIZE / BITS_PER_DIGIT ) ];

    emLen = pkcs1_emsa_encode( algorithm, hash_digest, cb_hash, signature, emLen );

    mpConvFromOctets( encoded_message, ( RSA_KEYSIZE / BITS_PER_DIGIT ), signature, emLen );
    prv_rsa_function( key->private_exponent , key->modul, encoded_message, sig, emLen );
    mpConvToOctets(sig, ( RSA_KEYSIZE / BITS_PER_DIGIT ), signature, emLen);

    return emLen;
}

int16_t rsa_verify( enum algorithm_t algorithm, RsaPublicKey_t *key, uint8_t *hash_digest, uint16_t cb_hash, uint8_t *signature, uint16_t cb_signature )
{

}
