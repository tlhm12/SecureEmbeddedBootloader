#include "lib.h"
#include <uart.h>
#include "mod000.h"
#include <string.h>
#include <stdbool.h>

#include <util/delay.h>
#include <bigdigits.h>
#include "rsa.h"
#include <sha256/sha256.h>
#include <avr/pgmspace.h>
#include "bigdigits_avr.h"

#define MAGICNUMBER (0xA123456A)

jmp_buf *env __attribute__ ((section (".noinit")));
uint32_t magicnumber __attribute__ ((section (".noinit")));

/* sensless but nesasary to have the linker working
 */
int main(void){
    if( magicnumber == MAGICNUMBER)
        longjmp( env, 1);
    init_mod000();
    while(true)
    {
        mod000_rgb(rot);
        _delay_ms(500);
        mod000_rgb(schwarz);
        _delay_ms(500);
    }
    return 0;
}

extern void __do_copy_data( void);

void* _init(jmp_buf* jmp) {
    env = jmp;
    if(setjmp(*jmp)){
         /*handle return*/
         magicnumber = 0;
         return getFunctionAddress;
    }
    magicnumber = MAGICNUMBER;
    __do_copy_data();
    return getFunctionAddress;
}

const char s_init_mod000[] PROGMEM = "init_mod000";
const char s_mod000_rgb[] PROGMEM = "mod000_rgb";
const char s_uart_init[] PROGMEM = "uart_init";
const char s_uart_getc[] PROGMEM = "uart_getc";
const char s_uart_putc[] PROGMEM = "uart_putc";
const char s_uart_puts[] PROGMEM = "uart_puts";
const char s_sha256_init[] PROGMEM = "sha256_init";
const char s_sha256_nextBlock[] PROGMEM = "sha256_nextBlock";
const char s_sha256_lastBlock[] PROGMEM = "sha256_lastBlock";
const char s_sha256_ctx2hash[] PROGMEM = "sha256_ctx2hash";
const char s_rsa_sign[] PROGMEM = "rsa_sign";
const char s_mpConvFromOctets[] PROGMEM = "mpConvFromOctets";
const char s_mpConvToOctets[] PROGMEM = "mpConvToOctets";
const char s_mpModExp[] PROGMEM = "mpModExp";
const char s_rsa_verify[] PROGMEM = "rsa_verify";
const char s_mpConvFromOctets_P[] PROGMEM = "mpConvFromOctets_P";
const char s_compute_hash[] PROGMEM = "compute_hash";

void* getFunctionAddress(const char* name){
    void* ret = NULL;
    if(strcmp_P(name, s_mod000_rgb) == 0){
        ret = mod000_rgb;
    }else if(strcmp_P(name, s_init_mod000) == 0){
        ret = init_mod000;
    }else if(strcmp_P(name, s_uart_init) == 0){
        ret = uart_init;
    }else if(strcmp_P(name, s_uart_getc) == 0){
        ret = uart_getc;
    }else if(strcmp_P(name, s_uart_putc) == 0){
        ret = uart_putc;
    }else if(strcmp_P(name, s_uart_puts) == 0){
        ret = uart_puts;
    }else if(strcmp_P(name, s_mpConvFromOctets) == 0){
        ret = mpConvFromOctets;
    }else if(strcmp_P(name,s_mpConvToOctets) == 0){
        ret = mpConvToOctets;
    }else if(strcmp_P(name,s_mpModExp) == 0){
        ret = mpModExp;
    }else if(strcmp_P(name, s_rsa_sign) == 0){
        ret = rsa_sign;
    }else if(strcmp_P(name,s_rsa_verify) == 0){
        ret = rsa_verify;
    }else if(strcmp_P(name, s_sha256_init) == 0){
        ret = sha256_init;
    }else if(strcmp_P(name,s_sha256_nextBlock) == 0){
        ret = sha256_nextBlock;
    }else if(strcmp_P(name, s_sha256_lastBlock) == 0){
        ret = sha256_lastBlock;
    }else if(strcmp_P(name, s_sha256_ctx2hash) == 0){
        ret = sha256_ctx2hash;
    }else if(strcmp_P(name, s_mpConvFromOctets_P) == 0){
        ret = mpConvFromOctets_P;
    }else if(strcmp_P(name, s_compute_hash) == 0){
        ret = compute_hash;
    }
    return ret;
}
