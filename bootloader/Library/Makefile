uartlibdir = uartlibrary
bigdigitlibdir = BigDigits-2.6.1
avr-crypto-lib = avr-crypto-lib
VPATH += $(avr-crypto-lib)

# MCU name, MOD000, MOD015=atmega328, MOD020=atmega168
MCU = atmega328
#MCU = atmega168
#MCU =atmega88
#MCU =atmega8
#MCU = at90s2313

# Processor frequency.
#     This will define a symbol, F_CPU, in all source code files equal to the
#     processor frequency. You can then use this symbol in your source code to
#     calculate timings. Do NOT tack on a 'UL' at the end, this will be done
#     automatically to create a 32-bit value in your source code.
#     Typical values are:
#         F_CPU =  1000000
#         F_CPU =  1843200
#         F_CPU =  2000000
#         F_CPU =  3686400
#         F_CPU =  4000000
#         F_CPU =  7372800
#         F_CPU =  8000000
#         F_CPU = 11059200
#         F_CPU = 14745600
#         F_CPU = 16000000
#         F_CPU = 18432000
#         F_CPU = 20000000
F_CPU = 16000000

CC = avr-gcc -c -mmcu=$(MCU)
LD = avr-gcc -mmcu=$(MCU)
RM = rm

AR = avr-ar

OBJCOPY = avr-objcopy
SIZE = avr-size

OPT = s

CSTANDARD = -std=gnu99

CDEFS = -DF_CPU=$(F_CPU)UL

EXTRAINCDIRS = $(bigdigitlibdir) $(uartlibdir) $(avr-crypto-lib) ./Library

CFLAGS = -g$(DEBUG)
CFLAGS += $(CDEFS)
CFLAGS += -O$(OPT)
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -fshort-enums
CFLAGS += -Wall
CFLAGS += -Wstrict-prototypes
CFLAGS += -fstack-usage
#CFLAGS += -mshort-calls
#CFLAGS += -fno-unit-at-a-time
#CFLAGS += -Wundef
#CFLAGS += -Wunreachable-code
#CFLAGS += -Wsign-compare
#CFLAGS += -Wa,-adhlns=$(<:%.c=$(OBJDIR)/%.lst)
CFLAGS += $(CSTANDARD)

CPPFLAGS += -DNO_ALLOCS
CPPFLAGS += -DMAX_FIXED_BIT_LENGTH=512
CPPFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))

LDFLAGS = -Wl,-Map=$(patsubst %.elf,%.map,$@)

HEXSIZE = $(SIZE) --target=$(FORMAT) $(TARGET).hex
ELFSIZE = $(SIZE) $(TARGET).elf

lib.hex: lib.elf
	$(OBJCOPY)  -O ihex -R .eeprom -R .fuse -R .lock -R .signature $< $@

lib.elf: lib.o uart.o mod000.o libbigdigits.a bigdigit-helper.o rsa.o sha256.o mp_octet_conversion_progmem.o
	$(LD) $(LDFLAGS) -o $@ $^

lib.o: lib.c lib.h

lib.eep: library_position.eeprom
	./makeeepromhex.pl < $< > $@

uart.o:
	$(MAKE) -C $(uartlibdir) -f Makefile.uart
	cp $(uartlibdir)/uart.o ./

mod000.o: mod000.c

sha256.o: sha256/sha256.c sha256/sha256.h
	$(CC) -c $(CPPFLAGS) $(CFLAGS) -o $@ $<

export
.PHONY: libbigdigits.a
libbigdigits.a:
	$(MAKE) -e -C $(bigdigitlibdir) $@
	cp $(bigdigitlibdir)/$@ ./

.PHONY: clean
clean:
	$(RM) -rf *.o
	$(RM) -rf *.elf
	$(RM) -rf *.hex
	$(RM) -f *.a
	$(MAKE) -C $(uartlibdir) -f Makefile.uart clean
	$(MAKE) -C $(bigdigitlibdir) clean
