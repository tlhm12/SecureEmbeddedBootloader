#ifndef __RSA_H__
#define __RSA_H__

#include <bigdigits.h>
#include "rsa_config.h"
#include <stdint.h>
#include <hashfunction_descriptor.h>

enum algorithm_t{
    alg_sha256
};

struct RsaPublicKey_t
{
    DIGIT_T modul[RSA_KEYSIZE / BITS_PER_DIGIT];
    DIGIT_T public_exponent[2];
};

typedef struct RsaPublicKey_t RsaPublicKey_t;

struct RsaPrivateKey_t
{
    DIGIT_T modul[RSA_KEYSIZE / BITS_PER_DIGIT];
    DIGIT_T private_exponent[RSA_KEYSIZE / BITS_PER_DIGIT];
};

typedef struct RsaPrivateKey_t RsaPrivateKey_t;


int rsa_encrypt( RsaPublicKey_t *key, uint8_t *message, uint16_t cb_message );

int rsa_decrypt( RsaPrivateKey_t *key, uint8_t *message, uint16_t cb_message );

int16_t rsa_sign( enum algorithm_t algorithm, RsaPrivateKey_t *key, uint8_t *hash_digest, uint16_t cb_hash, uint8_t *signature );

int16_t rsa_verify( enum algorithm_t algorithm, RsaPublicKey_t *key, uint8_t *hash_digest, uint16_t cb_hash, uint8_t *signature, uint16_t cb_signature);

int16_t compute_hash( hfgen_ctx_t *algorithm, uint8_t *message, uint16_t cb_message, uint8_t *hashValue);

#endif
