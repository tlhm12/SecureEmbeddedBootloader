#ifndef _MOD_000_
#define _MOD_000_
/**
 * @file AEVUM_MOD000.h
 * @brief Stellt Grundfunktionalit�ten der Hardware MOD000 her.
 * MOD_000 ist ein Mini-Controllermodul auf Basis Atmega328 mit den
Steckverbindern POW, DE, AS, ISP und einer RGB-LED on Board
 *
 *************************************************************************
 * @copyright 2015, AEVUM Mechatronik GmbH
 *
 * # Part of Modularis project
 *
 * Modularis simplifies product development.
 *
 * More infos
 * - <http://www.aevum-mechatronik.de>
 * - <http://www.modularis.de>
 *************************************************************************
 * # Steckbrief
 *
 * - Ben�tigte Hardware	: MOD000
 * - Optionale Hardware	: -
 * - Projektstatus	: Sample
 * - Verantwortlicher	: Dr. Mathias Bachmayer <bachmayer@aevum-mechatronik.de>
 *
 * # Versionshistory
 *
 * Version	|Datum		|Autor		|Beschreibung
 *
-------------|---------------|---------------|----------------------------------
-
 * 0.1		|2014-02-16	|Mathias Bachmayer	|initiale Version
 * 0.2		|2015-07-12	|Tommy Lehmann		|Trennung von Deklaration und Definition
 *
 * @todo Lizenz muss gefunden werden.
 *
 * # License
 * Copyright (c) 2010, AEVUM Mechatronik GmbH
 *
 */


// 1 A Einbinden Standardbibliotheken

	#include <avr/io.h>
	#include <avr/interrupt.h>
	#include <stdlib.h>
	#include <stdio.h>
// 1 B Modularis Bibliotheken einbinden
	//#include <AEVUM_SIGNALTYPES.h>
	//#include <AEVUM_PWM.h>

// 2 Globale Variablendeklarationen
	// SPI Vorbereitende Definitionen
	#define CSAPORT 1
	#define CSAPIN  0
	#define CSBPORT 3
	#define CSBPIN  4
	// Definitionen f�r ON-BOARD-RGB-LED via MOD000_RGB
	#define rot 1
	#define gelb 3
	#define blau 4
	#define gruen 2
	#define lila 5
	#define weiss 7
	#define schwarz 0
	#define cyan 6
// 3 Globale Funktionsdeklarationen
	/**
	 * Initialisiert die Funktionen des MOD000
	 *
	 *
	 * @post PB1, PB2, PD6 sind Ausg�nge f�r die RGB LED
	 * @post alle anderen GPIO-Pins sind Eing�nge mit Pull-Ups
	 */
	void init_mod000(void);

	/**
	 * Setzt die Onboard LED auf einen einfachen Farbwert.
	 *
	 * Die Funktion setzt eine 3 Bit Farbwert. Die Bitmaske f�r die Farben
	 * ist dabei wie folgt.
	 *
	 * - Rot: 0x01
	 * - Gr�n: 0x02
	 * - Blau: 0x04
	 *
	 * Der Wert 0x05 w�rde also den Rot- und Blauwert einschalten.
	 *
	 * @param RGB der Farbwert. Die Niederwertigsten 3 Bit stehen dabei f�r
	 * Rot, Gr�n und Blau
	 *
	 * @pre INIT_MOD000() was called
	 */
	void mod000_rgb(uint8_t RGB);

	/**
	 * Schaltet die Onboard LED abwechselnd mit der �bergebenen Farbe ein
	 * oder aus.
	 *
	 * Die LED wird immer im Wechsel des Funktionsaufrufs ein und
	 * ausgeschaltet.
	 * Die Funktion setzt eine 3 Bit Farbwert. Die Bitmaske f�r die Farben
	 * ist dabei wie folgt.
	 *
	 * - Rot: 0x01
	 * - Gr�n: 0x02
	 * - Blau: 0x04
	 *
	 * Der Wert 0x05 w�rde also den Rot- und Blauwert einschalten.
	 *
	 * Nach dem Initialisieren des Moduls w�rden 3 aufeinander folgende
         * Funktionsaufrufe Einschalten, Ausschalten, Einschalten bewirken.
	 *
	 * @param RGB der Farbwert. Die Niederwertigsten 3 Bit stehen dabei f�r
	 * Rot, Gr�n und Blau
	 *
	 * @pre INIT_MOD000() was called
	 */
	void mod000_rgb_toggel(uint8_t RGB);

	/**
	 * Initialisiert das Schreiben von Farbwerten mittels PWM.
	 *
	 * @post 3 PWM Ausg�nge des MOD000 werden genutzt um Farben zu schreiben
	 */
	//void init_mod000_pwmrgb(void);

	/**
	 * Gibt einen RGB Wert auf der Onboard LED aus.
	 *
	 *
	 * @param Farbe die RGB Farbe welche auf der LED ausgegeben werden soll
	 *
	 * @pre INIT_MOD000_PWMRGB() was called
	 */
	//void mod000_pwmrgb(RGBColor Farbe);

#endif
