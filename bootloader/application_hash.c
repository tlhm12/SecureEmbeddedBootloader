#include "application_hash.h"
#include <sha256/sha256.h>
#include "dyn_lib.h"
#include <util/delay.h>

uint8_t* hash_get_buffer(uint32_t *total_size);

Uart_Transfer_Application_t application_hash;

Uart_Transfer_Application_t* hash_get_application(void) {
    application_hash.get_buffer = hash_get_buffer;
    return &application_hash;
}

uint8_t block[SHA256_BLOCK_BYTES];

uint8_t* hash_get_buffer(uint32_t *total_size){
    *total_size = SHA256_BLOCK_BYTES;
    return block;
}
