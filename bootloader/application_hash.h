#ifndef __APPLICATION_HASH_H__
#define __APPLICATION_HASH_H__

#include "protokoll.h"

Uart_Transfer_Application_t* hash_get_application(void);

#endif
