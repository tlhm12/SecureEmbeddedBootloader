#include <string.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/boot.h>
#include <util/delay.h>
#include "uart_ptr.h"
#include "boot.h"
//#include "mod000.h"
#include "hexparser.h"
#include <sha256/sha256.h>
#include <hashfunction_descriptor.h>
#include <rsa.h>
#include <rsa_config.h>
#include <bigdigits.h>
#include <bigdigits_avr.h>
#include "dyn_lib.h"
#include "protokoll.h"
#include "application_hash.h"

#include "key.h"

#define BOOT_UART_BAUD_RATE     9600     /* Baudrate */

#define XON                     17       /* XON Zeichen */
#define XOFF                    19       /* XOFF Zeichen */

#define BOOT_STATE_SIGNATURE 255

#define TIME_TO_WAIT (2000)
#define ROUND_TIME (100)
#define MAX_ROUNDS_TO_WAIT ( TIME_TO_WAIT / ROUND_TIME )

	// Definitionen für ON-BOARD-RGB-LED via MOD000_RGB
	#define rot 1
	#define gelb 3
	#define blau 4
	#define gruen 2
	#define lila 5
	#define weiss 7
	#define schwarz 0
	#define cyan 6

uint8_t bootloader_state_maschine(uint16_t c);

bootloader_state_t init_bootloader_state(void);

void bootloader( void );
void hash_and_sign( void );
void generate_hash( uint8_t *hash );
void sign_digest(enum algorithm_t algorithm, uint8_t *hash, uint8_t cb_hash );
static char nibble_to_hex( uint8_t nibble );
static void byte_to_hex( char *hexvalues, uint8_t byte );

const bootloader_state_t DEFAULT_STATE = {
    .hex_check = 0,
    .flash_cnt = 0,
    .hex_addr = 0,
    .flash_page = 0,
    .parser_state = 0,
    .hex_cnt = 0,
    .hex_size = 0,
    .hex_data_cnt = 0,
    .hex_type = 0,
    .hex_checksum = 0,
    .flash_page_flag = 13
};

uint8_t bootloader_state_maschine(uint16_t c){
    switch((uint8_t)c)
    {
        case 'p':
            //uart_puts("Programmiere den Flash!\n\r");
            //uart_puts("Kopiere die Hex-Datei und füge sie"
            //" hier ein (rechte Maustaste)\n\r");
            uart_putc('!');
            return BOOT_STATE_PARSER;
        default:
            uart_puts("Verlasse den Bootloader\n\r");
            return BOOT_STATE_EXIT;
    }
}

bootloader_state_t init_bootloader_state(){
    bootloader_state_t init_value;
    /* Füllen der Puffer mit definierten Werten */
    init_value = DEFAULT_STATE;
    memset(init_value.hex_buffer, 0x00, sizeof(init_value.hex_buffer));
    memset(init_value.flash_data, 0xFF, sizeof(init_value.flash_data));
    return init_value;
}

int main(void)
{
    /* Empfangenes Zeichen + Statuscode */
    uint16_t        c = 0;

    /* temporäre Variable */
    uint8_t         temp;

    /* Startaddress of application (word address)
     * 0x6090 / 2 = 0x3048
     */
    void            (*start)( void ) = 0x3048;



    load_addresses();

    init_mod000();
    mod000_rgb(gruen);
    uart_init( UART_BAUD_SELECT(BOOT_UART_BAUD_RATE,F_CPU) );
    sei();

    mod000_rgb(blau);
    uart_puts("B");

    uint8_t rounds = 0;

    while( rounds < MAX_ROUNDS_TO_WAIT )
    {
        c = uart_getc();
        if( !(c & UART_NO_DATA) )
        {
                switch( c )
                {
                    case 'l':
                        bootloader();
                        break;
                    case 's':
                        hash_and_sign();
                        rounds = 0;//wait again
                        break;
                }
        }
        else
        {
            rounds++;
            uart_putc('.');
            _delay_ms( ROUND_TIME );
        }
    }

    //uart_puts("Reset AVR!\n\r");
    uart_putc('\r');
    uart_putc('\n');
    mod000_rgb(cyan);
    _delay_ms(100);
    /* Reset */
    start();

    return 0;
}

void bootloader()
{
    /* Empfangenes Zeichen + Statuscode */
    uint16_t        c = 0;

    /* Flag zum steuern des Programmiermodus */
    uint8_t boot_state = BOOT_STATE_EXIT;

    bootloader_state_t state = init_bootloader_state();
    do
    {
        c = uart_getc();
        if( !(c & UART_NO_DATA) )
        {
            /* Programmzustand: Parser */
            if(boot_state == BOOT_STATE_PARSER)
            {
                uart_putc(XOFF);
                boot_state = parser(&state, c); //branch to parser
                uart_putc(XON);
            }
            /* Programmzustand: UART Kommunikation */
            else if(boot_state != BOOT_STATE_PARSER)
            {
                        boot_state = bootloader_state_maschine(c);
                        break;
            }
        }
    }
    while(boot_state!=BOOT_STATE_EXIT);
}

void hash_and_sign()
{
    uint8_t hash[ SHA256_HASH_BYTES ];
    Uart_Transfer_Application_t* app = hash_get_application();
    register_application(3, app);
    read(hash, SHA256_HASH_BYTES);
    
    //generate_hash( hash );
    //sign_digest( alg_sha256, hash, sizeof(hash) );
}

void generate_hash( uint8_t *hash)
{

    uart_puts("Computing Hash\r\n");
    sha256_ctx_t ctx;
    hfdesc_t hash_descriptor;
    hash_descriptor.type = 2;
    hash_descriptor.hashsize_b = SHA256_HASH_BYTES;
    hash_descriptor.blocksize_b = SHA256_BLOCK_BYTES;
    hash_descriptor.init = lib_addr_tbl[6];
    hash_descriptor.nextBlock = lib_addr_tbl[7];
    hash_descriptor.lastBlock = lib_addr_tbl[8];
    hash_descriptor.ctx2hash = lib_addr_tbl[9];
    hfgen_ctx_t gen_ctx;
    gen_ctx.desc_ptr = &hash_descriptor;
    gen_ctx.ctx = &ctx;
    
    /***************************************************
     * initialize hash like it would be done in compute_hash function
     **************************************************/
    hfgen_ctx_t *algorithm = &gen_ctx;
    
    //uint8_t *msg_ptr = message;
    //uint8_t *msg_end_ptr = message + cb_message;
    hfdesc_t *hash_description = algorithm -> desc_ptr;
    void *state = algorithm -> ctx;
    const uint16_t blocksize = hash_description -> blocksize_b;
    hash_description -> init( state );
    
    /**************************************************
     * read bytes until we have a full block
     *************************************************/
    
    /* variables we need */
    uint8_t block[SHA256_BLOCK_BYTES];
    uint8_t cb_block;
    uint8_t c;
    
    /* initialize variables */
    cb_block = 0;
    
    while( cb_block < blocksize ) {
        block[cb_block] = uart_getc();
        if( !(c & UART_NO_DATA) ) {
            cb_block++;
        }
    }
    //int16_t hash_len = compute_hash( &gen_ctx, message, strlen( message ), hash );
    
    /***************************************************
     * Display the computed hash
     **************************************************/
    uart_puts("Computed Digest: ");
    for( int i = 0; i < hash_descriptor.hashsize_b; i++ )
    {
        char buf[25];
        //sprintf(buf, "%02x", signature[i]);
        byte_to_hex( buf, hash[ i ] );
        buf[ 2 ] = '\0';
        uart_puts(buf);
    }
    uart_puts("\r\n");
}

void sign_digest(enum algorithm_t algorithm, uint8_t *hash, uint8_t cb_hash )
{
    RsaPrivateKey_t priv_key;
    uint8_t signature[RSA_KEYSIZE / 8];
    uart_puts("Converting keys\r\n");
    mpConvFromOctets_P( priv_key.modul, RSA_KEYSIZE / BITS_PER_DIGIT,
                        n, sizeof(n) / sizeof(uint8_t) );
    mpConvFromOctets_P( priv_key.private_exponent,
                        RSA_KEYSIZE /BITS_PER_DIGIT,
                        d, sizeof(d) / sizeof(uint8_t) );

    uart_puts("Computing signature\r\n");
    int sig_len = rsa_sign( algorithm, &priv_key, hash, cb_hash, signature );
    uart_puts("Computed String: ");
    for( int i = 0; i < sig_len; i++ )
    {
        char buf[25];
        //sprintf(buf, "%02x", signature[i]);
        byte_to_hex( buf, signature[ i ] );
        buf[ 2 ] = '\0';
        uart_puts(buf);
    }
    uart_puts("\r\n");
}

static char nibble_to_hex( uint8_t nibble )
{
    if( nibble < 10 )
    {
        return nibble + '0';
    } else
    {
        return nibble + ( 'a' - 10 );
    }
}

static void byte_to_hex( char *hexvalues, uint8_t byte )
{
    hexvalues[ 0 ] = nibble_to_hex( byte >> 4 );
    hexvalues[ 1 ] = nibble_to_hex( byte & 0x0f );
}

void program_page (uint32_t page, uint8_t *buf) {
    uint16_t i;
    uint8_t sreg;
    sreg = SREG;
    mod000_rgb(blau);
    cli();
    eeprom_busy_wait();

    boot_page_erase (page);
    boot_spm_busy_wait();


    for (i=0; i<SPM_PAGESIZE; i+=2)
    {
        /* Set up little-endian word. */
        uint16_t w = *buf++;
        w += (*buf++) << 8;

        boot_page_fill (page + i, w);
    }
    boot_page_write (page);     /* Store buffer in flash page.		*/

    boot_spm_busy_wait();       /* Wait until the memory is written.*/

    /* Reenable RWW-section again. We need this if we want to jump back */
    /* to the application after bootloading. */
    boot_rww_enable ();

    /* Re-enable interrupts (if they were ever enabled). */
         SREG = sreg;
}
